var sql = require('mssql'),
    sgpconn = require('../config/connection'),
    connPoolPromise = null,
    DBUtil = {};

function getConnPoolPromise () {

    if (connPoolPromise) {
        //console.log("Entra por el if connPool");
        return connPoolPromise;
    }

    connPoolPromise = new Promise(function (resolve, reject) {
        //var conn = new sql.Connection(dbConfig);
        var conn = new sql.Connection(sgpconn.getConnection().config);

        conn.on('close', function () {
            console.log("OnClose");
            connPoolPromise = null;
        });

        conn.connect().then(function (connPool) {
            console.log("OnConnect");
            return resolve(connPool);
        }).catch(function (error) {
            console.log("Error al connect: " + error)
            connPoolPromise = null;
            return reject(error);
        })
    });

    return connPoolPromise;
}

function getSqlType (type) {
    if (!type) return sql.NVarChar; //Por defecto

    switch (type) {
        case 'Int':
            type = sql.Int;
            break;
        case 'NVarchar':
            type = sql.NVarChar(sql.MAX);
            break;
        case 'NVarChar':
            type = sql.NVarChar(sql.MAX);
            break;
        case 'VarChar':
            type = sql.VarChar(sql.MAX);
            break;
        case 'Date':
            type = sql.Date;
            break;
        case 'DateTime':
            type = sql.DateTime;
            break;
        case 'Bit':
            type = sql.Bit;
            break;
        case 'Xml':
            type = sql.Xml;
            break;
        //TODO Agregar los que faltan
    }

    return type;
}

DBUtil.executeSp = function (config) {
    console.log("--------- Ejecutando SP: " + config.spName);
    return getConnPoolPromise().then(function (connPool) {
        var request = new sql.Request(connPool);

        if (config.params && config.params.input) {
            config.params.input.forEach(function (ip) {
                request.input(ip.name, getSqlType(ip.type), ip.value);
            })
        }
        if (config.params && config.params.output) {
            config.params.output.forEach(function (op) {
                request.output(op.name, getSqlType(op.type), op.value);
            })
        }

        return request.execute(config.spName);
    })
}

module.exports = DBUtil;
