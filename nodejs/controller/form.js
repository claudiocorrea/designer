var DBUtil = require('../util/DBUtil');

exports.getAll = function (req, res) {
	var config = {
        spName: '[dbo].[Forms_TT]',
        params: {
            input: []
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.add = function (req, res) {
	var data = getParams(req.query)
	var config = {
        spName: (req.query.IdForm != 0) ? '[dbo].[WorkFlowRules.Forms.save]' : '[dbo].[WorkFlowRules.Forms.add]',
        params: data
    };
	
	console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			console.log(result);
			/*result = result[0][0].Result
			if(result.split('|')[0] != 'OK') {
				console.log("ERROR: ", result.split('|')[1]);
				res.status(500).json({ type: '', message: result.split('|')[1] });
			} else res.status(200).json(result.split('||')[1]) */	
			res.status(200).json('OK')	
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

function getParams(data) {
	var input = new Array(), params = { input: input };	
	input.push({name: 'IdUser', type: 'Int', value: data.idUser})
	input.push({name: 'IdLanguage', type: 'Int', value: 1})	
	if(data.IdForm != 0) input.push({name: 'IdForm', type: 'Int', value: data.IdForm})		
	input.push({name: 'FormName', type: 'NVarchar', value: data.FormName})
	input.push({name: 'Description', type: 'NVarchar', value: data.Description})
	if(data.IdForm == 0) { 
		input.push({name: 'IdEmpresa', type: 'Int', value: data.IdEmpresa})
		input.push({name: 'Original', type: 'Int', value: 1})
		input.push({name: 'Code', type: 'NVarchar', value: data.Code})
	} else { 
		input.push({name: 'Code', type: 'NVarchar', value: data.Code})
		input.push({name: 'IdEmpresa', type: 'Int', value: data.IdEmpresa})
		input.push({name: 'Original', type: 'Int', value: 1})
	}
	input.push({name: 'StyleSheet', type: 'NVarchar', value: data.StyleSheet})
	input.push({name: 'IdFormType', type: 'Int', value: data.IdFormType})
	input.push({name: 'InfoPathTemplate', type: 'NVarchar', value: data.InfoPathTemplate})
	input.push({name: 'Columnas', type: 'Int', value: data.Columnas})
	if(data.IdForm == 0)  input.push({name: 'LocalTransaction', type: 'Int', value: 0})
	input.push({name: 'Token', type: 'NVarchar', value: data.token})
	if(data.IdForm == 0) params.output = [{name: 'Result', type: 'NVarchar', value: ''}]
	return params	
}

exports.remove = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.Forms.delete]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'IdForm', type: 'Int', value: req.params.formId}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.formId);
			console.log(result)
			result = result[0][0]
			if(result.Status == 'Error') {
				console.log("ERROR: " + result.Error);
				res.status(500);
				res.json({success: false, error: result.Error});
			} else {
				res.status(200);
				res.json(result);
			}
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500);
            res.json({success: false, error: error.message});
        });
}

exports.getFieldsByFormId = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.GetFormFieldCondition]',
        params: {
            input: [
				{name: 'IdForm', type: 'Int', value: req.query.formId},
				{name: 'StepId', type: 'Int', value: req.query.stepId}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200);
            res.json(result[0]);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.updateFields = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.UpdateFormFieldCondition]',
        params: {
            input: [
				{name: 'StepId', type: 'Int', value: req.query.stepId},
				{name: 'xmlData', type: 'NVarchar', value: req.query.xmlData}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200);
            res.json(result[0]);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.saveFormFields = function (req, res) {
	var config = {
        spName: '[dbo].[FormsFields_MX_FieldsbyBatch]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'IdForm', type: 'Int', value: req.query.idForm},
				{name: 'Token', type: 'NVarchar', value: req.query.token},
				{name: 'xmlData', type: 'NVarchar', value: req.query.xmlData}
			]
        }
    };
	
	//console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			//console.log(result)
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.getExtJSFormFields = function (req, res) {
    var config = {
        spName: '[dbo].[getFormFields]',
        params: {
            input: [
                {name: 'IdForm', type: 'Int', value: req.query.idForm},
                {name: 'WorkFlowId', type: 'Int', value: (req.query.workflowId != null && req.query.workflowId != undefined && req.query.workflowId != "")?req.query.workflowId:null},
                {name: 'User', type: 'Int', value: req.query.userId}
            ]
        }
    }
	
	DBUtil.executeSp(config)
        .then(function (result) {
			var formExtJS = getFormFields(result)
			res.status(200).json(formExtJS);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });		
}

function getFormFields (result) {
	var secciones = []
	if (Array.isArray(result)) {
		if (Array.isArray(result[0])) {
			result = result[0];
		}

		//console.log("result: " + JSON.stringify(result));
		var seccion = null, idFieldContainers = new Array();
		result.forEach(function(field) {
			var fieldContainer = false
			var seccion	= getSeccion(secciones, field.Seccion)
			if(seccion == null) {
				var id = 'section-' + (secciones.length +1)
				seccion = { 
					xtype: 'panel',			
					reference: id, name: id,
					bodyPadding: 10, collapsible: true,
					defaults: { width: '100%', }, items: [],			
					viewModel: { data: { title: field.Seccion } },
					bind: { title: '{title}' },
					header: { listeners: { click: 'fieldSelection'} },
					listeners: { collapse: 'sectionSelect', expand: 'sectionSelect' }
				}
				secciones.push(seccion)
			}			
			seccion.items.push(createExtJSField(field))
			if(field.Tipo == 'FieldContainer') idFieldContainers.push(field.Id.toLowerCase())			
		})		
		if(idFieldContainers.length > 0) {
			idFieldContainers.forEach(function(id){
				var seccion	= getSeccion(secciones, id)
				if(seccion != null) {
					var item = getItem(secciones, id)
					if(item != null) {
						item.items = item.items.concat(seccion.items)
						secciones = removeSeccion(secciones, seccion)
					}
				}				
			})			
		}
	}
	//console.log(JSON.stringify(secciones))
	return {secciones: secciones}
}

getSeccion = function(secciones, seccion) {
	var result = null
	for(var i = 0; i < secciones.length; i++) {
		if(secciones[i].viewModel.data.title == seccion) {
			result = secciones[i]
			break
		}
	}
	return result
}

getItem = function(secciones, item) {
	var result = null
	for(var s = 0; s < secciones.length; s++) {
		var seccion = secciones[s]
		for(var i = 0; i < seccion.items.length; i++) {
			if(seccion.items[i].name == item) {
				result = seccion.items[i]
				break
			}
		}
	}	
	return result
}

removeSeccion = function(secciones, seccion) {
	var result = new Array
	for(var i = 0; i < secciones.length; i++) {
		if(secciones[i].name != seccion.name)
			result.push(secciones[i])
	}
	return result
}

createExtJSField = function(item) {
	var field = (item.Tipo == "Radio Button") ? {} : {
			idField: item.IdField,
			name: item.Id.toLowerCase(),
            fieldLabel: item.TextoFrontEnd,
			labelAlign: (item.TextoFrontEnd.length > 17) ? 'top' : 'left',			 
			editable: false, labelWidth: 125,
			posicion: item.Posicion,
			mandatory: eval(item.Obligatorio), readonly: eval(item.SoloLectura),
			originDB: item.IdOrigen, tableDB: item.Tablename,
			spDB: item.Procedimiento, input: item.Parametros,
			fieldValue: item.CampoValueCombo, comboText: item.CampoTextCombo,
			hidden: (item.Visible == 1) ? true : false
        };
	
	field.listeners = { focusenter: 'fieldSelection' }
	if (item.Style != "") { 
		//field.fieldStyle = item.Style 
		field.style = item.Style 
	}
	if(item.Id == global.KPI && item.TextoFrontEnd == 'KPI') {
		field.hidden = true
	}
	
	try {
        switch (item.Tipo) {
			case "FieldContainer":
				field = {					 
					xtype: 'panel', idField: item.IdField, 
					name: item.Id.toLowerCase(),
                    layout: 'hbox', posicion: item.Posicion,
					bodyPadding: 5, border: true, margin: '5 0 10 0',
					defaults: { flex: 1, margin: '0 20 5 0' },
					items: [
						{	xtype:'button', userCls:'circular', iconCls:'x-fa fa-plus', 
							margin: '2 10 4 0', flex: 0, handler: 'selectedColumns' }				
					],			
					listeners: { focusleave: 'outSelectionColumns', add: 'addItemToFieldContainer' }
				}
				break;
            case "TextBox":
                field.xtype = 'textfield';
				break;
			case "TextArea":
                field.xtype = 'htmleditor';
				field.labelAlign = 'top'
				field.listeners = { focusenter: 'fieldSelection', sync: 'fieldSelection' }
                break;
			case "DateField":
				field.xtype = 'datefield'
				field.dategreater = eval(item.FechaMayorOIgualaHoy)
				break;
			case "NumberField":
                field.xtype = 'numberfield'
				break;
			case "Combo":
                field.xtype = 'combobox'
                field.editable = false
                field.typeAhead = true
				field.displayField = 'value'
				field.valueField = 'id' 
				field.publishes = 'value',
				field.allowEmpty = item.PermiteValorVacio
				field.optionNew = item.OpcionNuevo								
				break;			
            case "CheckBox":
                field.xtype = 'checkbox';
                field.fieldLabel = null;
                field.boxLabel = item.TextoFrontEnd
                field.hiddenApprove = eval(item.OcultaAprobarSiEsFalso)
                field.ignoreIfEmpty = eval(item.IgnoraSiNoHayDatos)
                break;
			case "Radio Button":
				field = {
                    xtype: 'radiogroup', idField: item.IdField,
					name: item.Id.toLowerCase(),
					fieldLabel: item.TextoFrontEnd, labelWidth: 125,
                    posicion: item.Posicion,
					items:[
						{	name:'opcion',
							bind:{ boxLabel:'{txtFields.words.yes}'},
							inputValue:'si', labelClsExtra:'labelField'
						},{	
							name:'opcion', 
							boxLabel:'No', inputValue:'no',					
							padding:'0 0 0 15', labelClsExtra:'labelField'
						}
					],
					mandatory: eval(item.Obligatorio), readonly: eval(item.SoloLectura),
					listeners: { focusenter: 'fieldSelection' }
				}
				break;
			case "Lista a Buscar Multiple":
				field.xtype = 'tagfield';
				break;
			case "Form":
				field = { 
					xtype: 'fieldcontainer', idField: item.IdField,
					name: item.Id,
					fieldLabel: item.TextoFrontEnd,
					labelClsExtra:'labelField', posicion: item.Posicion,
					IdForm: item.IdFormularioHijo,
					items: [
						{	xtype:'button', userCls:'circular',
							iconCls:'x-fa fa-file-text-o',
							tooltip: item.TextoFrontEnd
						}
					],
					mandatory: eval(item.Obligatorio), readonly: eval(item.SoloLectura),
					listeners: { focusenter: 'fieldSelection' }
				}
				break;
		}
	} catch (e) {
        console.log("*** ERROR al parsear EXTJS Item: " + e)
    }
	
	return field
}

