var DBUtil = require('../util/DBUtil');

exports.getAllApprovalsTemplates = function (req, res) {
    var config = {
        spName: '[dbo].[ApprovalsTemplates_TT]',
        params: { input: [] }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.getAllForms = function (req, res) {
	var config = {
        spName: '[dbo].[Forms_TT]',
        params: { input: [] }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.getAllFormTypes = function (req, res) {
	var config = {
        spName: '[dbo].[FormsTypes_TT]',
        params: { input: [] }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.getAllTimeAlerts = function (req, res) {
	var config = {
        spName: '[dbo].[TimeBeforeAlerts_TT]',
        params: { input: [] }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.getAllOperators = function (req, res) {
	var config = {
        spName: '[dbo].[Operators_TT]',
        params: { input: [] }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.getAllProfiles = function (req, res) {
	var config = {
        spName: '[dbo].[Profiles_TT]',
        params: { input: [] }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}