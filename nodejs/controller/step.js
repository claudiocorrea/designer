var DBUtil = require('../util/DBUtil');

exports.getStepsByWorkflowId = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.GetStepsByWorkflowId]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'WorkflowID', type: 'Int', value: req.query.workflowId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.add = function (req, res) {
	var data = getParams(req.query)
	//console.log(req.query)
	//console.log('addStep: ', data)
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_Update]',
        params: data
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			console.log(result)			
			result = result[0][0].Result
			if(result.split('|')[0] != 'OK') {
				console.log("ERROR: ", result.split('|')[1]);
				res.status(500).json({ type: '', message: result.split('|')[1] });
			} else {				
				req.query.StepID = result.split('||')[1]
				if(req.query.dependsON != undefined && req.query.dependsON != 1) {
					exports.saveDependences(req, res)
				} else res.status(200).json(result.split('||')[1])	
			}
        })
		.catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.saveDependences = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_UpdateDependencies]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'StepId', type: 'Int', value: req.query.StepID},
				{name: 'DependsOn', type: 'Int', value: req.query.dependsON},
				{name: 'Checked', type: 'Int', value: 1},
				{name: 'LocalTransaction', type: 'Int', value: 0}
            ],
			output : [ {name: 'Result', type: 'NVarchar', value: ''} ]
        }
    };
	
	//console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json({ result: result[0], stepId: req.query.StepID});
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.getDependenceConditions = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_GetConditionsDependencies]',
        params: {
            input: [
                {name: 'StepId', type: 'Int', value: req.query.stepId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.addDependenceConditions = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_DependencyConditionalAdd]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.query.stepId},
                {name: 'DependsOn', type: 'Int', value: req.query.dependsOn},
                {name: 'IdField', type: 'Int', value: req.query.fieldId},
                {name: 'IdOperator', type: 'Int', value: req.query.operatorId},
                {name: 'Value', type: 'NVarchar', value: req.query.value},
            ]
        }
    };
	
	//console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
} 

exports.removeDependenceConditions = function (req, res) {
	console.log(req.params)
	var config = {
        spName: '[dbo].[Steps_Dependencies_Conditions_E]',
        params: {
            input: [
                {name: 'IdCondition', type: 'Int', value: req.params.conditionId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
} 

function getParams(data) {
	var input = new Array(), params = { input: input };		
	input.push({name: 'IdUser', type: 'Int', value: 1})
    input.push({name: 'IdLanguage', type: 'Int', value: 1})
    input.push({name: 'WorkflowId', type: 'Int', value: data.WorkflowID})
    input.push({name: 'StepId', type: 'Int', value: data.StepId})
    input.push({name: 'Title', type: 'NVarchar', value: data.Title})
    input.push({name: 'Description', type: 'NVarchar', value: data.Description})
    input.push({name: 'StartDate', type: 'NVarchar', value: data.StartDate})
    input.push({name: 'EndDate', type: 'NVarchar', value: data.EndDate})
    input.push({name: 'GenerateAlerts', type: 'Int', value: data.GenerateAlerts == 'true' ? 1 : 0})
    input.push({name: 'allowDocs', type: 'Int', value: data.AllowDocs == 'true' ? 1 : 0})
    input.push({name: 'timeBeforeAlertId', type: 'Int', value: data.TimeBeforeAlertId})
    input.push({name: 'extraReceptors', type: 'NVarchar', value: data.ExtraReceptorsMails})
    input.push({name: 'Days', type: 'Int', value: data.Dias})
    input.push({name: 'IdForm', type: 'Int', value: data.IdForm})
    input.push({name: 'FormReadOnly', type: 'Int', value: data.FormReadOnly == 'true' ? 1 : 0})
    input.push({name: 'WorkFlowNested', type: 'Int', value: data.WorkFlowNested})
    input.push({name: 'NumerationType', type: 'Int', value: data.NumerationType})
    input.push({name: 'RechazoEnFormularios', type: 'Int', value: data.RechazoEnFormularios == 'true' ? 1 : 0})
    input.push({name: 'NotificaViaMail', type: 'Int', value: data.NotificaViaMail == 'true' ? 1 : 0})
    input.push({name: 'MailsaNotificar', type: 'NVarchar', value: data.MailsaNotificar == null ? '' : data.MailsaNotificar})
    input.push({name: 'NotificaAprobaciones', type: 'Int', value: data.NotificaAprobaciones == 'true' ? 1 : 0})
    input.push({name: 'NotificaRechazos', type: 'Int', value: data.NotificaRechazos == 'true' ? 1 : 0})
	input.push({name: 'Idtemplate', type: 'Int', value: data.IdTemplate})
    input.push({name: 'LocalTransaction', type: 'Int', value: 0})
	params.output = [{name: 'Step_Update', type: 'NVarchar', value: ''}]
	
	return params
}

exports.getGroupsByStep = function (req, res) {
	var config = {
        spName: '[dbo].[Steps_Groups_TX_StepID]',
        params: {
            input: [
                {name: 'StepID', type: 'Int', value: req.query.stepId},
				{name: 'AllGroups', type: 'Int', value: 1},
				{name: 'FilterByGroup', type: 'NVarchar', value: ''},
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.setGroups = function (req, res) {
	var config = {
        spName: '[dbo].[Steps_Groups_MX_Add]',
        params: {
            input: [
				{name: 'User', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.query.stepId},
				{name: 'Groups', type: 'NVarchar', value: req.query.groups}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			result = result[0][0].Result
			if(result != "OK") {
				console.log("ERROR: " + result);
				res.status(500).json({ type: error.name, message: error.message });
			} else {
				res.status(200).json(result[0][0]);
			}
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.getUsersByStep = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_Users]',
        params: {
            input: [
                {name: 'StepId', type: 'Int', value: req.query.stepId},
				{name: 'AllUser', type: 'Int', value: 0},
				{name: 'FilterByUser', type: 'NVarchar', value: ''},
				{name: 'File', type: 'NVarchar', value: ''}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.setUsers = function (req, res) {
	var config = {
        spName: '[dbo].[Steps_Users_MX_Mandatory]',
        params: {
            input: [
				{name: 'User', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.query.stepId},
				{name: 'Users', type: 'NVarchar', value: req.query.users}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			result = result[0][0].Result
			if(result != "OK") {
				console.log("ERROR: " + result);
				res.status(500).json({ type: error.name, message: error.message });;
			} else {
				res.status(200).json(result[0][0]);
			}
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.remove = function (req, res) {
    var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_Delete]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: 1},
                {name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'StepId', type: 'Int', value: req.params.stepId}
            ]
        }
    };

    DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.stepId);
            console.log(result)
            result = result[0][0]
            if(result.Status != 'OK') {
                console.log("ERROR: " + result.Error);
                res.status(500).json({ type: '', message: result.Error });
            } else res.status(200).json(result.Status);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.getUnion = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.Step_MyStepsOr]',
        params: {
            input: [
				{name: 'StepId', type: 'Int', value: req.query.stepId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.updateUnion = function (req, res) {
	var config = {
        spName: '[dbo].[ActualizacionPasosParalelos]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.query.idUser},
                {name: 'IdLanguage', type: 'Int', value: 1},
                {name: 'XmlData', type: 'NVarchar', value: req.query.parallels}
            ]
        }
    };
	
	//console.log(req.query)
	
	DBUtil.executeSp(config)
		.then(function (result) {
			console.log("Then " + config.spName);
			res.status(200).json(result[0]);
		})
		.catch(function (error) {
			console.log("ERROR: " + error);
			res.status(500).json({ type: error.name, message: error.message });
		});
}

