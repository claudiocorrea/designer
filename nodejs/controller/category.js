var DBUtil = require('../util/DBUtil');

exports.getAll = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.Category.GetAll]',
        params: {
            input: []
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            res.status(500).json({success: false, error: error});
        });
}

exports.remove = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.Categories.Delete]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'IdCategory', type: 'Int', value: req.params.idCategory}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.idCategory);
			console.log(result)
			result = result[0][0]
			if(result.Status == 'Error') {
				console.log("ERROR: " + result.Error);
				res.status(500).json({success: false, error: result.Error});
			} else {
				res.status(200).json(result);
			}
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.save = function (req, res) {
	console.log(req.query)
	var parameters = [
		{name: 'IdUser', type: 'Int', value: 1},
		{name: 'IdLanguage', type: 'Int', value: 1}
	]
	
	if(req.query.CategoryID != '0')
		parameters.push({name: 'CategoryID', type: 'Int', value: req.query.CategoryID})
	
	parameters.push({name: 'Title', type: 'NVarchar', value: req.query.Title})
	parameters.push({name: 'Description', type: 'NVarchar', value: req.query.Description})
	parameters.push({name: 'IdEmpresa', type: 'Int', value: req.query.IdEmpresa})
	//parameters.push({name: 'Token', type: 'NVarchar', value: req.query.token})
	
	var config = {
        spName: req.query.CategoryID != '0' ? '[dbo].[WorkFlowRules.Categories.Save]' : '[dbo].[WorkFlowRules.Categories.Add]',
        params: {
            input: parameters
        }
    };
	
	console.log(parameters)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200);
            res.json(result[0]);
        })
        .catch(function (error) {
            res.status(500);
            res.json({success: false, error: error});
        });
}
