var DBUtil = require('../util/DBUtil');

exports.getAll = function (req, res) {
	var config = {
        spName: '[dbo].[Empresas_TT]',
        params: {
            input: []
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200);
            res.json(result[0]);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.remove = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.Company.Delete]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'IdEmpresa', type: 'Int', value: req.params.idEmpresa}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.idEmpresa);
			console.log(result)
			result = result[0][0]
			if(result.Status == 'Error') {
				console.log("ERROR: " + result.Error);
				res.status(500).json({success: false, error: result.Error});
			} else {
				res.status(200).json(result);
			}
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.save = function (req, res) {
	var parameters = [
		{name: 'IdUser', type: 'Int', value: 1},
		{name: 'IdLanguage', type: 'Int', value: 1}
	]
	if(req.query.IdEmpresa != '0')
		parameters.push({name: 'IdEmpresa', type: 'Int', value: req.query.IdEmpresa})
	
	parameters.push({name: 'NombreEmpresa', type: 'NVarchar', value: req.query.Descripcion})
	parameters.push({name: 'Habilitado', type: 'Int', value: req.query.Enabled == 'true' ? 1 : 0})
	parameters.push({name: 'Codigo', type: 'Int', value: req.query.Codigo == '' ? 0 : req.query.Codigo})
	//parameters.push({name: 'Token', type: 'NVarchar', value: req.query.token})
	
	var config = {
        spName: req.query.IdEmpresa != '0' ? '[dbo].[WorkFlowRules.Company.save]' : '[dbo].[WorkFlowRules.Company.Add]',
        params: {
            input: parameters
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			console.log(result)
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
            console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}
