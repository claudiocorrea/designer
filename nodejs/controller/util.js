var fs = require('fs'), util = {};

exports.theme = function (req, res) {
	console.log('Cambiando theme....')
	console.log(req.query)
	
	var baseColor = req.query.ui == 'dark' ? 'dimgray' : 'dynamic(#1E90FF)'
	
	var txt = "$enable-font-awesome: dynamic(true);\n" + 
		"$base-color: " + baseColor + ";\n" +
		"$base-color2: dynamic(#3F505F);\n\n" +

		"$second-color: #F6F6F6;\n" +
		"$second-color2: " + (req.query.ui == 'dark' ? 'black' : 'dimgray') + ";\n" +
		"$second-color3: #E4E4E4;\n" +
		"$second-color4: #919191;\n\n" +

		"$login-background-color: $second-color2;\n" +
		"$login-image-subpanel: url(../resources/images/wallpaper.jpg);\n" +
		"$login-textfield-color: $base-color;\n\n" +

		"$title-color: $second-color2;\n" +
		"$title-section-color: $second-color2;\n\n" +

		"$button-circular-color: $second-color;\n" +
		"$button-font-color: $second-color4;\n" +
		"$button-header-color: white;\n"
	
	fs.writeFile(global.StylePath, txt, function (err) {
		if (err) { return console.log(err); }
		console.log("The file was saved!");
	});
	
	res.status(200).json('OK');
}

