var DBUtil = require('../util/DBUtil');

exports.getAll = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.All]',
        params: {
            input: [
                {name: 'justInactives', type: 'Int', value: 0},
				{name: 'Title', type: 'NVarchar', value: ''},
				{name: 'AllFields', type: 'Int', value: 1}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) { 
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.add = function (req, res) {
	var data = getParams(req.query)
	var config = {
        spName: (req.query.WorkflowID != 0) ? '[dbo].[WorkFlowRules.WorkFlow.save]' : '[dbo].[WorkFlowRules.WorkFlow.add]',
        params: data
    };
	
	//console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			//console.log(result);
			result = result[0][0].Result
			if(result.split('|')[0] != 'OK') {
				console.log("ERROR: ", result.split('|')[1]);
				res.status(500).json({ type: '', message: result.split('|')[1] });
			} else res.status(200).json(result.split('||')[1])	
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

function getParams(data) {
	var input = new Array(), params = { input: input };	
	input.push({name: 'IdUser', type: 'Int', value: data.idUser})
	input.push({name: 'IdLanguage', type: 'Int', value: 1})
	if(data.WorkflowID != 0) input.push({name: 'WorkflowID', type: 'Int', value: data.WorkflowID})
	input.push({name: 'Title', type: 'NVarchar', value: data.Title})
	input.push({name: 'Description', type: 'NVarchar', value: data.Description})
	input.push({name: 'CategoryID', type: 'NVarchar', value: data.CategoryID})
	input.push({name: 'StartDate', type: 'NVarchar', value: data.StartDate})
	input.push({name: 'EndDate', type: 'NVarchar', value: data.EndDate})
	if(data.WorkflowID != 0) input.push({name: 'IdState', type: 'Int', value: data.IdState})
	input.push({name: 'RelativeDates', type: 'Int', value: data.RelativeDates == 'true' ? 1 : 0})
	input.push({name: 'Original', type: 'Int', value: 1})
	input.push({name: 'CanBeNode', type: 'Int', value: data.CanBeNode == 'true' ? 1 : 0})
	if(data.WorkflowID == 0) input.push({name: 'Referencia', type: 'NVarchar', value: ''})
		else input.push({name: 'Reference', type: 'NVarchar', value: ''})
	if(data.WorkflowID == 0) input.push({name: 'GeneraAlertas', type: 'Int', value: data.GeneraAlertas == 'true' ? 1 : 0})
		else input.push({name: 'GenerateAlerts', type: 'Int', value: data.GeneraAlertas == 'true' ? 1 : 0})
	if(data.WorkflowID == 0) input.push({name: 'UsaCondicionalesPasos', type: 'Int', value: data.UsaCondicionalesPasos == 'true' ? 1 : 0})
		else input.push({name: 'UseConditionalsInSteps', type: 'Int', value: data.UsaCondicionalesPasos == 'true' ? 1 : 0})
	if(data.WorkflowID == 0) input.push({name: 'UsaCondicionalesUsuarios', type: 'Int', value: data.UsaCondicionalesUsuarios == 'true' ? 1 : 0})	
		else input.push({name: 'UseConditionalsInUsers', type: 'Int', value: data.UsaCondicionalesUsuarios == 'true' ? 1 : 0})
	if(data.WorkflowID == 0)  input.push({name: 'LocalTransaction', type: 'Int', value: 0})
	params.output = [{name: 'Result', type: 'NVarchar', value: ''}]
	return params	
}

exports.remove = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.delete]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'WorkflowID', type: 'Int', value: req.params.workflowId}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.workflowId);
			console.log(result)
			result = result[0][0]
			if(result.Status != 'OK') {
				console.log("ERROR: " + result.Error);
				res.status(500).json({ type: '', message: result.Error });
			} else res.status(200).json(result.Status);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.clone = function (req, res) {
	console.log(req.query)
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.CloneFull]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'MyNewName', type: 'NVarchar', value: req.query.name},
				{name: 'WorkflowId', type: 'Int', value: req.query.workflowId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) { 
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.publish = function (req, res) {
	console.log(req.query)
	var config = {
        spName: '[dbo].[WorkFlowRules.WorkFlow.RefreshData]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: req.query.idUser},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'WorkflowId', type: 'Int', value: req.query.workflowId},
				{name: 'Full', type: 'Int', value: 1},
				{name: 'LocalTransaction', type: 'Int', value: 0}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) { 
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}
