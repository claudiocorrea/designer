var DBUtil = require('../util/DBUtil');

exports.getAll = function (req, res) {
	var config = {
        spName: '[dbo].[OrigenesDeDatos_TT]',
        params: {
            input: [ ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) { 
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}
/*
exports.add = function (req, res) {
	var data = getParams(req.query)
	var config = {
        spName: (req.query.IdUser != 0) ? '[dbo].[WorkFlowRules.Users.save]' : '[dbo].[WorkFlowRules.Users.add]',
        params: data
    };
	
	console.log(config.params.input)
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			console.log(result);
			result = result[0][0]			
			if(result.Resultado != 'OK') {
				console.log("ERROR: ", result.Error);
				res.status(500).json({ type: '', message: result.Error });
			} else res.status(200).json(result)	
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

function getParams(data) {
	console.log(data)
	var input = new Array(), params = { input: input };	
	input.push({name: 'IdUser', type: 'Int', value: data.idUser})
	input.push({name: 'IdLanguage', type: 'Int', value: 1})
	if(data.IdUser != 0) input.push({name: 'IdUserToModify', type: 'Int', value: data.IdUser})
	input.push({name: 'Login', type: 'NVarchar', value: data.Login})
	input.push({name: 'FirstName', type: 'NVarchar', value: data.FirstName})
	input.push({name: 'LastName', type: 'NVarchar', value: data.LastName})
	input.push({name: 'SPUser', type: 'NVarchar', value: data.SPUser ? data.SPUser : ''})	
	input.push({name: 'Enabled', type: 'Int', value: data.Enabled == 'true' ? 1 : 0})
	input.push({name: 'Email', type: 'NVarchar', value: data.Email})
	input.push({name: 'IdProfile', type: 'Int', value: data.IdProfile})
	if(data.IdUser == 0) input.push({name: 'Password', type: 'NVarchar', value: data.PasswordSql})
	input.push({name: 'Legajo', type: 'NVarchar', value: data.Legajo})
	input.push({name: 'Inbox', type: 'Int', value: 1})
	input.push({name: 'TipoSistema', type: 'Int', value: data.TipoSistema == 'true' ? 1 : 0})
	input.push({name: 'Token', type: 'NVarchar', value: data.token})
	if(data.WorkflowID == 0)  input.push({name: 'NoTransaction', type: 'Int', value: 0})
	return params	
}

exports.remove = function (req, res) {
	var config = {
        spName: '[dbo].[WorkFlowRules.Users.delete]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'IdUserToDelete', type: 'Int', value: req.params.IdUser},
				{name: 'Token', type: 'NVarchar', value: req.params.token}
			]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName + ': ' + req.params.IdUser);
			console.log(result)
			result = result[0][0]
			console.log(result)
			if(result.Resultado != 'OK') {
				console.log("ERROR: " + result.Error);
				res.status(500).json({ type: '', message: result.Error });
			} else res.status(200).json(result.Resultado);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.clone = function (req, res) {
	console.log(req.query)
	var config = {
        spName: '[dbo].[WorkFlowRules.Users.Clone]',
        params: {
            input: [
                {name: 'IdUser', type: 'Int', value: 1},
				{name: 'IdLanguage', type: 'Int', value: 1},
				{name: 'MyNewName', type: 'NVarchar', value: req.query.name},
				{name: 'WorkflowId', type: 'Int', value: req.query.workflowId}
            ]
        }
    };
	
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0]);
        })
        .catch(function (error) { 
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}
*/