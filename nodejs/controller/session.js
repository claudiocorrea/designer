var DBUtil = require('../util/DBUtil');

exports.login = function (req, res) {
	var config = {
        spName: '[dbo].[Queries_asmx.Login_ORG]',
        params: {
            input: [
				{name: 'UserName', type: 'NVarChar', value: req.query.username},
				{name: 'PassWord', type: 'NVarChar', value: req.query.password}
            ]
        }
    };
	req.session.user = null;
	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
			req.session.user = result[0][0]
            res.status(200).json(result[0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.validateToken = function (req, res) {
	var config = {
        spName: '[dbo].[Tokens_TX_ValidateUser]',
        params: {
            input: [
				{name: 'IdUser', type: 'Int', value: req.query.userId},
				{name: 'Token', type: 'NVarChar', value: req.query.token}
            ]
        }
    };

	DBUtil.executeSp(config)
        .then(function (result) {
            console.log("Then " + config.spName);
            res.status(200).json(result[0][0]);
        })
        .catch(function (error) {
			console.log("ERROR: " + error);
            res.status(500).json({ type: error.name, message: error.message });
        });
}

exports.validate = function (req, res) {
	console.log("Validate session... ")
	console.log(req.session)
    if (req.session && req.session.user) {
        res.status(200);
        res.send({success: true, user: req.session.user });
    } else {
        res.status(401);
        res.send({success: false});
    }
}

exports.invalidate = function (req, res) {
    req.session.user = null;
    req.session.destroy();
    res.status(200);
    res.send({success: true});
}
