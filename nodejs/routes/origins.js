var express = require('express'),
    routerOrigins = express.Router(),
    originsController = require('../controller/origins');

routerOrigins.get('/getAll', originsController.getAll);
//routerOrigins.post('/newOrigin', originsController.add);
//routerOrigins.delete('/deleteOrigin/:IdOrigin', originsController.remove);

module.exports = routerOrigins;