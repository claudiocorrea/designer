var express = require('express'),
    routerStores = express.Router(),
    storesController = require('../controller/stores'),
    enterpriseController = require('../controller/enterprise'),
	categoriesController = require('../controller/category');

routerStores.get('/getAllApprovalsTemplates', storesController.getAllApprovalsTemplates);
routerStores.get('/getAllFormTypes', storesController.getAllFormTypes);
routerStores.get('/getAllOperators', storesController.getAllOperators);
routerStores.get('/getAllProfiles', storesController.getAllProfiles);
routerStores.get('/getAllTimeAlerts', storesController.getAllTimeAlerts);

routerStores.get('/getAllEnterprises', enterpriseController.getAll);
routerStores.post('/newEnterprise', enterpriseController.save);
routerStores.delete('/deleteEnterprise/:idEmpresa', enterpriseController.remove);

routerStores.get('/getAllCategories', categoriesController.getAll);
routerStores.post('/newCategory', categoriesController.save);
routerStores.delete('/deleteCategory/:idCategory', categoriesController.remove);


module.exports = routerStores;