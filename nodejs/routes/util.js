var express = require('express'),
    routerUtil = express.Router(),
    utilController = require('../controller/util');

routerUtil.get('/theme', utilController.theme);

module.exports = routerUtil;