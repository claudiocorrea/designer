var express = require('express'),
    routerWorkflow = express.Router(),
    workflowController = require('../controller/workflow');

routerWorkflow.get('/getAllWorkflows', workflowController.getAll);
routerWorkflow.post('/newWorkflow', workflowController.add);
routerWorkflow.delete('/deleteWorkflow/:workflowId', workflowController.remove);
routerWorkflow.get('/publish', workflowController.publish);

routerWorkflow.post('/cloneWorkflow', workflowController.clone);

module.exports = routerWorkflow;