var express = require('express'),
    routerForm = express.Router(),
    formController = require('../controller/form');

routerForm.get('/getAll', formController.getAll);
routerForm.post('/newForm', formController.add);
routerForm.delete('/deleteForm/:formId', formController.remove);

routerForm.get('/getFieldsByFormId', formController.getFieldsByFormId);
routerForm.put('/updateFields/:fieldId', formController.updateFields);



routerForm.get('/getFormFields', formController.getExtJSFormFields);
routerForm.post('/saveFormFields', formController.saveFormFields);

module.exports = routerForm;