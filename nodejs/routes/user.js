var express = require('express'),
    routerUser = express.Router(),
    userController = require('../controller/user');

routerUser.get('/getAll', userController.getAll);
routerUser.post('/newUser', userController.add);
routerUser.delete('/deleteUser/:IdUser', userController.remove);

routerUser.post('/cloneUser', userController.clone);

module.exports = routerUser;