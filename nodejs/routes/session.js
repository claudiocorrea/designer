var express = require('express'),
    routerSession = express.Router(),
    sessionController = require('../controller/session');

routerSession.get('/validate', sessionController.validate);
routerSession.post('/login', sessionController.login);
routerSession.post('/validateToken', sessionController.validateToken);
routerSession.get('/invalidate', sessionController.invalidate);

module.exports = routerSession;