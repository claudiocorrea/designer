var express = require('express'),
    routerStep = express.Router(),
    stepController = require('../controller/step');

routerStep.get('/getSteps', stepController.getStepsByWorkflowId);
routerStep.post('/newStep', stepController.add);
routerStep.delete('/deleteStep/:stepId', stepController.remove);

routerStep.get('/getGroupsByStep', stepController.getGroupsByStep);
routerStep.put('/setGroups/:groupId', stepController.setGroups);

routerStep.get('/getUsersByStep', stepController.getUsersByStep);
routerStep.put('/setUsers/:userId', stepController.setUsers);

routerStep.get('/getDependenceConditions', stepController.getDependenceConditions);
routerStep.post('/addDependenceConditions', stepController.addDependenceConditions);
routerStep.delete('/removeDependenceConditions/:conditionId', stepController.removeDependenceConditions);

routerStep.get('/getUnion', stepController.getUnion);
routerStep.post('/updateUnion', stepController.updateUnion);


module.exports = routerStep;