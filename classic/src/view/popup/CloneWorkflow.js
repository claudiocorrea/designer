Ext.define('Designer.view.popup.CloneWorkflow', {
    extend: 'Ext.window.Window',
	
	requires: [ ],
	
	reference: 'cloneWorkflowPopup',
	controller: 'cloneWorkflowPopup',
	
	y: 20,
	height: '95%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	plugins: 'responsive',
	responsiveConfig: {
		'width >= 1000': { width: '60%' },
		'width < 1000': { width: '80%' }
	},

    bind: { title: '{txtFields.titles.cloneWorkflow}' },
    modal: true,
	scrollable: 'vertical',

    items : [
		{
            xtype: 'form',
            reference: 'workflowForm',
			border: false,
			bodyPadding: 10,			
			defaults: {
				width: '100%'
			},
            items: [
				{	xtype: 'textfield',
					name: 'Title',
					bind: { fieldLabel: '{txtFields.words.title}' },
					emptyText: '',
					allowBlank: false
				},{ 	
					xtype: 'fieldcontainer', 
					layout: 'hbox',
					items: [
						{ 	xtype: 'datefield',
							name: 'StartDate',
							reference: 'dfStartDate',
							bind: { fieldLabel: '{txtFields.titles.startDate}' },
							width: 250,
							format: 'd/m/Y',
							allowBlank: false,
							minValue: new Date()
						}, {
							xtype:'button',
							userCls: 'circular', margin: '0 10',
							iconCls:'x-fa fa-calendar-check-o',
							bind: { tooltip: '{txtFields.words.date}s' },
							handler: ''
						}
					]
				}, { 	xtype: 'panel', reference: 'panelList',
						layout: { type: 'accordion', animate: true, vertical: true },
						defaults: {	 width: '100%' }
				}
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			style: 'border: none; background-color: red;',
            handler: 'deleteWorkflow',
			hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
			formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})