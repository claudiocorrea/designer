Ext.define('Designer.view.popup.Parallels', {
    extend: 'Ext.window.Window',
	
	requires: [ ],
	
	id: 'parallelsPopup',
	controller: 'parallelsPopup',
	
	width: 560,
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.titles.parallelNodes}' },
    modal: true,

    items : [
		{ 	xtype: 'form', reference: 'parallelsForm',
            bodyPadding: 10, layout: 'hbox',
			defaults: { xtype: 'radio', labelWidth: 70},
            items: [
                { 	id: 'rdY', fieldLabel: 'Union', boxLabel: 'Y',
					name: 'union', inputValue: 1, 
					margin: '0 20', checked: true
				}, {
					id: 'rdO', boxLabel: 'O', name: 'union',
					inputValue: 0
				}, {
					xtype:'button', margin: '0 20',
					userCls: 'circular', iconCls:'x-fa fa-save',
					bind: { tooltip: '{txtFields.words.save}' },
					handler: 'escribeConsole', hidden: true
				}
            ]
        }, {  	
			xtype: 'panel', reference: 'panelList', layout: 'vbox', margin: '0 0 10 0',
			bodyPadding: 5, defaults: { width: '100%', margin: 5 },
			tbar: { baseCls: 'screenBackground',
				items: [
					{ 	xtype: 'label', cls: 'titleSection', margin: 10,
						bind: { text: '{txtFields.titles.parallelStep}' }
					}
				]
			}
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			//style: 'border: none; background-color: red;',
            handler: 'delete', hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
            formBind: true, handler: 'save'
        }, {
            bind: { text: '{txtFields.words.close}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})

function createParallelsProperties(data) {
    var win = Ext.getCmp('parallelsPopup')
    if (!win) {
        win = new Designer.view.popup.Parallels({
			dataStep: data
		});
    }
	win.show()
}

function deleteGatewayPopup(data) {
	var win = Ext.getCmp('parallelsPopup')
	if (!win) {
        win = new Designer.view.popup.Parallels({
			dataStep: data
		});
    }
	win.controller.loadData()
	win.controller.delete()
}
