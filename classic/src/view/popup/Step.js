Ext.define('Designer.view.popup.Step', {
    extend: 'Ext.window.Window',
	
	requires: [ 
		'Designer.store.Forms',
		'Designer.store.TimeAlerts',
        'Designer.store.ApprovalsTemplates',
		'Designer.store.Operators'
	],
	
	id: 'stepPopup',
	controller: 'stepPopup',
	
	y: 20,
	width: '60%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.properties}' },
    modal: true,
	closable: false,

    items : [
		{ 	xtype: 'form',
            reference: 'stepForm',
			layout: {
                type: 'accordion',
                animate: true,
                //activeOnTop: true,
                vertical: true
            },
            border: false,			
			defaults: { height: 400, bodyPadding: 10, scrollable: 'vertical' },
            items: [				
				{ 	bind: { title: '{txtFields.words.information}' }, 
					defaults: {	 width: '100%' }, 
					items: [
						{ 	xtype: 'textfield',
							bind: { fieldLabel: '{txtFields.words.name}' },
							name: 'Title',
							emptyText: '',
							allowBlank: false
						}, {
                            xtype: 'textarea',
                            bind: { fieldLabel: '{txtFields.words.description}' },
                            name: 'Description',
                            emptyText: '',
                            allowBlank: false
                        }, {
                            xtype: 'fieldcontainer',
                            bind: { fieldLabel: '{txtFields.words.day}s' },
							layout: 'hbox',
                            defaults: { hideLabel: 'true' },
                            items: [
                                { 	xtype: 'numberfield',
                                    name: 'Dias', width: 100,
									value: 1,
                                    minValue: 1, maxValue: 60,
                                    allowBlank: false
                                }, {
                                    xtype: 'checkboxfield',
                                    name: 'AllowDocs',
									bind: { boxLabel: '{txtFields.titles.externDocs}' },
                                    margin: '0 0 0 20',
                                    inputValue: true
                                }
                            ]
                        }, {
							xtype: 'combobox',
							bind: { fieldLabel: '{txtFields.words.form}' },
							name: 'IdForm',
							reference: 'cbForms',
							store: { type: 'forms' },
							valueField: 'IdForm', displayField: 'FormName',
							queryMode: 'remote', queryCaching: true,
							allowBlank: false, validateBlank: false,
							editable: false
						}, {
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaults: {
								xtype: 'checkboxfield',
								//hideLabel: 'true',
								inputValue: true,
								margin: '0 50 0 0'
							},
							margin: '0 0 10 0',
							items: [
								{ 	name: 'FormReadOnly',
									bind: { fieldLabel: '{txtFields.titles.onlyRead}' }
								}, {
									name: 'RechazoEnFormularios', labelWidth: 150,
									bind: { fieldLabel: '{txtFields.titles.rejectionInForm}' }
								}
							]
						}, {
							xtype: 'combobox',
							reference: 'numerationType', name: 'NumerationType',
							bind: { fieldLabel: '{txtFields.titles.selfEnumeration}' },									
							store: Ext.create('Ext.data.Store', {
								fields: ['id', 'name'],
								data : [ {"id": 0, "name":"Manual"}, {"id": 1, "name":"Secuencial"}, {"id": 2, "name":"Random"} ]
							}), valueField: 'id', displayField: 'name', value: 2,
							queryMode: 'local', queryCaching: false, editable: false
						}, {
							xtype: 'combobox',
							reference: 'cbDependsOn', name: 'dependsON',
							bind: { fieldLabel: '{txtFields.titles.dependsOn}' },									
							valueField: 'id', displayField: 'name', validateBlank: false,
							queryMode: 'local', queryCaching: false, editable: false
						},
						{   xtype: 'hiddenfield', name: 'WorkflowID' },
						{   xtype: 'hiddenfield', name: 'StepId' },
						{   xtype: 'hiddenfield', name: 'IdState' },
						{   xtype: 'hiddenfield', name: 'StartDate' },
						{   xtype: 'hiddenfield', name: 'EndDate' },
						{   xtype: 'hiddenfield', name: 'Groups' },
						{   xtype: 'hiddenfield', name: 'Users' }
					]
				},
				{ 	bind: { title: '{txtFields.titles.alertAndNotifications}' }, 
					defaults: { width: '100%' },
					items: [
						{	xtype: 'fieldcontainer',
							layout: 'hbox',
                            defaults: { hideLabel: true },
                            items: [
                                {  	xtype: 'checkboxfield',
									name: 'GenerateAlerts',
									reference: 'ckbAlerts',
									bind: { fieldLabel: '{txtFields.titles.gralAlerts}' },
									labelAlign: 'center', inputValue: true,
									margin: '0 20 0 0'
								}, {
									xtype: 'combobox', width: 310,
									name: 'TimeBeforeAlertId', reference: 'cbTimesAlerts',									
									store: { type: 'timeAlerts' },
									valueField: 'TimeBeforeAlertsID', displayField: 'Title',
									queryMode: 'remote', queryCaching: true, editable: false,
									bind: { fieldLabel: '{txtFields.titles.timeAlerts}',
									disabled: '{!ckbAlerts.checked}' }, allowBlank: false
								}
                            ]
						}, { 	
							xtype: 'combobox', name: 'IdTemplate', reference: 'cbTemplate',
							bind: {  fieldLabel: '{txtFields.titles.alertTemplate}',
								disabled: '{!ckbAlerts.checked}' }, allowBlank: false,									
							store: { type: 'approvalsTemplates' }, valueField: 'IdTemplate',
							displayField: 'Name', queryMode: 'remote', queryCaching: true, editable: false,
						}, {
							xtype: 'textarea',
							fieldLabel: 'Genera Extras',
							bind: { fieldLabel: '{txtFields.titles.generateExtras}',
							disabled: '{!ckbAlerts.checked}' }, allowBlank: false,
							name: 'ExtraReceptorsMails',
							emptyText: ''
						}, {
							xtype: 'fieldset',
							bind: { title: '{txtFields.titles.notifications}' },							
							defaults: { anchor: '100%' },
							margin: '-10 0 0 0',
							items: [
								{ 	xtype: 'fieldcontainer',
									layout: 'hbox',
									defaults: {
										xtype: 'checkboxfield',
										hideLabel: 'true',
										inputValue: true,
										value: false, margin: '-8 0 0 20'
									},
									items: [
										{ 	name: 'NotificaAprobaciones', reference: 'chkNoficaAprob', bind: { boxLabel: '{txtFields.words.approved}' } },
										{ 	name: 'NotificaRechazos', reference: 'chkNoficaRechazo', bind: { boxLabel: '{txtFields.words.rejection}s' } },
										{ 	name: 'NotificaViaMail', reference: 'chkNoficaMail',
											bind: { boxLabel: '{txtFields.titles.viaMail}' }
										}
									]
								}, { 	
									xtype: 'combobox', name: 'IdTemplateAlerts', reference: 'cbTemplateAlerts',									
                                    store: { type: 'approvalsTemplates' }, valueField: 'IdTemplate',
                                    displayField: 'Name', queryMode: 'remote', queryCaching: true, editable: false,
									bind: { fieldLabel: '{txtFields.titles.approvalTemplate}',
										disabled: '{!chkNoficaAprob.checked && !chkNoficaRechazo.checked && !chkNoficaMail.checked}'
									}, allowBlank: false
								}, {
									xtype: 'textarea',
									name: 'MailsaNotificar',
									emptyText: '', allowBlank: false,
									bind: { disabled: '{!chkNoficaMail.checked}' }
								}
							]
						}
					]
				},
				{ 	bind: { title: '{txtFields.titles.enableFields}' }, 
					defaults: { width: '100%' },
					bbar: {	
						baseCls: 'screenBackground', padding: 5,
						items: [
							'->', {   xtype:'button', 
								iconCls:'x-fa fa-save', userCls: 'circular',
								bind: { tooltip: '{txtFields.words.save}' },
								handler: 'saveFormFields'
							}, '->'
						]
					},
					items: [ { xtype: 'gridFields', flex: 1} ]
				},
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			//style: 'border: none; background-color: red;',
            handler: 'delete', hidden: true
        },
        '->', {
            bind: { text: '{txtFields.words.save}' },
            formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})

function createStepProperties(data) {
    var win = Ext.getCmp('stepPopup')
    if (!win) {
        win = new Designer.view.popup.Step({
			dataStep: data
		});
    }
	win.show()
}

function deleteStepPopup(data) {
	var win = Ext.getCmp('stepPopup')
	if (!win) {
        win = new Designer.view.popup.Step({
			dataStep: data
		});
    }
	win.controller.delete()
}