Ext.define('Designer.view.popup.Depends', {
    extend: 'Ext.window.Window',
	
	requires: [
		'Designer.store.FormFields'
	],
	
	id: 'dependsPopup',
	controller: 'dependsPopup',
	
	y: '20%',
	width: 560,
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.dependency}' },
    modal: true,
	closable: false,

    items : [
		{
            xtype: 'form',
            reference: 'dependsForm',
			border: false,
			bodyPadding: 10,			
			defaults: { width: '100%' },
			bbar: {
				baseCls: 'screenBackground',
				items: [
					'->', {
						xtype:'button', bind: { tooltip: '{txtFields.words.save}' },
						userCls: 'circular', iconCls:'x-fa fa-save', margin: 5,						
						handler: 'save'
					}, '->'
				]
			},
            items: [
				{ 	
					xtype: 'fieldcontainer', 
					layout: 'hbox',					
					items: [
						{	xtype: 'fieldcontainer', bind: { fieldLabel: '{txtFields.words.condition}' },
							layout: 'hbox', 
							items: [
								{   xtype: 'combobox', name: 'field', reference: 'cbFields', flex : 1,
									store: { type: 'formFields' }, valueField: 'IdField', autoSelectLast: false,
									displayField: 'FieldName', queryMode: 'remote', editable: false, allowBlank: false
								},
								{ 	xtype: 'combobox', name: 'condition', reference: 'cbDependCondition',									
									store: { type: 'operators' }, valueField: 'IdOperator', width: 70, margin: '0 10 0 10',
									displayField: 'Description', queryMode: 'remote', queryCaching: true, editable: false, allowBlank: false
								},
								{ 	xtype: 'textfield', name: 'value', emptyText: '', flex : 1, allowBlank: false }
							]
						}
					]
				}
			]
		}, { xtype: 'panel', reference: 'panelList', layout: 'vbox', bodyPadding: 5, defaults: { width: '100%', margin: 5 } }
	],
	
	buttons: [
		'->', {
            bind: { text: '{txtFields.words.close}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})

function createDependsProperties(data) {
    var win = Ext.getCmp('dependsPopup')
    if (!win) {
        win = new Designer.view.popup.Depends({
			dataStep: data
		});
    }
	win.show()
}
