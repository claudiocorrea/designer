Ext.define('Designer.view.popup.Form', {
    extend: 'Ext.window.Window',
	
	requires: [ 
		'Designer.store.Enterprises',
		'Designer.store.FormTypes'
	],
	
	reference: 'formPopup',
	controller: 'formPopup',
	
	y: '15%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	plugins: 'responsive',
	responsiveConfig: {
		'width >= 1000': { width: '60%' },
		'width < 1000': { width: '80%' }
	},

    bind: { title: '{txtFields.words.form}' },
    modal: true,

    items : [
		{
            xtype: 'form',
            reference: 'formForm',
            border: false,
			bodyPadding: 10,
			defaults: {
				width: '100%',
				//labelAlign: 'top',
			},
            items: [
				{   xtype: 'hiddenfield', name: 'IdForm' },
				{	xtype: 'textfield',
					name: 'FormName',
					bind: { fieldLabel: '{txtFields.words.name}' },
					emptyText: '',
					allowBlank: false
				}, {
					xtype: 'textarea',
					name: 'Description',
					bind: { fieldLabel: '{txtFields.words.description}' },
					emptyText: '',
					allowBlank: false
				}, {
					xtype: 'combo',
					bind: { fieldLabel: '{txtFields.words.enterprise}' },
					name:'IdEmpresa',
					reference: 'cbEmpresa',							
					store: { type: 'enterprises' },
					displayField: 'Descripcion',
					valueField: 'IdEmpresa',
					publishes: 'value',
					editable: false
				}, {
					xtype: 'combo',
					bind: { fieldLabel: '{txtFields.titles.styleSheet}' },
					name:'IdStyleSheet',
					reference: 'cbStyleSheet',							
					//store: { type: 'enterprises' },
					displayField: 'Descripcion',
					valueField: 'IdEmpresa',
					publishes: 'value',
					editable: false,
					hidden: true
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					defaults: { width: '50%' },							
					items: [
						{ 	xtype: 'combo',
							bind: { fieldLabel: '{txtFields.words.type}' },
							name:'IdFormType',
							reference: 'cbFormType',							
							store: { type: 'formTypes' },
							displayField: 'Description',
							valueField: 'IdFormType',
							publishes: 'value',
							editable: false,
							margin: '0 15 0 0'
						}, {
							xtype: 'numberfield',
							bind: { fieldLabel: '{txtFields.words.column}s' },
							name: 'Columnas',
							value: 0, minValue: 0, 
							margin: '0 1 0 0',
							hidden: true
						}
					]
				},
				{   xtype: 'hiddenfield', name: 'Code' },
                {   xtype: 'hiddenfield', name: 'InfoPathTemplate' },
                {   xtype: 'hiddenfield', name: 'StyleSheet' },
				{   xtype: 'hiddenfield', name: 'Original', value: true}			
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			style: 'border: none; background-color: red;',
            handler: 'deleteWorkflow',
			hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
			formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})