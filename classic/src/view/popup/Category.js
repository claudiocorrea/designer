Ext.define('Designer.view.popup.Category', {
    extend: 'Ext.window.Window',
	
	requires: [ 'Designer.store.Enterprises' ],
	
	reference: 'categoryPopup',
	controller: 'categoryPopup',
	
	y: '15%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	plugins: 'responsive',
	responsiveConfig: {
		'width >= 1000': { width: '60%' },
		'width < 1000': { width: '80%' }
	},

    bind: { title: '{txtFields.words.category}' },
    modal: true,

    items : [
		{
            xtype: 'form',
            reference: 'categoryForm',
            border: false,
			bodyPadding: 10,
			defaults: {
				width: '100%',
				labelAlign: 'top',
			},
            items: [
				{   xtype: 'hiddenfield', name: 'CategoryID' },
				{	xtype: 'textfield',
					name: 'Title',
					bind: { fieldLabel: '{txtFields.words.title}' },
					emptyText: '',
					allowBlank: false
				}, {
					xtype: 'textarea',
					name: 'Description',
					bind: { fieldLabel: '{txtFields.words.description}' },
					emptyText: '',
					allowBlank: false
				}, {
					xtype: 'combo',
					bind: { fieldLabel: '{txtFields.words.enterprise}' },
					name:'IdEmpresa',
					reference: 'cbEmpresa',							
					store: { type: 'enterprises' },
					displayField: 'Descripcion',
					valueField: 'IdEmpresa',
					publishes: 'value',
					editable: false
				}				
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			style: 'border: none; background-color: red;',
            handler: 'deleteWorkflow',
			hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
			formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})