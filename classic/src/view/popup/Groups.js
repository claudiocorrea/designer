Ext.define('Designer.view.popup.Groups', {
    extend: 'Ext.window.Window',
	
	requires: [ 
		'Designer.grid.Groups',
		'Designer.grid.MandatoryUsers'
	],
	
	reference: 'groupsPopup',
	controller: 'groupsPopup',
	
	y: 10,
	width: '60%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.group}s' },
    modal: true,
    closable: false,

    items : [
		{ 	xtype: 'form',
            reference: 'groupsForm',
			layout: {
                type: 'accordion',
                animate: true,
                //activeOnTop: true,
                vertical: true
            },
            border: false,			
			defaults: { height: 450, bodyPadding: 5, scrollable: 'vertical' },
            items: [
				{ 	bind: { title: '{txtFields.words.group}s' }, 
					defaults: { width: '100%' },
					bbar: {	
						baseCls: 'screenBackground', padding: 5,						
						items: [
							'->', {   xtype:'button', 
								iconCls:'x-fa fa-save', userCls: 'circular',
								bind: { tooltip: '{txtFields.words.save}' },
								handler: 'saveGroups'
							}, '->'
						]
					},
					items: [ { xtype: 'gridGroups', flex: 1} ]
				},
				{ 	bind: { title: '{txtFields.titles.mandatoryUsers}' }, 
					defaults: { width: '100%' },
					bbar: {	
						baseCls: 'screenBackground', padding: 5,
						items: [
							'->', {   xtype:'button', 
								iconCls:'x-fa fa-save', userCls: 'circular',
								bind: { tooltip: '{txtFields.words.save}' },
								handler: 'saveUsers'
							}, '->'
						]
					},
					items: [ { xtype: 'gridMandatoryUsers', flex: 1} ]
				}
			]
		}
	],
	
	buttons: [
        '->', /*{
            bind: { text: '{txtFields.words.save}' },
            formBind: true,
            handler: 'save'
        },*/ {
            bind: { text: '{txtFields.words.close}' },
            handler: 'close'
        }
    ],
	
	listeners:{
        afterrender: 'loadGroups'
    }
})

function createGroupsProperties(data) {
    var win = Ext.getCmp('groupsPopup')
    if (!win) {
        win = new Designer.view.popup.Groups({
			dataStep: data
		});
    }
	win.show()
}