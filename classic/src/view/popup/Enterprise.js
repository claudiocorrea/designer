Ext.define('Designer.view.popup.Enterprise', {
    extend: 'Ext.window.Window',
	
	requires: [ ],
	
	reference: 'enterprisePopup',
	controller: 'enterprisePopup',
	
	y: '15%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	plugins: 'responsive',
	responsiveConfig: {
		'width >= 1000': { width: '60%' },
		'width < 1000': { width: '80%' }
	},

    bind: { title: '{txtFields.words.enterprise}' },
    modal: true,

    items : [
		{
            xtype: 'form',
            reference: 'enterpriseForm',
            border: false,
			bodyPadding: 10,
			defaults: {						
				width: '100%',
				//labelAlign: 'top',
			},
            items: [
				{   xtype: 'hiddenfield', name: 'IdEmpresa' },
				{	xtype: 'textfield',
					name: 'Descripcion',
					bind: { fieldLabel: '{txtFields.words.title}' },
					emptyText: '',
					allowBlank: false
				}, {
					xtype: 'fieldcontainer',
					bind: { fieldLabel: '{txtFields.words.enabled}' },
					layout: 'hbox',
					defaults: { hideLabel: true	},
					items: [
						{ 	xtype: 'checkboxfield', margin: '0 30 0 0',
							name: 'Enabled', reference: 'chkEnabled', inputValue: true							
						}, {	
							xtype: 'textfield', width: '50%',
							name: 'Codigo', editable: false,
							bind: { fieldLabel: '{txtFields.words.code}' },
							hidden: true
						}
					]
				}
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			style: 'border: none; background-color: red;',
            handler: 'deleteEnterprise',
			hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
			formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})