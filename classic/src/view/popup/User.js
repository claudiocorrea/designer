Ext.define('Designer.view.popup.User', {
    extend: 'Ext.window.Window',
	
	requires: [ 
		'Designer.store.Profiles'
	],
	
	id: 'userPopup',
	controller: 'userPopup',
	
	y: 20,
	width: '60%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },

    bind: { title: '{txtFields.words.properties}' },
    modal: true,

    items : [
		{ 	xtype: 'form',
            reference: 'userForm',
			layout: {
                type: 'accordion',
                animate: true,
                //activeOnTop: true,
                vertical: true
            },
            border: false,			
			defaults: { height: 420, bodyPadding: 5 },
            items: [				
				{ 	bind: { title: '{txtFields.words.information}' }, 
					defaults: {	 width: '100%', xtype: 'textfield' }, 
					items: [
						{   xtype: 'hiddenfield', name: 'IdUser' },
						{   xtype: 'hiddenfield', name: 'SPUser' },
						{	xtype: 'panel', layout: 'column',
							items: [
								{	columnWidth: 0.3, 
									items: [
										{ 	xtype: 'image', reference: 'imageUser',
											width: 150, height: 150, margin: '0 10 10 25',
											userCls: 'circular',
											src: 'resources/images/users/HSA0.png',
											hidden: true
										}, {
											xtype:'button', reference: 'btnUser',
											margin: '0 10 0 25', userCls: 'circular',
											style: 'border: none; background-color: white;'
										}
									]
								}, {	
									columnWidth: 0.7,
									defaults: {	 width: '100%', labelAlign: 'top' }, 									
									items: [
										{ 	xtype: 'textfield',
											bind: { fieldLabel: '{txtFields.words.firstname}' },
											name: 'FirstName',
											emptyText: '',
											allowBlank: false
										}, { 
											xtype: 'textfield',
											bind: { fieldLabel: '{txtFields.words.lastname}' },
											name: 'LastName',
											emptyText: '',
											allowBlank: false
										}
									]
								}
							]
						}, { 	
							bind: { fieldLabel: '{txtFields.words.user}' },
							name: 'Login',
							emptyText: '',
							allowBlank: false
						}, {
							xtype: 'combobox',
							bind: { fieldLabel: '{txtFields.words.profile}' },
							name: 'IdProfile',
							reference: 'cbProfile',
							store: { type: 'profiles' },
							valueField: 'IdProfile', displayField: 'Description',
							queryMode: 'remote', queryCaching: true,
							allowBlank: true, validateBlank: false,
							editable: false
						}, { 	
							bind: { fieldLabel: '{txtFields.words.password}' },
							name: 'PasswordSql', emptyText: '',
							inputType: 'password'
						}, { 	
							fieldLabel: 'Email',
							name: 'Email',
							emptyText: '', vtype: 'email',
							//allowBlank: false
						}, {
							xtype: 'fieldcontainer',
							layout: 'hbox',
							defaults: {
								xtype: 'checkboxfield',
								//hideLabel: 'true',
								inputValue: true,
								margin: '0 30 0 0'
							},
							margin: '0 0 10 0',
							items: [
								{ 	xtype: 'textfield',
									fieldLabel: 'Legajo',
									name: 'Legajo',
									emptyText: '', width: 250
								}, { 	
									name: 'TipoSistema',
									boxLabel: 'System'
								}, {
									name: 'Enabled', labelWidth: 150,
									bind: { boxLabel: '{txtFields.words.enabled}' }
								}, 
							]
						}						
					]
				},
				{ 	bind: { title: '{txtFields.titles.alertAndNotifications}' },  //GRUPOS A LOS QUE PERTENECE
					defaults: { width: '100%' }, hidden: true,
					items: [ ]
				},
				{ 	bind: { title: '{txtFields.titles.enableFields}' },  //WORKFLOWS EN LOS QUE PARTICIPA
					defaults: { width: '100%' }, hidden: true,
					//items: [ { xtype: 'gridFields', flex: 1} ]
				}
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			//style: 'border: none; background-color: red;',
            hidden: true, handler: 'delete'
        },
        '->', {
            bind: { text: '{txtFields.words.save}' },
            formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})

function createStepProperties(data) {
    var win = Ext.getCmp('stepPopup')
    if (!win) {
        win = new Designer.view.popup.Step({
			dataStep: data
		});
    }
	win.show()
}

function deleteStepWithOutPopup(data) {
	var win = Ext.getCmp('stepPopup')
	if (!win) {
        win = new Designer.view.popup.Step({
			dataStep: data
		});
    }
	win.controller.delete()
}