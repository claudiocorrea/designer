Ext.define('Designer.view.popup.Workflow', {
    extend: 'Ext.window.Window',
	
	requires: [ 
		'Designer.store.Enterprises',
		'Designer.store.Categories',	
	],
	
	reference: 'workflowPopup',
	controller: 'workflowPopup',
	
	y: '15%',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	plugins: 'responsive',
	responsiveConfig: {
		'width >= 1000': { width: '60%' },
		'width < 1000': { width: '80%' }
	},

    bind: { title: '{txtFields.words.workflow}' },
    modal: true,

    items : [
		{
            xtype: 'form',
            reference: 'workflowForm',
			layout: 'column',
            border: false,			
			defaults: {
				bodyPadding: 10
			},
            items: [
				{ 	columnWidth: 0.5,
					defaults: {						
						width: '100%',
						labelAlign: 'top',
					},
					items: [
						{	xtype: 'textfield',
							name: 'Title',
							bind: { fieldLabel: '{txtFields.words.title}' },
							emptyText: '',
							allowBlank: false
						}, {
							xtype: 'textarea',
							name: 'Description',
							bind: { fieldLabel: '{txtFields.words.description}' },
							emptyText: '',
							allowBlank: false
						},{
							xtype: 'combo',
							bind: { fieldLabel: '{txtFields.words.enterprise}' },
							name:'IdEmpresa',
							reference: 'cbEmpresa',							
							store: { type: 'enterprises' },
							displayField: 'Descripcion',
							valueField: 'IdEmpresa',
							publishes: 'value',
							editable: false
						}, {
							xtype: 'combo',
							name: 'CategoryID',
							reference: 'cbCategoria',
							store: { type: 'categories' },
							displayField: 'Title',
							valueField: 'CategoryID',
							queryMode: 'remote',
							forceSelection: true,
							allowBlank: false,
							editable: false,
							bind: {
								fieldLabel: '{txtFields.words.category}',
								filters: {
									property: 'IdEmpresa',
									value: '{cbEmpresa.value}'
								}
							}
						}
					]				
				},
				{ 	columnWidth: 0.5,
					defaults: {
						labelAlign: 'top',
					},
					items: [
						{ 	xtype: 'fieldcontainer',	
							bind: { fieldLabel: '{txtFields.titles.startDate}' },
							plugins: 'responsive', hidden: false, 
							responsiveConfig: {
								'width >= 1000': { layout: 'hbox' },
								'width < 1000': { layout: 'vbox' }
							},
							defaults: {
								width: '100%',
								hideLabel: 'true'
							},
							items: [
								{
									xtype: 'datefield',
									name: 'StartDate',
									reference: 'dfStartDate',
									fieldLabel: 'Fecha Inicio',
									width: 150,
									format: 'd/m/Y',
									value: new Date()
								}, {
									xtype: 'checkboxfield',
									name: 'RelativeDates',
									fieldLabel: '',
									bind: { boxLabel: '{txtFields.titles.relativeDates}' },
									margin: '0 0 0 15',
									inputValue: true,
									checked: true
								}
							]
						}, {
							xtype: 'fieldset',
							title: 'Configuracion',														
							items: [
								{ 	xtype: 'fieldcontainer',								
									defaults: {
										xtype: 'checkboxfield',
										hideLabel: 'true',
										inputValue: true,
										value: false
									},
									items: [
										{ boxLabel: 'Puede ser Nodo', name: 'CanBeNode', hidden: true},
										{ boxLabel: 'Genera Alertas', name: 'GeneraAlertas'},
										{ boxLabel: 'Usa Condicionales Pasos', name: 'UsaCondicionalesPasos'},
										{ boxLabel: 'Usa Condicionales Usuarios', name: 'UsaCondicionalesUsuarios'}
									]
								}
							]
						}
					]
				},
				{   xtype: 'hiddenfield', name: 'WorkflowID' },
                {   xtype: 'hiddenfield', name: 'IdState' },
                {   xtype: 'hiddenfield', name: 'EndDate'},
				{   xtype: 'hiddenfield', name: 'Original', value: true}
			]
		}
	],
	
	buttons: [
		{
            bind: { text: '{txtFields.words.delete}' },
			style: 'border: none; background-color: red;',
            handler: 'deleteWorkflow',
			hidden: true
        }, '->', {
            bind: { text: '{txtFields.words.save}' },
			formBind: true,
            handler: 'save'
        }, {
            bind: { text: '{txtFields.words.cancel}' },
            handler: 'close'
        }
    ],

    listeners:{
        afterrender: 'loadData'
    }
})