Ext.define('Designer.view.main.Menu', {
    extend: 'Ext.Panel',
    xtype: 'mainMenu',
	
	requires: [	],

	viewModel: 'menu',
	
	layout: 'border',	
	header: false,
	
	items: [
		{	region: 'west',
			width: 60,
			split: false,
			cls: 'treelist-with-nav',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			border: false,
			scrollable: 'y',
			items: [{
				xtype: 'treelist', 
				reference: 'navTreeList',
				ui: 'navigator',				
				micro: true, expanderOnly: false,
				bind: '{navItems}',
				listeners: {
					selectionchange: 'onNavTreeSelectionChange'
				}
			}]
		}, {
			region: 'center',
			reference: 'mainMenuPanel',
			items: [{ xtype: 'workflowsScreen', flex: 1 }]
		}
	]

})
