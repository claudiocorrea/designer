Ext.define('Designer.view.main.Main', {
    extend: 'Ext.container.Container',
    xtype: 'main',

    id: 'main',

    requires: [	
		'Ext.plugin.Viewport',
		'Designer.view.screen.Workflows',
		'Designer.view.screen.Forms',
		'Designer.view.screen.Frame',
		'Designer.view.screen.Enterprises',
		'Designer.view.screen.Categories',
		'Designer.view.screen.Users',
    ],

    controller: 'main', 
	viewModel: 'main',
	plugins: 'viewport',	

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        { xtype: 'mainToolbar'},
        { xtype: 'mainMenu', flex: 1 }
    ]

})