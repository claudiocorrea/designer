Ext.define('Designer.view.main.Toolbar', {
    extend: 'Ext.Toolbar',
    xtype: 'mainToolbar',
	
	id: 'toolbar',

    height: 60,
    padding: 0,
    border: false,

    items: [
        { 	xtype: 'component',
            cls: 'sgplogo',
            html: '<a href="."><div><img src="resources/images/Logo 60.png"></div></a>'
        }, {
            xtype: 'label',
            text: globalParams.appName,
            cls: 'sgptitle'
        }, '->', {
            xtype: 'label', reference: 'nameUser',
            text: 'username', hidden: true
        }, {
            xtype: 'image', reference: 'imageUser',
            width: 40, height: 40, margin: '0 10 0 0',
            userCls: 'circular',
            src: 'resources/images/users/HSA0.png',
			hidden: true
        }, {
            xtype:'button',
			reference: 'btnUser',
			userCls: 'circular',
            style: 'border: none; background-color: white;'
        }, {
            xtype:'button',
			userCls: 'circular button-header',			
            iconCls:'x-fa fa-cog', arrowVisible: false,
            bind: { tooltip: '{txtFields.words.config}' },
			menu: [
				{ 	bind: { text: '{txtFields.words.lang}' },
                    menu: {
                        items: [
                            {	id: 'es',
                                bind: { text: '{txtFields.words.spanish}' },
                                checked: false,
                                group: 'lang',
                                checkHandler: 'changeLanguage'
                            }, {
								id: 'en',
                                bind: { text: '{txtFields.words.english}' },
                                checked: false,
                                group: 'lang',
                                checkHandler: 'changeLanguage'
                            }
						]
					}
				}, { 	
					bind: { text: '{txtFields.words.theme}' },
                    menu: {
                        items: [
                            {	id: 'light',
                                bind: { text: 'Light' },
                                checked: false,
                                group: 'theme',
                                checkHandler: 'changeTheme'
                            }, {
								id: 'dark',
                                bind: { text: 'Dark' },
                                checked: false,
                                group: 'theme',
                                checkHandler: 'changeTheme'
                            }
						]
					}, hidden: true	
				}		
			]
        }, {
            xtype:'button',
			userCls: 'circular button-header',
            iconCls:'x-fa fa-sign-out',
            bind: { tooltip: '{txtFields.words.logout}' },
            handler: 'logout'
        }
    ]

})