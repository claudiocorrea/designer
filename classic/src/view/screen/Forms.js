Ext.define('Designer.view.screen.Forms', {
    extend: 'Ext.Panel',
    xtype: 'formsScreen',
	
	requires: [ 
		'Designer.grid.Forms',
		'Designer.view.screen.FormDesigner',
		'Designer.view.screen.FormsController' 
	],

    id: 'formsScreen',
	
	controller: 'forms',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	bodyPadding: 15,
	
	viewModel: {
		data: { Title: '' }			
	},
	
	tbar: {
        padding: '5 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.form}s' }
            }, '->', {
				xtype: 'label',
				html: '<span class=\'fa fa-search\' style: \'color: \'gray\'; \'></span>'				
			}, {
                xtype: 'textfield',
				reference: 'tfFind',
                width: '30%', margin: '0 0 0 10',
                bind: {
                    value: '{Title}',
                    emptyText: '{txtFields.titles.findForm}'
                },
                listeners: { change: 'findForm' }
            }, '->', {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-plus',
                bind: { tooltip: '{txtFields.words.new}' },
                handler: 'newForm'
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-bug',
                tooltip: 'Testing form',
                handler: 'newFormDesigner', hidden: true
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [{ xtype: 'gridForms', reference: 'formsGrid'}],

    listeners: {
        afterrender: 'initScreen'
    }

})