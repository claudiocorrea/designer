Ext.define('Designer.view.screen.Categories', {
    extend: 'Ext.Panel',
    xtype: 'categoriesScreen',
	
	requires: [ 
		'Designer.grid.Categories',
		'Designer.view.screen.CategoriesController'		
	],

    id: 'categoriesScreen',
	
	controller: 'categories',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	bodyPadding: 15,
	
	viewModel: {
		data: { Title: '' }			
	},
	
	tbar: {
        padding: '5 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.category}s' }
            }, '->', {
				xtype: 'label',
				html: '<span class=\'fa fa-search\' style: \'color: \'gray\'; \'></span>'				
			}, {
                xtype: 'textfield',
				reference: 'tfFind',
                width: '30%', margin: '0 0 0 10',
                bind: {
                    value: '{Title}',
                    emptyText: '{txtFields.titles.findCategory}'
                },
                listeners: { change: 'findCategory' }
            }, '->', {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-plus',
                bind: { tooltip: '{txtFields.words.new}' },
                handler: 'newCategory'
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [{ xtype: 'gridCategories', reference: 'categoriesGrid'}],

    listeners: {
        afterrender: 'initScreen'
    }

})