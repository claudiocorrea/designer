Ext.define('Designer.view.screen.Workflows', {
    extend: 'Ext.Panel',
    xtype: 'workflowsScreen',
	
	requires: [ 'Designer.view.screen.Designer' ],

    id: 'workflowsScreen',
	
	controller: 'workflow',
	
	scrollable: 'vertical',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	viewModel: {
		data: { Title: '' }			
	},
	
	tbar: {
        padding: '5 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.workflow}s' }
            }, '->', {
				xtype: 'label',
				html: '<span class=\'fa fa-search\' style: \'color: \'gray\'; \'></span>'				
			}, {
                xtype: 'textfield',
				reference: 'tfFind',
                width: '30%', margin: '0 0 0 10',
                bind: {
                    value: '{Title}',
                    emptyText: '{txtFields.titles.findWorkflow}'
                },
                listeners: { change: 'findWorkflow' }
            }, '->', {
                xtype: 'button', margin: 4,
                userCls: 'circular button-toolbar',
                iconCls: 'x-fa fa-plus',
                bind: { tooltip: '{txtFields.words.new}' },
                handler: 'newWorkflow'
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular button-toolbar',
                iconCls: 'x-fa fa-bug',
                tooltip: 'Testing model',
                handler: 'openModelTesting', hidden: true
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular button-toolbar',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [ 
		{
            xtype: 'panel',
            layout: 'border',
			height: Ext.getBody().getViewSize().height - 110,
            border: false,
            defaults: {
                collapsible: true,
                split: false,
				scrollable: 'vertical',
				margin: 10, border: true
            },
            items: [
                {   region: 'center', 
                    bind: { title: '{txtFields.titles.originals}' },  
					items: [{ xtype: 'gridWorkflows', reference: 'originalsGrid'}]
                },
                {   region: 'south',
                    bind: { title: '{txtFields.words.instance}s' },
                    flex: 1, collapsed: true,
                    items: [{ xtype: 'gridWorkflows', reference: 'instancesGrid' }]
                }
            ]

        }
	],

    listeners: {
        afterrender: 'initScreen'
    }

})