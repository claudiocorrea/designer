Ext.define('Designer.view.screen.Designer', {
    extend: 'Ext.Panel',
    xtype: 'designerScreen',

    id: 'designerScreen',
	
	requires: [
		'Designer.view.popup.Step',
		'Designer.view.popup.Depends',
		'Designer.view.popup.Groups',
		'Designer.view.popup.Parallels',
		'Designer.view.screen.DesignerController'
	],
	
	controller: 'designer',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	tbar: {
        padding: 5,
        baseCls: 'screenBackground',
		defaults: {
            xtype:'button',
            userCls: 'circular',
            margin: 5,
        },
        items: [
			{   reference: 'requestReturn',
                iconCls:'x-fa fa-arrow-left',
                bind: { tooltip: '{txtFields.words.back}' },
                handler: 'returnTab'
            },
            {
                xtype: 'label', reference: 'title',
                cls: 'titleSection', userCls: '',
                text: 'Designer'
            }, '->', {
                iconCls: 'x-fa fa-ellipsis-v',
                bind: { tooltip: '{txtFields.words.info}' },
                handler: 'viewJSONModel',
				hidden: true //Boton para mostrar el codigo del modelo
            }, {
                iconCls: 'x-fa fa-search-plus',
                tooltip: 'Zoom +',
                handler: 'increaseZoom'
            }, {
                iconCls: 'x-fa fa-search-minus',
                tooltip: 'Zoom -',
                handler: 'decreaseZoom'
            }, {
                iconCls: 'x-fa fa-info-circle',
                bind: { tooltip: '{txtFields.words.information}' },
                handler: '',
				hidden: true  //Boton para mostrar el panel de colores
            }, {
                iconCls: 'x-fa fa-external-link-square',
                bind: { tooltip: '{txtFields.words.publish}' },
                handler: 'modelCheck',
				hidden: false  //Boton para publicar el modelo
            },{
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [
		{ 	xtype: 'panel', bodyPadding: 5,
			html: '<div id=\'myDiagram\' style=\'height: ' + (Ext.getBody().getViewSize().height - 120) + 'px; background-color: #f6f6f6;\'></div>' 
		} 
	],

    listeners: {
        beforerender: 'loadLibraries',
        afterrender: 'initScreen'
    }

})