Ext.define('Designer.view.screen.Lock', {
    extend: 'Ext.Panel',
    xtype: 'lockScreen',
	
	requires: [ 'Ext.plugin.Viewport' ],

    id: 'lockScreen',
	
	controller: 'lock',	
    viewModel: 'main',
	
	plugins: 'viewport',
	
	layout: 'center',	
	baseCls: 'loginBackground',

    items: [		
		{	xtype: 'label', cls: 'appTitle',
			text: globalParams.appName
		}, {	xtype: 'panel', baseCls: 'panel',		
			plugins: 'responsive', responsiveConfig: {
				'width >= 500': { width: '75%', height: '75%', },
				'width < 500': { width: '95%', height: '95%', }
			},			
			layout: {
				type: 'hbox',
				align: 'stretch'
			},			
			defaults: {
				plugins: 'responsive'
			},
			items: [ 
				{ 	responsiveConfig: {
						'width >= 500': { hidden: false },
						'width < 500': { hidden: true }
					},
					width: '50%',
					baseCls: 'infoPanel',
					items: [ 
						{ 	xtype: 'component', cls: 'bigLogo',
							html: '<div><img src="resources/images/BigLogo.png"></div>'
						}
					]  
				},
				{ 	responsiveConfig: {
						'width >= 500': { width: '50%', bodyPadding: 50 },
						'width < 500': { width: '100%', bodyPadding: 20 }
					},					
					tbar: {
						baseCls: 'whiteBackground',
						items: [
							'->', {
								xtype:'button', userCls: 'circular', margin: 5,
								iconCls: 'x-fa fa-language', arrowVisible: false,
								bind: { tooltip: '{txtFields.words.lang}' },
								menu: {
									items: [
										{	id: 'es',
											bind: { text: '{txtFields.words.spanish}' },
											checked: false,
											group: 'lang',
											checkHandler: 'changeLanguage'
										}, {
											id: 'en',
											bind: { text: '{txtFields.words.english}' },
											checked: false,
											group: 'lang',
											checkHandler: 'changeLanguage'
										}
									]
								}
							}
						]
					},
					items: [ 
						{ 	xtype: 'form', reference: 'loginForm',
							defaults: {
								xtype: 'textfield', ui: 'login',
								allowBlank: false, 
								labelAlign: 'top',
								width: '100%', margin: '20 0' 
							}, items: [
								{	xtype: 'label', cls: 'signIn', 
									bind: { text: '{txtFields.words.signIn}' }									
								}, { 	
									name: 'username', margin: '25 0',
									bind: { fieldLabel: '{txtFields.words.username}', value: '{username}' },
									listeners: { specialkey: 'onSpecialKey' }
								}, { 	
									name: 'password',
									bind: { fieldLabel: '{txtFields.words.password}', value: '{password}' },
									inputType: 'password', listeners: { specialkey: 'onSpecialKey' }
								}, { 	
									xtype: 'button',margin: '30 0', reference: 'btnLogin',
									bind: { text: '{txtFields.words.login}' },
									formBind: true, handler: 'login'
								}
							]
						}
					]  
				}
			]
		}
	],
	
	bbar: {
		baseCls: 'noBackground',
		items: [
			'->', {
				xtype: 'label', style: 'margin: 5px; padding: 5px; background: gray;',
				text: '© Copyright 2018 Simbius SGP SRL. All rights reserved'
			}, '->'
		]
	},

    listeners: { }

})