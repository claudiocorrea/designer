Ext.define('Designer.view.screen.Users', {
    extend: 'Ext.Panel',
    xtype: 'usersScreen',
	
	requires: [ 
		'Designer.grid.Users',
		'Designer.view.screen.UsersController'
	],

    id: 'usersScreen',
	
	controller: 'users',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	bodyPadding: 15,
	
	viewModel: {
		data: { Title: '' }			
	},
	
	tbar: {
        padding: '5 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.user}s' }
            }, '->', {
				xtype: 'label',
				html: '<span class=\'fa fa-search\' style: \'color: \'gray\'; \'></span>'				
			}, {
                xtype: 'textfield',
				reference: 'tfFind',
                width: '30%', margin: '0 0 0 10',
                bind: {
                    value: '{Title}',
                    emptyText: '{txtFields.titles.findUser}'
                },
                listeners: { change: 'findUser' }
            }, '->', {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-plus',
                bind: { tooltip: '{txtFields.words.new}' },
                handler: 'newUser'
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [{ xtype: 'gridUsers', reference: 'usersGrid'}],

    listeners: {
        afterrender: 'initScreen'
    }

})