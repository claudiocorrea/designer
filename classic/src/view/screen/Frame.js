Ext.define('Designer.view.screen.Frame', {
    extend: 'Ext.Panel',
    xtype: 'frameScreen',
	
	requires: [ 'Designer.view.screen.FrameController' ],

    id: 'frameScreen',
	
	controller: 'frame',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	tbar: {
        padding: 5,
        baseCls: 'screenBackground',
		defaults: {
            xtype:'button',
            userCls: 'circular',
            margin: 5,
        },
        items: [
			{   reference: 'return',
                iconCls:'x-fa fa-arrow-left',
                bind: { tooltip: '{txtFields.words.back}' },
                handler: 'returnTab', 
				hidden: true
            },
			{   xtype: 'label', reference: 'titleSection',
                cls: 'titleSection', userCls: '',
                bind: { text: '{txtFields.titles.frame}' }
            }, '->', {
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [	],

    listeners: {
        beforerender: 'initScreen'
    }

})