Ext.define('Designer.view.screen.Enterprises', {
    extend: 'Ext.Panel',
    xtype: 'enterprisesScreen',
	
	requires: [ 
		'Designer.grid.Enterprises',
		'Designer.view.screen.EnterpriseController'
	],

    id: 'enterprisesScreen',
	
	controller: 'enterprise',
	
	layout: {
        type: 'vbox',
        align: 'stretch'
    },
	
	bodyPadding: 15,
	
	viewModel: {
		data: { Title: '' }			
	},
	
	tbar: {
        padding: '5 10',
        baseCls: 'screenBackground',
        items: [
            {
                xtype: 'label',
                cls: 'titleSection',
                bind: { text: '{txtFields.words.enterprise}s' }
            }, '->', {
				xtype: 'label',
				html: '<span class=\'fa fa-search\' style: \'color: \'gray\'; \'></span>'				
			}, {
                xtype: 'textfield',
				reference: 'tfFind',
                width: '30%', margin: '0 0 0 10',
                bind: {
                    value: '{Title}',
                    emptyText: '{txtFields.titles.findEnterprise}'
                },
                listeners: { change: 'findEnterprise' }
            }, '->', {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-plus',
                bind: { tooltip: '{txtFields.words.new}' },
                handler: 'newEnterprise'
            }, {
                xtype: 'button', margin: 4,
                userCls: 'circular',
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [{ xtype: 'gridEnterprises', reference: 'enterprisesGrid'}],

    listeners: {
        afterrender: 'initScreen'
    }

})