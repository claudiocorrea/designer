Ext.define('Designer.view.screen.FormDesigner', {
    extend: 'Ext.Panel',
    xtype: 'formDesignerScreen',
	
	requires: [ 
		'Designer.store.Forms',
		'Designer.store.Origins',
		'Designer.view.screen.FormDesignerController' 
	],

    id: 'formDesignerScreen',
	
	controller: 'formDesigner',
	
	layout: {	
        type: 'hbox',
        align: 'stretch'
    },
	
	tbar: {
        padding: 5,
        baseCls: 'screenBackground',
		defaults: {
            xtype: 'button',
            userCls: 'circular button-toolbar',
            margin: 5,
        },
        items: [
			{   reference: 'requestReturn',
                iconCls:'x-fa fa-arrow-left',
                bind: { tooltip: '{txtFields.words.back}' },
                handler: 'returnTab'
            }, {
                xtype: 'label',
                cls: 'titleSection', userCls: '',
                bind: { text: '{txtFields.titles.formDesigner}' }
            }, '->', {
                iconCls: 'x-fa fa-file',
                bind: { tooltip: 'Ver Formulario ExtJS' },
                handler: 'viewForm',
				hidden: false
            },{
                iconCls: 'x-fa fa-save',
                bind: { tooltip: '{txtFields.words.save}' },
                handler: 'saveFields'
            }, {
                iconCls: 'x-fa fa-refresh',
                bind: { tooltip: '{txtFields.words.refresh}' },
                handler: 'refreshScreen'
            }
        ]
    },

    items: [
		{   xtype: 'toolbar', dock: 'left',
			baseCls: 'screenBackground',
			//bodyPadding: 5,
			defaults: {
				xtype: 'button',
				userCls: 'circular',
				margin: '5 10',
			},
			items: [
				{	iconCls: 'section-icon', tooltip: 'Seccion', handler: 'addSection' }, 
				{ iconCls: null, glyph: 47, handler: 'addFieldContainer' },
			'-', 
				//{ iconCls: 'x-fa fa-code', tooltip: 'Label', handler: 'addLabel' },
				{ iconCls: 'textfield-icon', tooltip: 'Textfield', handler: 'addTextfield' }, 
				{ iconCls: 'textarea-icon', tooltip: 'Textarea', handler: 'addHTMLEditor' },				 
				{ iconCls: 'datefield-icon', tooltip: 'Datefield', handler: 'addDatefield' }, 
				{ iconCls: 'numberfield-icon', tooltip: 'Numberfield', handler: 'addNumberfield' }, 
				{ iconCls: 'combobox-icon', tooltip: 'Combobox', handler: 'addCombobox' },
				{ iconCls: 'checkbox-icon', tooltip: 'Checkbox', handler: 'addCheckbox' }, 
				{ iconCls: 'radiobutton-icon', tooltip: 'Radio Button', handler: 'addRadiobutton' }, 
				{ iconCls: 'list-icon', tooltip: 'List', handler: 'addList' },
				//{ iconCls: 'htmleditor-icon', tooltip: 'HTML Editor', handler: 'addHTMLEditor' }, 
				{ iconCls: 'form-icon', tooltip: 'Form', handler: 'addForm' } 
			]
		},
		{	xtype: 'panel',
			layout: 'border',
            flex: 1, border: false,
			height: Ext.getBody().getViewSize().height - 110,            
			defaults: { collapsible: true, scrollable: 'vertical' }, 
			items: [				
				{ 	region: 'east', flex: 1, bodyPadding: 10, 
					border: true,					
					bind: { title: '{txtFields.words.properties}' },
					tools: [
						{ 	iconCls: 'x-fa fa-arrow-up', handler: 'upField'},
						{ 	iconCls: 'x-fa fa-arrow-down', handler: 'downField' },
						{ 	reference: 'iconDelete', iconCls: 'x-fa fa-trash', 
							bind: { tooltip: '{txtFields.words.delete}'}, 
							handler: 'deleteField', hidden: true
						}
					], scrollable: 'vertical',
					items: [
						{ 	xtype: 'form',
							reference: 'propertiesForm',
							defaults: { width: '100%' },
							items: [						
								{	xtype: 'textfield',
									bind: { fieldLabel: '{txtFields.words.name}'},
									name: 'name', reference: 'tfName',
									listeners: { change: 'changeProperties' }, hidden: true
								}, {	
									xtype: 'textfield',
									bind: { fieldLabel: '{txtFields.words.label}'},
									name: 'label', reference: 'tfLabel',
									listeners: {  change: 'changeLabel' }, hidden: true
								}, { 	
									xtype: 'numberfield', 
									name: 'posicion', reference: 'nfPosition',
									bind: { fieldLabel: '{txtFields.words.position}' },
									value: 0,
									listeners: {  change: 'changeProperties' }, hidden: true
								}, {
									xtype: 'fieldcontainer', reference: 'fcReadOnly',
									plugins: 'responsive',
									responsiveConfig: {
										'width >= 1000': { layout: 'hbox' },
										'width < 1000': { layout: 'vbox' }
									},	
									defaults: { xtype: 'checkboxfield', margin: '0 40 0 0' },									
									items: [
										{   name: 'mandatory',
											bind: { fieldLabel: '{txtFields.words.mandatory}' },
											handler: 'changeProperties'
										}, {
											name: 'readonly',
											bind: { fieldLabel: '{txtFields.titles.onlyRead}' },
											handler: 'changeProperties'
										}
									], hidden: true
								}, {   
									xtype: 'checkboxfield',
									name: 'createReference', reference: 'ckReference',
									bind: { fieldLabel: '{txtFields.titles.createReference}' },
									handler: 'changeProperties', hidden: true
								}, {
									xtype: 'fieldcontainer', reference: 'ckOptionNew', 
									plugins: 'responsive',
									responsiveConfig: {
										'width >= 1000': { layout: 'hbox' },
										'width < 1000': { layout: 'vbox' }
									},	
									defaults: { xtype: 'checkboxfield', margin: '0 40 0 0' },							
									items: [
										{   name: 'optionNew', reference: 'optionNew',
											bind: { fieldLabel: '{txtFields.titles.optionNew}' },
											handler: 'changeProperties',
										},  {
											xtype: 'button', userCls: 'circular',
											iconCls: 'x-fa fa-plus', margin: '0 20 0 -20',
											hidden: true, //bind: { hidden: '{!optionNew.checked}' },
											handler: 'addComboItem'
										}, {   
											name: 'allowEmpty', //reference: 'optionNew',
											bind: { fieldLabel: '{txtFields.titles.allowEmpty}' },
											handler: 'changeProperties',
										}
									], hidden: true
								}, {   
									xtype: 'checkboxfield', 
									name: 'onlyNumbers', reference: 'ckOnlyNumbers',
									bind: { fieldLabel: '{txtFields.titles.onlyNumbers}' },
									handler: 'changeProperties',
									hidden: true
								}, {
									xtype: 'fieldcontainer', reference: 'fcDate',
									plugins: 'responsive',
									responsiveConfig: {
										'width >= 1000': { layout: 'hbox' },
										'width < 1000': { layout: 'vbox' }
									},	
									defaults: { xtype: 'checkboxfield', margin: '0 40 0 0' },									
									items: [
										{ 	name: 'datetype',
											bind: { fieldLabel: '{txtFields.titles.dateType}' },
											handler: 'changeProperties'
										}, 
									], hidden: true
								}, { 	
									xtype: 'checkboxfield', name: 'dategreater',
									reference: 'ckGreater',
									bind: { fieldLabel: '{txtFields.titles.dateGreaterOrSameAsToday}' },
									handler: 'changeProperties', hidden: true
								}, {	
									xtype: 'textfield', 
									name: 'style', reference: 'tfStyle',
									bind: { fieldLabel: '{txtFields.titles.styleSheet}'},
									listeners: {  change: 'changeProperties' }, hidden: true
								}, {	
									xtype: 'textfield', name: 'htmlfield', reference: 'tfHtmlField',
									bind: { fieldLabel: '{txtFields.titles.htmlfield}'},									
									listeners: {  change: 'changeProperties' }, hidden: true
								}, {
									xtype: 'fieldcontainer', reference: 'fcExtras',
									plugins: 'responsive',
									responsiveConfig: {
										'width >= 1000': { layout: 'hbox' },
										'width < 1000': { layout: 'vbox' }
									},	
									defaults: { xtype: 'checkboxfield', margin: '0 40 0 0' },									
									items: [
										{ 	name: 'ignoreIfEmpty',
											bind: { fieldLabel: '{txtFields.titles.ignoreIfEmpty}' },
											handler: 'changeProperties'
										}, { 	
											name: 'hiddenApprove',
											bind: { fieldLabel: '{txtFields.titles.hiddenApprove}' },
											handler: 'changeProperties'
										}
									], hidden: true
								}, {
									xtype: 'combo',
									bind: { fieldLabel: '{txtFields.words.form}s' },
									name:'IdForm',
									reference: 'cbForm',							
									store: { type: 'forms' },
									displayField: 'FormName',
									valueField: 'IdForm',
									publishes: 'value',
									editable: false, 
									listeners: {  change: 'changeProperties' }, hidden: true
								}, { 
									xtype: 'panel', reference: 'sqlPanel',
									collapsible: true, //collapsed: true, 
									title: 'SQL', bodyPadding: '10 0', hidden: true, 
									defaults: { xtype: 'textfield', width: '100%', }, 
									items: [
										{ 	xtype: 'combo',
											bind: { fieldLabel: '{txtFields.titles.dataOrigins}' },
											name:'originDB',
											reference: 'cbOriginDB',							
											store: { type: 'origins' },
											displayField: 'Nombre',
											valueField: 'IdOrigen',
											publishes: 'value',
											editable: false, 
											listeners: {  change: 'changeProperties' }
										}, {	
											name: 'tableDB', reference: 'tfTableDB',
											bind: { fieldLabel: '{txtFields.words.table}'},
											listeners: {  change: 'changeProperties' }
										}, {	
											name: 'spDB', reference: 'tfSPDB',
											bind: { fieldLabel: '{txtFields.titles.storeProcedure}'},
											listeners: {  change: 'changeProperties' }
										}, {	
											name: 'input', reference: 'tfInput',
											bind: { fieldLabel: '{txtFields.titles.inputParameters}'},
											listeners: {  change: 'changeProperties' }
										}, {	
											name: 'fieldValue', reference: 'tfFieldValue',
											bind: { fieldLabel: '{txtFields.titles.fieldValue}'},
											listeners: {  change: 'changeProperties' }
										}, {	
											name: 'comboText', reference: 'tfComboText',
											bind: { fieldLabel: '{txtFields.titles.textCombo}'},
											listeners: {  change: 'changeProperties' }
										}
									]
								}
							]
						}
					]
				},
				{ 	region: 'center', title: 'Preview', 
					reference: 'previewPanel',
					layout: {
						type: 'vbox',
						align: 'stretch'
					}, //ui: 'light',
					flex: 2, bodyPadding: 10,
					collapsible: false, split: false,
					items: [
						{ 	xtype: 'panel',
							reference: 'formPanel',
							defaults: { width: '100%', },
							items: [ ]
						}
					]
				},				
			]
		}
	],

    listeners: {
        beforerender: 'initScreen'
    }

})