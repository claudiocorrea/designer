Ext.define('Designer.grid.Enterprises', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridEnterprises',
	
	requires: [ 'Designer.store.Enterprises' ],
	
	height: Ext.getBody().getViewSize().height - 140,

    store: { type: 'enterprises'},
	
	header: false,	
    headerBorders: false,
	
	border: true,
	
	scrollable: 'vertical',

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: '15%', sortable : true, dataIndex: 'IdEmpresa',
            tpl: [ '<p/><div class="tplText">{IdEmpresa}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enterprise}'},
            flex: 2, sortable : true, dataIndex: 'Descripcion',
            tpl: [ '<p/><div class="tplText">{Descripcion}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.code}'},
            width: '15%', sortable : true, dataIndex: 'Codigo',
            tpl: [ '<p/><div class="tplText">{Codigo}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enabled}'},
            width: '15%', sortable : true, dataIndex: 'Enabled',
            tpl: [ '<p/><div class="tplText">{Enabled}</div>' ],
			hidden: false
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                bind: {tooltip: '{txtFields.words.edit}'},
                handler: 'editEnterprise'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-times-circle-o',
                bind: {tooltip: '{txtFields.words.delete}'},
                handler: 'deleteEnterprise'
            }]
        }
	]
	
})