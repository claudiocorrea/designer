Ext.define('Designer.grid.Categories', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridCategories',
	
	requires: [ 'Designer.store.Categories' ],
	
	height: Ext.getBody().getViewSize().height - 140,

    store: { type: 'categories'},
	
	header: false,	
    headerBorders: false,
	
	border: true,
	
	scrollable: 'vertical',

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: '15%', sortable : true, dataIndex: 'CategoryID',
            tpl: [ '<p/><div class="tplText">{CategoryID}</div>' ],
			hidden: true
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.category}'},
            width: '25%', sortable : true, dataIndex: 'Title',
            tpl: [ '<p/><div class="tplText">{Title}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.description}'},
            flex: 2, sortable : true, dataIndex: 'Description',
            tpl: [ '<p/><div class="tplText">{Description}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enterprise}'},
            width: '15%', sortable : true, dataIndex: 'IdEmpresa',
            tpl: [ '<p/><div class="tplText">{IdEmpresa}</div>' ],
			hidden: true
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enterprise}'},
            width: '15%', sortable : true, dataIndex: 'empresa',
            tpl: [ '<p/><div class="tplText">{empresa}</div>' ],
			hidden: false
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                bind: {tooltip: '{txtFields.words.edit}'},
                handler: 'editCategory'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-times-circle-o',
                bind: {tooltip: '{txtFields.words.delete}'},
                handler: 'deleteCategory'
            }]
        }
	]
	
})