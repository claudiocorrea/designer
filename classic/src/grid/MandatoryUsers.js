Ext.define('Designer.grid.MandatoryUsers', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridMandatoryUsers',
	
	requires: [  'Designer.store.MandatoryUsers' ],
	
	reference: 'gridMandatoryUsers',

    store: { type: 'mandatoryUsers'},

    headerBorders: false,   

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: 50, sortable : true, dataIndex: 'IdUser',
            tpl: [ '<p/><div class="tplText">{IdUser}</div>' ],
			hidden: true
        },
        {   xtype: 'templatecolumn',
			//bind: {text: '{txtFields.titles.Enabled users}'},
			text: 'Usuarios habilitados',
            flex: 1, sortable : true, dataIndex: 'FirstName',
            tpl: [ '<p/><div class="tplText">{FirstName} {LastName}</div>' ],
			hidden: false
        },
        {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.email}'},
            flex: 1, sortable : true, dataIndex: 'Email',
            tpl: [ '<p/><div class="tplText">{Email}</div>' ],
			hidden: true
        }, 
		{ 	xtype: 'checkcolumn',
			bind: {text: '{txtFields.words.mandatory}'},
            align: 'center', width: 100,			
			headerCheckbox: true, dataIndex: 'Mandatory', 
			menuDisabled: true
        },
	],
	
	listeners: {
		//afterrender: 'loadUsers'
	}
	
})