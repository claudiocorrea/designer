Ext.define('Designer.grid.Forms', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridForms',
	
	requires: [ 'Designer.store.Forms' ],
	
	height: Ext.getBody().getViewSize().height - 140,

    store: { type: 'forms'},
	
	header: false,	
    headerBorders: false,
	
	border: true,
	
	scrollable: 'vertical',

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            flex: 1, sortable : true, dataIndex: 'IdForm',
            tpl: [ '<p/><div class="tplText">{IdForm}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.name}'},
            flex: 1, sortable : true, dataIndex: 'FormName',
            tpl: [ '<p/><div class="tplText">{FormName}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.description}'},
            flex: 2, sortable : true, dataIndex: 'Description',
            tpl: [ '<p/><div class="tplText">{Description}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.original}'},
            width: '15%', sortable : true, dataIndex: 'Original',
            tpl: [ '<p/><div class="tplText">{Original}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enterprise}'},
            width: '15%', sortable : true, dataIndex: 'IdEmpresa',
            tpl: [ '<p/><div class="tplText">{IdEmpresa}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.type}'},
            width: '15%', sortable : true, dataIndex: 'IdFormType',
            tpl: [ '<p/><div class="tplText">{IdFormType}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.column}s'},
            width: '15%', sortable : true, dataIndex: 'Columnas',
            tpl: [ '<p/><div class="tplText">{Columnas}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.code}'},
            width: '15%', sortable : true, dataIndex: 'Code',
            tpl: [ '<p/><div class="tplText">{Code}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.titles.styleSheet}'},
            width: '15%', sortable : true, dataIndex: 'StyleSheet',
            tpl: [ '<p/><div class="tplText">{StyleSheet}</div>' ],
			hidden: true
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                bind: {tooltip: '{txtFields.words.edit}'},
                handler: 'editForm'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-building',
                bind: {tooltip: '{txtFields.titles.formFields}'},
                handler: 'designerForm'
            }], hidden: false
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-times-circle-o',
                bind: {tooltip: '{txtFields.words.delete}'},
                handler: 'deleteForm'
            }]
        }
	]
	
})