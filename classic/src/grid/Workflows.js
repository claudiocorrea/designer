Ext.define('Designer.grid.Workflows', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridWorkflows',
	
	requires: [ 'Designer.store.Workflows' ],
	
	flex: 1,

    store: { type: 'workflows'},
	
	header: false,	
    headerBorders: false,

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: '5%', sortable : true, dataIndex: 'WorkflowID',
            tpl: [ '<p/><div class="tplText">{WorkflowID}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.title}'},
            flex: 1, sortable : true, dataIndex: 'Title',
            tpl: [ '<p/><div class="tplText">{Title}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.description}'},
            flex: 2, sortable : true, dataIndex: 'Description',
            tpl: [ '<p/><div class="tplText">{Description}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.category}'},
            width: '15%', sortable : true, dataIndex: 'Category',
            tpl: [ '<p/><div class="tplText">{Category}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.enterprise}'},
            width: '15%', sortable : true, dataIndex: 'Empresa',
            tpl: [ '<p/><div class="tplText">{Empresa}</div>' ],
			hidden: true
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.state}'},
            width: 120, sortable : true, dataIndex: 'StateTxt',
            tpl: [ '<p/><div class="tplText">{StateTxt}</div>' ],
			hidden: false
        }, {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.original}'},
            width: 120, sortable : true, dataIndex: 'Original',
            tpl: [ '<p/><div class="tplText">{Original}</div>' ],
			hidden: true
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                bind: {tooltip: '{txtFields.words.edit}'},
                handler: 'viewWorkflow'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-clone',
                bind: {tooltip: '{txtFields.words.clone}'},
                handler: 'cloneModel'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-sitemap',
                bind: {tooltip: '{txtFields.words.model}'},
                handler: 'openModel'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-times-circle-o',
                bind: {tooltip: '{txtFields.words.delete}'},
                handler: 'deleteWorkflow'
            }]
        }
	]
	
})