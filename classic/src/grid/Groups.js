Ext.define('Designer.grid.Groups', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridGroups',
	
	requires: [ 'Designer.store.Groups' ],
	
	reference: 'gridGroups',

    store: { type: 'groups'},	

    headerBorders: false,    

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: 50, sortable : true, dataIndex: 'IdGroup',
            tpl: [ '<p/><div class="tplText">{IdGroup}</div>' ],
			hidden: true
        },
        {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.name}'},
            flex: 1, sortable : true, dataIndex: 'Name',
            tpl: [ '<p/><div class="tplText">{Name}</div>' ],
			hidden: false
        }, 
		{ 	xtype: 'checkcolumn',
			bind: {text: '{txtFields.words.enabled}'},
            align: 'center', width: 100,			
			headerCheckbox: true, dataIndex: 'Enabled', 
			menuDisabled: true
        },
	],
	
	listeners: {
		//afterrender: 'loadGroups'
	}
	
})