Ext.define('Designer.grid.Fields', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridFields',
	
	requires: [ 'Designer.store.FormFields' ],
	
	reference: 'gridFields',

    store: { type: 'formFields'},	

    headerBorders: false,    

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            width: 50, sortable : true, dataIndex: 'IdField',
            tpl: [ '<p/><div class="tplText">{IdField}</div>' ],
			hidden: true
        },
        {   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.name}'},
            flex: 1, sortable : true, dataIndex: 'FieldName',
            tpl: [ '<p/><div class="tplText">{FrontendText} ({FieldName})</div>' ],
			hidden: false
        }, 
		{ 	xtype: 'checkcolumn',
			bind: {text: '{txtFields.words.enabled}'},
            align: 'center', width: 100,			
			headerCheckbox: true, dataIndex: 'Habilitado', 
			menuDisabled: true
        },
		{ 	xtype: 'checkcolumn',
			text: 'Visible',
            align: 'center', width: 100, 			
			headerCheckbox: true, dataIndex: 'Visible', 
			menuDisabled: true
        }
	],
	
	features: [ { ftype:'grouping' } ],
	
	listeners: {
		//afterrender: 'loadFields'
	}
	
})