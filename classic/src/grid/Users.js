Ext.define('Designer.grid.Users', {
    extend: 'Ext.grid.Panel',
    xtype: 'gridUsers',
	
	requires: [ 'Designer.store.Users' ],
	
	height: Ext.getBody().getViewSize().height - 140,

    store: { type: 'users'},
	
	header: false,	
    headerBorders: false,
	
	border: true,
	
	scrollable: 'vertical',

    columns: [
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.id}'},
            flex: 1, sortable : true, dataIndex: 'IdUser',
            tpl: [ '<p/><div class="tplText">{IdUser}</div>' ],
			hidden: true
        },
		{   xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.username}'},
            flex: 1, sortable : true, dataIndex: 'Login',
            tpl: [ '<p/><div class="tplText">{Login}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.firstname}'},
            flex: 1, sortable : true, dataIndex: 'FirstName',
            tpl: [ '<p/><div class="tplText">{FirstName}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.lastname}'},
            width: '15%', sortable : true, dataIndex: 'LastName',
            tpl: [ '<p/><div class="tplText">{LastName}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.profile}'},
            width: '15%', sortable : true, dataIndex: 'IdProfile',
            tpl: [ '<p/><div class="tplText">{IdProfile}</div>' ],
			hidden: true
        }, {   
			xtype: 'templatecolumn',
			bind: {text: '{txtFields.words.profile}'},
            width: '15%', sortable : true, dataIndex: 'profile',
            tpl: [ '<p/><div class="tplText">{profile}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			text: 'Email',
            flex: 1, sortable : true, dataIndex: 'Email',
            tpl: [ '<p/><div class="tplText">{Email}</div>' ],
			hidden: false
        }, {   
			xtype: 'templatecolumn',
			text: 'System',
            width: '8%', sortable : true, dataIndex: 'TipoSistema',
            tpl: [ '<p><div class="tplText">{TipoSistema}</div><p/>' ],
			hidden: false
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-edit',
                bind: {tooltip: '{txtFields.words.edit}'},
                handler: 'editUser'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-times-circle-o',
                bind: {tooltip: '{txtFields.words.delete}'},
                handler: 'deleteUser'
            }]
        }, {
            xtype: 'actioncolumn',
            menuDisabled: true, width: 40,
            align: 'center',
            items: [{
                iconCls: 'x-fa fa-clone',
                bind: {tooltip: '{txtFields.words.clone}'},
                handler: 'cloneUser'
            }], hidden: true
        }
	]
	
})