var port = 8008,	
	compression = require('compression'),
	express = require('express'),
	app = express(),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	path = require('path'),
	router = express.Router(),
	routerSession = require('./nodejs/routes/session'),
	routerWorkflow = require('./nodejs/routes/workflow'),
	routerStores = require('./nodejs/routes/stores'),
	routerStep = require('./nodejs/routes/step'),
	routerForm = require('./nodejs/routes/form'),
	routerUser = require('./nodejs/routes/user'),
	routerOrigins = require('./nodejs/routes/origins'),
	routerUtil = require('./nodejs/routes/util');
	
global.DB = 'Spm';
global.StylePath = './classic/sass/src/Application.scss';
global.KPI = 'cpbakpi'
	
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.header('Cache-Control', 'no-cache');

    if ( req.method == 'OPTIONS') { res.sendStatus(200); res.end(); } else { next(); }
})

app.use(compression());
app.use(express.static('./'));

app.use(session({
    secret: 'SGPDesigner2017',
    resave: false,
    saveUninitialized: true,
	user: '',
    cookie: {
        secure: true,
        expires: 30000
    }
}));


app.use(router);
app.use('/session', routerSession);
app.use('/workflow', routerWorkflow);
app.use('/stores', routerStores);
app.use('/step', routerStep);
app.use('/form', routerForm);
app.use('/user', routerUser);
app.use('/util', routerUtil);
app.use('/origins', routerOrigins);

app.listen(port, function () {
    console.log('Servidor iniciado ( port: ' + port + ') - ' + new Date() + '... ');
});
