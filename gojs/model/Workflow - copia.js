var $ = null
var myDiagram
var data
var testing = false

var configSize = 20
var eventSize = 30
var gatewaySize = 40
var nodeWidth = 180
var nodeHeight = 100

var nodeSpacing = 150

var baseColor = '#1E90FF'
var backgroundColor = '#f6f6f6'
var defaultFont = "12pt 'Open Sans', 'Helvetica Neue', helvetica, arial, verdana, sans-serif"

var infoTooltip = true

function initModel(p_data, p_readOnly) {
	data = p_data	
	testing = data.workflow ? false : true
	console.log('Iniciando modelo....', data, 'testing: ', testing)
	
	if ($ == null) $ = go.GraphObject.make;
	
	// DIAGRAMA
    myDiagram = $(go.Diagram, "myDiagram", {
        initialContentAlignment: go.Spot.TopCenter,
		layout: $(go.LayeredDigraphLayout,
                  { direction: 90, layeringOption: go.LayeredDigraphLayout.LayerLongestPathSource, columnSpacing: 50}),
		maxSelectionCount: 1,
		"SelectionDeleting": function (e) {
              //console.log("SelectionDeleting: ", e.subject.first())
			  if(e.subject.first().data.category != undefined)
			  deleteNode(e.subject.first())
		},
		"SelectionDeleted": function (e) { },
		"LinkDrawn": function (e) { },
		"toolManager.hoverDelay": 100,
		isReadOnly: data.workflow ? data.workflow.Original == 'true' ? false : true : false
	})
	
	//  NODOS
    var nodeTemplateMap = new go.Map("string", go.Node);
	nodeTemplateMap.add("step", createStepTemplate());
	nodeTemplateMap.add("event", createEventTemplate());
	nodeTemplateMap.add("gateway", createGatewayTemplate());
	myDiagram.nodeTemplateMap =  nodeTemplateMap;
	
	// LINKS
    myDiagram.linkTemplate = $(go.Link,
        {   routing: go.Link.AvoidsNodes, curve: go.Link.JumpGap, corner: 10, resegmentable: true,
            reshapable: true, relinkableFrom: true, relinkableTo: true, toEndSegmentLength: 10,
			selectionAdornmentTemplate: createLinkIconPallete(), resizeAdornmentTemplate: false
        }, new go.Binding("points").makeTwoWay(),
        $(go.Shape, { stroke: "gray", strokeWidth: 3 }),
		$(go.Shape, "Diamond",
			{   alignment: new go.Spot(100, 0, 150, 15),
				width: 20, height: 20, visible: false, cursor: 'pointer',
				strokeWidth: 1, stroke: 'gray', fill: 'orange',
				mouseEnter: function(e, obj) { obj.fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.fill = 'orange' },
				doubleClick: function(e, obj) { if(!myDiagram.isReadOnly) callDependsPopup(obj.part.data) },
				toolTip: infoTooltip ? createTooltip('conditions'): null
			}, new go.Binding("visible", "condition")
		),		
        $(go.Shape, { toArrow: "Triangle", scale: 1.2, fill: "black", stroke: null }),
        $(go.TextBlock,
            {   name: "Label", editable: true, text: "label", segmentOffset: new go.Point(-10, -10), visible: false },
            new go.Binding("text", "text").makeTwoWay(),
            new go.Binding("visible", "visible").makeTwoWay())
    );
	
	createInfo()
	
	setModel(data)
	
}

//------------------------------------------   STEPS   -------------------------------------------------------------------

function createStepTemplate() {
	return $(go.Node, "Spot", {
			selectionAdornmentTemplate: createIconPallete('step'),
			doubleClick: function(e, obj) { if(!myDiagram.isReadOnly) callPropertiesPopup(obj.part.data) },
			toolTip: infoTooltip ? createTooltip('groups'): null 
		}, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Auto",
            {   name: "panel", minSize: new go.Size(nodeWidth, nodeHeight),
                desiredSize: new go.Size(nodeWidth, nodeHeight)
            }, $(go.Panel, "Spot",
                $(go.Shape, "RoundedRectangle",
                    {   name: "shape", strokeWidth: 2, stroke: 'white',
                        parameter1: 0, cursor: !myDiagram.isReadOnly ? 'pointer': '',
                        portId: "", fromLinkable: true, toLinkable: true,
                        fromSpot: go.Spot.AllSides, toSpot: go.Spot.NotBottomSide
                    }, new go.Binding("fill", "IdState", setFillByState)
				), $(go.Shape, "Pointer",
                    {   alignment: new go.Spot(0, 0, nodeWidth - 15, 15),
						width: 18, height: 18, strokeWidth: 1, stroke: 'gray', fill: 'red'
					}, new go.Binding("visible", "RechazoEnFormularios")
                ), $(go.TextBlock,
                    {   alignment: go.Spot.Center, font: defaultFont,
                        textAlign: "start", margin: 0, stroke: 'white',
                        editable: false, cursor: 'pointer'
                    }, new go.Binding("text").makeTwoWay(),
					new go.Binding("stroke", "IdState", setStrokeByState)
                )
			))
		)
}

//------   PROPERTIES

function setFillByState(c) {
	if(data.workflow.Original == 'true') return baseColor
	if(c == 9) return 'lightgray'
	var colors = [ baseColor, "yellow", "green", "red", "orange" ]
    return c < colors.length ?  colors[c-1] : baseColor
}

function setStrokeByState(c) {
	if(data.workflow.Original == 'true') return 'white'
	var colors = [ baseColor, "yellow", "green", "red", "orange" ]
    return colors[c-1] != "yellow" && c != 9 ?  'white' : 'dimgray'
}

//------------------------------------------   EVENTS   ----------------------------------------------------------------

function createEventTemplate() {
    return $(go.Node, "Vertical",
        { 	locationSpot: go.Spot.Center,
            selectionAdorned: false
        }, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
		$(go.Panel, "Spot",
            $(go.Shape, "Circle",
                {   name: "shape", fill: 'limegreen',
                    strokeWidth: 2, stroke: 'white',
                    desiredSize: new go.Size(eventSize, eventSize),
                    cursor: "pointer",
                    portId: "", fromLinkable: false, toLinkable: true,
                    fromSpot: go.Spot.AllSides, toSpot: go.Spot.NotBottomSide
                }, new go.Binding("fill", "type", function(s) { return s == 1 ? 'limegreen' : 'red' })
            )
		)
	)
}

//------------------------------------------   GATEWAY   ---------------------------------------------------------------

function nodeGatewaySymbolTypeConverter(s) {
    var tasks =  ["ThinX",          // 1 - Exclusive  (exclusive can also be no symbol, just bind to visible=false for no symbol)
                  "ThinCross",      // 2 - Parallel
                  "Circle",         // 3 - Inclusive
                  "AsteriskLine",   // 4 - Complex                  
                  "Pentagon",       // 5 - double cicle event based gateway
                  "Pentagon",       // 6 - exclusive event gateway to start a process (single circle)
                  "ThickCross"]     // 7 - parallel event gateway to start a process (single circle)
    if (s < tasks.length) return tasks[s];
    return "NotAllowed"; // error
  }

function createGatewayTemplate() {
    return $(go.Node, "Vertical",
        { 	locationSpot: go.Spot.Center,
            selectionAdornmentTemplate: createIconPallete('gateway'),
			doubleClick: function(e, obj) { if(!myDiagram.isReadOnly) callParallelsPopup(obj.part.data) }
		}, new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Spot",
            $(go.Shape, "Diamond",
                {   name: "shape", fill: 'orange',
                    strokeWidth: 1, stroke: 'white',                    
                    desiredSize: new go.Size(gatewaySize, gatewaySize),
                    portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer",
                    fromSpot: go.Spot.AllSides, toSpot: go.Spot.TopLeftSides
                }
            ), $(go.Shape, "NotAllowed",
				{ 	alignment: go.Spot.Center, stroke: 'white', fill: 'white', cursor: "pointer",
					desiredSize: new go.Size(gatewaySize -22, gatewaySize -22)},
                new go.Binding("figure", "type", nodeGatewaySymbolTypeConverter),
				new go.Binding("strokeWidth", "type", function(s) { return (s <=4 ) ? 3 : 1; }),
				new go.Binding("fill", "type", function(s) { return s == 2 ? 'orange' : 'white'; })
			)
		))
}
//------------------------------------------   OTHER THiNGS   ------------------------------------------------------------

function createTooltip(type) {
	var fieldTemplate = 
		$(go.Panel, "TableRow",
			$(go.TextBlock, {
				margin: new go.Margin(0, 2), column: 1, font: defaultFont,
				alignment: new go.Spot(0, 1)
			}, new go.Binding("text", "", function(o) {  
				if(type == 'groups') return o.approved && o.approved != 0 ? o.name + ' - ' + o.date : o.name 
				else return o.condition }),
			new go.Binding("stroke", "", function(o) { 
				if(o.approved) {
					if(o.approved == 1) return 'green'
					else if(o.approved == -1) return 'red'					
				} else if(o.mandatory && o.mandatory) return 'red'
				return 'black' 
			})
		));
		
	if(type == 'groups') {
		return $(go.Adornment, "Auto",
			new go.Binding("location", "loctooltip", go.Point.parse).makeTwoWay(go.Point.stringify),
			new go.Binding("visible", "", function() { return readOnly }),
			$(go.Shape, { fill: "#f6f6f6", strokeWidth: 2, stroke: 'white' }), //FONDO
				$(go.Panel, "Vertical",
					$(go.Panel, "Auto",
						{ 	stretch: go.GraphObject.Horizontal,
							minSize: new go.Size(200, 20)},
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "GRUPOS", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ 	padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate 
						}, new go.Binding("itemArray", "Groups")
					), $(go.Panel, "Auto",
						{ stretch: go.GraphObject.Horizontal, minSize: new go.Size(200, 20) },
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "APROBADORES", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate },
						new go.Binding("itemArray", "Users"))
				)
			)
	} else if(type == 'conditions') {
		return $(go.Adornment, "Auto",
			new go.Binding("location", "loctooltip", go.Point.parse).makeTwoWay(go.Point.stringify),
			new go.Binding("visible", "", function() { return readOnly }),
			$(go.Shape, { fill: "#f6f6f6", strokeWidth: 2, stroke: 'white' }), //FONDO
				$(go.Panel, "Vertical",
					$(go.Panel, "Auto",
						{ 	stretch: go.GraphObject.Horizontal,
							minSize: new go.Size(200, 20)},
						$(go.Shape, { fill: baseColor, stroke: null }), //TITULO
						$(go.TextBlock, "CONDICIONES", {
							alignment: go.Spot.Left, margin: 5,
							stroke: "white", font: defaultFont }
					)), $(go.Panel, "Table",
						{ 	padding: 1,
							minSize: new go.Size(150, 10),
							defaultStretch: go.GraphObject.Horizontal,
							itemTemplate: fieldTemplate 
						}, new go.Binding("itemArray", "Conditions")
					)
				)
			)
	}
}

function createIconPallete(obj) {
	var posX = obj == 'step' ? 0.1 : 0
	var posY = obj == 'step' ? 1.1 : 1.2
	var spacing = obj == 'step' ? 0.15 : 0.55
	var posNewObj = obj == 'step' ? 0.85 : 1.1
    var adornment = $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
            $(go.Shape, { fill: null, stroke: "dodgerblue", strokeWidth: 0 }),
            $(go.Placeholder)
        ), $(go.Panel, "Spot",
            { 	name: 'mainAdornment',
                alignment: go.Spot.Right, alignmentFocus: go.Spot.Left,
                cursor: "pointer"
            }
        ), new go.Binding("visible", "", function() { return !myDiagram.isReadOnly })
    )

//------------------------------------------   CONFIGURATION
	adornment.add(
        $(go.Panel, "Auto",
            { 	isActionable: true,
                alignment: new go.Spot(posX, posY, 0, 0),
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("circle").fill = 'darkgray'},
                mouseLeave: function(e, obj) { obj.findObject("circle").fill = 'white' },
                click: function(e, obj) { obj.part.data.category == 'step' ? callPropertiesPopup(obj.part.data) : callParallelsPopup(obj.part.data) }
            }, $(go.Shape,
                { 	name: "circle", geometryString: gearStr,
                    fill: 'white',
                    strokeWidth: 1, stroke: 'gray',
                    desiredSize: new go.Size(configSize, configSize)
                }
            ), $(go.Shape, "Circle",
                { 	fill: backgroundColor,
                    strokeWidth: 1, stroke: 'gray',
                    desiredSize: new go.Size(8, 8)
                }
            )
        )
    )

//------------------------------------------   DELETE
	posX += spacing
	adornment.add(
        $(go.Panel, "Spot",
            { 	name: 'deleteIcon', isActionable: true,
                alignment: new go.Spot(posX, posY, 0, 0),
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("circle").fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.findObject("circle").fill = 'white' },
                click: function(e, obj) {deleteNode(obj.part.adornedPart) }
            }, $(go.Shape, "ManualOperation",
                { 	name: 'circle',  
                    strokeWidth: 0, stroke: 'gray', fill: 'white',
                    width: 15, height: 14
                }
            ), $(go.Shape,   
                { 	geometryString: trash,
					fill: 'white', strokeWidth: 1, stroke: 'gray',
					width: 16, height: 16, alignment: new go.Spot(0, 0, 8, 6)
                }
            ), new go.Binding("visible", "key", function(s) { return myDiagram.model.nodeDataArray.length > 3  })
        )
    )

//------------------------------------------   USERS
	posX += spacing
	adornment.add(
        $(go.Panel, "Spot",
            { 	name: 'userIcon', isActionable: true,
                alignment: new go.Spot(posX, posY, 0, 0),
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("user").fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.findObject("user").fill = 'white' },
                click: function(e, obj) { callGroupsPopup(obj) }
            }, $(go.Shape, "Actor",
                { 	name: 'user', 
                    strokeWidth: 1, stroke: 'gray', fill: 'white',
                    width: 16, height: 18
                }
            ), new go.Binding("visible", "key", function(s) { return s != 0 && s != "0" && obj == 'step' })
        )
    )

//------------------------------------------   NEW OBJECTS

	adornment.add(
        $(go.Panel, "Spot",
            { 	name: 'gatewayIcon', isActionable: true,
                alignment: new go.Spot(posNewObj, posY, 0, 0),
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("circle").fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.findObject("circle").fill = 'orange' },
                click: function(e, obj) { addGateway(obj.part.adornedPart) }
            }, $(go.Shape, "Diamond", 
                { 	name: 'circle', fill: 'orange',
                    strokeWidth: 0, stroke: 'gray',
                    desiredSize: new go.Size(configSize -5, configSize -5)
                }
            ), new go.Binding("visible", "key", function(s) { return s != 0 && s != "0" && obj == 'step' && gatewayVisible(s) })
        )
    )
	
	adornment.add(
        $(go.Panel, "Spot",
            { 	name: 'stepIcon', isActionable: true,
                alignment: new go.Spot(posNewObj + 0.1, posY, 0, 0),
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("circle").fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.findObject("circle").fill = baseColor },
                click: function(e, obj) { addNode(obj.part.adornedPart, 'step') }
            }, $(go.Shape, "Rectangle", 
                { 	name: 'circle', fill: baseColor,
                    strokeWidth: 1, stroke: 'white',
                    desiredSize: new go.Size(configSize -6, configSize -6)
                }
            ), new go.Binding("visible", "key", function(s) { return s != 0 && s != "0"  })
        )
    )
	
	
    return adornment
}

function createLinkIconPallete(obj) {

    var adornment = $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
            $(go.Shape, { fill: null, stroke: "dodgerblue", strokeWidth: 0 }),
            $(go.Placeholder)
        ), $(go.Panel, "Spot",
            { 	name: 'mainAdornment',
                alignment: go.Spot.Right, alignmentFocus: go.Spot.Left,
                cursor: "pointer"
            }
        ), new go.Binding("visible", "", function() { return !myDiagram.isReadOnly })
    )

//------------------------------------------   CONFIGURATION
	adornment.add(
        $(go.Panel, "Auto",
            { 	isActionable: true,
                cursor: "pointer",
                mouseEnter: function(e, obj) { obj.findObject("circle").fill = 'lightgray'},
                mouseLeave: function(e, obj) { obj.findObject("circle").fill = 'orange' },
                click: function(e, obj) { addCondition(obj.part.data) }
            }, 
			$(go.Shape, "Diamond", 
                { 	name: 'circle', fill: 'orange',
                    strokeWidth: 1, stroke: 'gray', 
                    desiredSize: new go.Size(configSize +5, configSize +5)
                }
            ), new go.Binding("visible", "", function(s) { return conditionVisible(s) })
        )
    )
	
	return adornment
}

//------------------------------------------   INFO COLORS

function infoFigure() { return { figure: 'Circle', stroke: 'white', width: 18, height: 18, margin: 5 } }
function infoText() { return { font: defaultFont, alignment: go.Spot.Left, margin: 5, stroke: 'dimgray' } }
function createInfo() {	
	if(data.workflow && data.workflow.Original != 'true') {
		myDiagram.add( $(go.Part, { layerName: "Grid", _viewPosition: new go.Point(10,100) },
			 $(go.Panel, "Table",
				$(go.Shape, { row: 1, column: 0, fill: 'blue' }, infoFigure()),
				$(go.TextBlock, "No habilitado", { row: 1, column: 1 }, infoText()),
				$(go.Shape, { row: 2, column: 0, fill: 'yellow' }, infoFigure()),
				$(go.TextBlock, "Pendiente", { row: 2, column: 1 }, infoText()),
				$(go.Shape, { row: 3, column: 0, fill: 'green' }, infoFigure()),
				$(go.TextBlock, "Aprobado", { row: 3, column: 1 }, infoText()),
				$(go.Shape, { row: 4, column: 0, fill: 'red' }, infoFigure()),
				$(go.TextBlock, "Rechazado", { row: 4, column: 1 }, infoText()),
				$(go.Shape, { row: 5, column: 0, fill: 'lightgray' }, infoFigure()),
				$(go.TextBlock, "Ignorado", { row: 5, column: 1 }, infoText())
			 )
		));
		  
		  
		  
		myDiagram.addDiagramListener("ViewportBoundsChanged", function(e) {
			var diagram = e.diagram;
			diagram.startTransaction("fix Parts");
			diagram.parts.each(function(part) {
			  if(part._viewPosition) {
				part.position = diagram.transformViewToDoc(part._viewPosition);
				part.scale = 1/diagram.scale;
			  }
			});
			diagram.commitTransaction("fix Parts");
		});
	}
}


//------------------------------------------   ELEMENTOS  --------------------------------------------------------------

var thickCross = "M7.76,27.794v5.997c0,2.209,1.791,4,4,4h12.914v12.917c0,2.209,1.791,4,4,4h6c2.209,0,4-1.791,4-4V37.792h12." +
    "913 c2.209,0,4-1.791,4-4v-5.997c0-2.209-1.791-4-4-4H38.674V10.877c0-2.209-1.791-4-4-4h-6c-2.209,0-4,1.791-4,4v12.917H11." +
    "76 C9.551,23.794,7.76,25.585,7.76,27.794z";

var gearStr = "F M 391,5L 419,14L 444.5,30.5L 451,120.5L 485.5,126L 522,141L 595,83L 618.5,92L 644,106.5" +
    "L 660.5,132L 670,158L 616,220L 640.5,265.5L 658.122,317.809L 753.122,322.809L 770.122,348.309L 774.622,374.309" +
    "L 769.5,402L 756.622,420.309L 659.122,428.809L 640.5,475L 616.5,519.5L 670,573.5L 663,600L 646,626.5" +
    "L 622,639L 595,645.5L 531.5,597.5L 493.192,613.462L 450,627.5L 444.5,718.5L 421.5,733L 393,740.5L 361.5,733.5" +
    "L 336.5,719L 330,627.5L 277.5,611.5L 227.5,584.167L 156.5,646L 124.5,641L 102,626.5L 82,602.5L 78.5,572.5" +
    "L 148.167,500.833L 133.5,466.833L 122,432.5L 26.5,421L 11,400.5L 5,373.5L 12,347.5L 26.5,324L 123.5,317.5" +
    "L 136.833,274.167L 154,241L 75.5,152.5L 85.5,128.5L 103,105.5L 128.5,88.5001L 154.872,82.4758L 237,155" +
    "L 280.5,132L 330,121L 336,30L 361,15L 391,5 Z M 398.201,232L 510.201,275L 556.201,385L 505.201,491L 399.201,537" +
    "L 284.201,489L 242.201,385L 282.201,273L 398.201,232 Z";
var user = "M24 24.082v-1.649c2.203-1.241 4-4.337 4-7.432 0-4.971 0-9-6-9s-6 4.029-6 9c0 3.096 1.797 6.191 4 7.432v1." +
	"649c-6.784 0.555-12 3.888-12 7.918h28c0-4.030-5.216-7.364-12-7.918z"	
var groups = "M10.225 24.854c1.728-1.13 3.877-1.989 6.243-2.513-0.47-0.556-0.897-1.176-1.265-1.844-0.95-1.726-1.453-3.627" + 
	"-1.453-5.497 0-2.689 0-5.228 0.956-7.305 0.928-2.016 2.598-3.265 4.976-3.734-0.529-2.39-1.936-3.961-5.682-3.961-6 0-6 " +
	"4.029-6 9 0 3.096 1.797 6.191 4 7.432v1.649c-6.784 0.555-12 3.888-12 7.918h8.719c0.454-0.403 0.956-0.787 1.506-1.146z"
	
var trash = "M6 32h20l2-22h-24zM20 4v-4h-8v4h-10v6l2-2h24l2 2v-6h-10zM18 4h-4v-2h4v2z"