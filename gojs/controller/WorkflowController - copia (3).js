var objSelected, contSteps = 0, contOthers = 0;

//------------------------------------------   MODEL   ------------------------------------------------------------------

function setModel(data) {
	var model = createModel(data.nodes)
	myDiagram.model = new go.GraphLinksModel(model.nodesArray, model.linksArray)
}

function setDefaultModel(nodesArray, linksArray) {
	nodesArray.push(createNode('', 'step', 0, 'init', 'Nuevo Paso'))
	linksArray.push(createLink('init', 'step-0'))
	nodesArray.push(createNode('end', 'event', 0, 'step-0'))
	linksArray.push(createLink('step-0', 'end'))
	return { "nodesArray": nodesArray , "linksArray": linksArray }
}

function createModel(nodes) {
	contSteps = -1; contOthers = -1
	var nodesArray =  [ { key: "init", category: "event", type: "1" } ]
	var linksArray = []
	if(nodes && nodes.length > 0) {	
		console.log('Cargando modelo...')
		nodes.forEach(function(step) {
			var addLink = true, existNode = getStep(nodesArray, step.step.StepId);
			if(!existNode) {
				nodesArray.push(step)
				if(step.step.Paralelo > 0) {	
					var idGateway = 'gateway-' + step.parent
					var gateway = getNode(nodesArray, idGateway)
					if(gateway == null) {
						gateway = createNode(idGateway, 'gateway', step.step.Paralelo -1, step.key)
						nodesArray.push(gateway)
						linksArray.push(createLink (step.parent, idGateway))
					}	
					linksArray.push(createLink (idGateway, step.key, step.step.UnionConCondicional ? true : false, step.step.Conditions))
					addLink = false
				}
				if(step.step.UltimoPaso) {
					nodesArray.push(createNode('end', 'event', '0', step.key))
					linksArray.push(createLink(step.key, 'end'))				
				}
			} else  { 
				linksArray.push(createLink(step.parent, existNode.key)) 
			}
			if(addLink) linksArray.push(createLink(step.parent, step.key, step.step.UnionConCondicional ? true : false, step.step.Conditions))
		})
		if(nodesArray.length == 2) {
			nodesArray.push(createNode('end', 'event', 0, nodesArray[1].key))
			linksArray.push(createLink(nodesArray[1].key, 'end'))
		}
		return { "nodesArray": nodesArray , "linksArray": linksArray }
	} else return setDefaultModel(nodesArray, linksArray)
}

function updateModel() {
	myDiagram.model = new go.GraphLinksModel(myDiagram.model.nodeDataArray, myDiagram.model.linkDataArray)
}

function checkModel() {
	console.log('checkModel: ', myDiagram.model.nodeDataArray)
	var check = true
	myDiagram.model.nodeDataArray.forEach(function(node) {
		if(node.category == 'step') {
			if(node.key == node.step.StepId) { node.error = true; check = false }
			if(!node.step.Groups && node.step.Users) { node.error = true; check = false }
		} else if(node.category == 'gateway') {
			if(node.type == -1)  {
				node.error = true; check = false
			}
			var links = getLinkByKey(node.key, 'from')
			if(links.length == 0 || links[0].to == 'end') {
				node.error = true; check = false
			}
		}
	})
	updateModel()
	return check
}

//------------------------------------------   NODES   ------------------------------------------------------------------

function getStep(list, id) {
	var exist = null
	list.forEach(function(obj) {		
		if(obj.category == 'step' && obj.step.StepId == id)  { exist = obj }
	})
	return exist
}

function getNode(list, key) {
	var exist = null
	list.forEach(function(obj) {
		if(obj.key == key)  { exist = obj }
	})
	//console.log('Existe key: ' + key, exist)
	return exist
}

function getAllNodes() {
	var nodes = new Array()
	myDiagram.model.nodeDataArray.forEach(function(node){
		if(node.key != 'end')
			nodes.push(node)
	})
	return nodes
}

function createNode(key, category, type, parent, text) {
	//console.log('createNode: ', contSteps)
	testing = data.workflow ? false : true
	if(key == null || key == '') {
		if(category == 'step') key = 'step-' + ++contSteps
		else key = category + '-' + ++contOthers
	}
	var node = {
        key : key,
        category: category,
		type: type,
		parent: parent,
		error: false
	}
	if(category == 'step') {
		node.text = text != undefined ? text : "Nuevo Paso " + (contSteps > 0 ? contSteps : '')
		node.step = {
			StepId: node.key,
			Title: node.text,
			Description: '',
			IdState: 1,
			GenerateAlerts: !testing ? data.workflow.GeneraAlertas : false,
			RechazoEnFormularios: false,
			WorkflowID: !testing ? data.workflow.WorkflowID : '',
			dependsON: parent
		}		
	}
	//console.log('createNode: ', node)
	return node
}

function addNode(node, type) {
	
	var newnode, newlink;		
	var parentNode = node.data.key
	
	switch (type) {
		case 'step':
			if(node.data.category != 'step') parentNode =  node.data.key.substring(0, node.data.key.length-1)	
			newnode = createNode('', 'step', 0, parentNode)
			if(node.data.category == 'step' && node.data.key != node.data.step.StepId)
				newnode.step.dependsON = node.data.step.StepId
			break;
		default:
			newnode = createNode('', type, -1, parentNode)
	}
	
	myDiagram.startTransaction("addNode")
	
	myDiagram.model.addNodeData(newnode)
	myDiagram.model.addLinkData(createLink (node.data.key, newnode.key))
	
	var links = getLinkByKey(node.data.key)
	if(type != 'step') {	
		links.forEach(function(link) { if(link.to != newnode.key) changeProperty(link, 'from', newnode.key) })
	} else if(links.length == 2) {
		links.forEach(function(link) { 
			if(link.to != newnode.key && myDiagram.findNodeForKey(link.to).data.category == 'event') 
				changeProperty(link, 'from', newnode.key) })
	}
	
	myDiagram.commitTransaction("addNode")
	if(newnode.category == 'step') newnode.step.StepId = newnode.key
	updateModel()
	return newnode
}

function moveNode(node, toKey) {
	console.log('links: ', getLinkByKey(node.key, 'to'))
	var links = getLinkByKey(node.key, 'to')
	links[0].from = toKey
	node.parent = toKey
	var linksToNode = getLinkByKey(toKey, 'from')
	linksToNode.forEach(function(link){
		if(link.to != node.key) {
			if(link.to == 'end' && linksToNode.length > 2) {
				myDiagram.model.removeLinkData(link)
				myDiagram.model.removeNodeData(getNode(myDiagram.model.nodeDataArray, 'end'))
			} else if(link.to == 'end' && linksToNode.length == 2) link.from = node.key
		}
	})
}

function deleteNode(node) {
	node = node.data ? node.data : node
	var links = getLinkByKey(node.key, 'to')
	if(links.length > 0) {
		var parent = links[0].from
		links.forEach(function(link) { myDiagram.model.removeLinkData(link) })				
		getLinkByKey(node.key).forEach(function(link) { 
			console.log('delete: ', link)
			changeProperty(link, 'from', parent) 
			var obj = myDiagram.findNodeForKey(link.to).data
			obj.parent = parent
			if(node.category == 'step')
				obj.step.dependsON = parent
		})		 
	} 
	myDiagram.model.removeNodeData(node)
	updateModel()
}

function confirmDeleteNode(node) {
	console.log('Seguro quiere eliminar el nodo?')
	if(testing) deleteNode(node)
	else if(node.data.category == 'step') {
		if(node.data.key == node.data.step.StepId)
			deleteNode(node)
		else deleteStepPopup(node.data)
	} else if(node.data.category == 'gateway') {		
		if(node.type == -1)  
			deleteNode(node) 
		else {
			node.data.childs = new Array()
			getLinkByKey(node.data.key).forEach(function(link) {
				node.data.childs.push(myDiagram.findNodeForKey(link.to).data)
			})
			deleteGatewayPopup(node.data)
		}
	} else if(node.data.key != 'init') deleteNode(node)
}

function returnDeleteNode(result) {
	console.log('returnDeleteNode: ', result)
	if(result.remove) { 
		/*getLinkByKey(result.node.key).forEach(function(link) {
			var node = myDiagram.findNodeForKey(link.to).data
			node.parent = result.node.parent
		})*/
		deleteNode(result.node)	
	}
}

//------------------------------------------   LINKS   ------------------------------------------------------------------

function createLink(from, to, condition, listConditions) { 
	condition = condition == undefined ? false : condition
	return { from: from, to: to, condition: condition, conditions: condition ? listConditions : {}}  
}

function getLinkByKey(key, direction) {
	var linksArray = myDiagram.model.linkDataArray, linkList = new Array();
	linksArray.forEach(function(link) {
		if(direction == 'to') { if(link.to === key) linkList.push(link) }
		else if(link.from === key) linkList.push(link)
	})
	return linkList
}

//------------------------------------------   ZOOM   -------------------------------------------------------------------

function increaseZoom() { myDiagram.commandHandler.increaseZoom(); }
function decreaseZoom() { myDiagram.commandHandler.decreaseZoom(); }

//------------------------------------------   PROPERTIES   ------------------------------------------------------------------

function changeProperty(obj, property, value) {
	//console.log('changeProperty: ' + obj.key + ' ' + property + ': ' + value)
    myDiagram.startTransaction("changeProperty");
    myDiagram.model.setDataProperty(obj, property, value);
    myDiagram.commitTransaction("changeProperty");
}

function objectProperties(obj) {
	objSelected = obj
	obj.testing = data.workflow ? false : true
	if(obj.category == 'step') {		
		if(obj.step.dependsON == 0) obj.step.dependsON = 'init'
		obj.childs = getLinkByKey(obj.key)
		createStepProperties(obj)
	} else if(obj.category == 'gateway') {
		obj.childs = new Array()
		getLinkByKey(obj.key).forEach(function(link) {
			obj.childs.push(myDiagram.findNodeForKey(link.to).data)
		})
		createParallelsProperties(obj)
	}
}

function returnObjectProperties(error, result) {
	console.log('returnObjectProperties: ', 'result: ', result, 'objSelected: ', objSelected)
	if(error) objSelected.error = true
	else if(result.change) {
		if(objSelected.category == 'step') {
			objSelected.step = result.data
			changeProperty(objSelected, 'step', result.data)
			objSelected.text = objSelected.step.Title
			objSelected.childs.forEach(function(node) {
				if(node.to.length > 7 && node.to.substring(0, 7) == 'gateway') {
					getLinkByKey(node.to, 'from').forEach(function(link) {
						var obj = myDiagram.findNodeForKey(link.to).data
						if(obj.category == 'step') obj.step.dependsON = objSelected.step.StepId						
					})
				} else myDiagram.findNodeForKey(node.to).data.step.dependsON = objSelected.step.StepId
			})			
		} else if(objSelected.category == 'gateway') {
			objSelected.type = result.data.union
			var links = getLinkByKey(objSelected.key, 'from')
			console.log(links)
			if(links.length != result.data.childs.length) {
				links.forEach(function(link) {
					var exist = false
					for(var i = 0; i < result.data.childs.length; i++) {
						if(result.data.childs[i].key == link.to) exist = true
					}
					if(!exist) { link.from = objSelected.parent }
				})
			}
		}
		objSelected.error = false
	}
	updateModel()
}

function groupsProperties(obj) {
	objSelected = obj
	obj.testing = data.workflow ? false : true
	createGroupsProperties(obj)
}

function returnGroupsProperties(result) {
	console.log('returnGroupsProperties: ', 'result: ', result)
	if(result.change) {
		//objSelected.step = result.data
		//changeProperty(objSelected, 'step', result.data)
	}
}

function dependenceProperties(obj) {
	console.log('dependenceProperties: ', obj)
	obj.testing = data.workflow ? false : true
	obj.firstStep = { StepId: myDiagram.model.nodeDataArray[1].step.StepId, IdForm: myDiagram.model.nodeDataArray[1].step.IdForm }
	var fromNode = myDiagram.findNodeForKey(obj.from).data
	if(fromNode.category == "gateway") {		
		obj.fromNode = myDiagram.findNodeForKey(obj.from.substring(8)).data
	} else obj.fromNode = fromNode
	obj.toNode = myDiagram.findNodeForKey(obj.to).data
	createDependsProperties(obj)
}

function returnDependenceProperties(result) {
	console.log('returnDependenceProperties: ', 'result: ', result)
	if(result.change) {
		var link
		getLinkByKey(result.data.from).forEach(function(l){
			if(l.to == result.data.to) link = l
		})
		changeProperty(link, 'condition', result.data.condition)
		var step = myDiagram.findNodeForKey(result.data.to).data
		//changeProperty(step.step, 'UnionConCondicional', result.data.condition)
		step.step.UnionConCondicional = result.data.condition
		step.step.Conditions = result.data.conditions
	}
}

//------------------------------------------   VISIBLES   ------------------------------------------------------------------

function visibleAddGateway(nodekey) {
	var links = getLinkByKey(nodekey)
	if(links.length > 1) {
		var childs = new Array()
		links.forEach(function(link) { childs.push(myDiagram.findNodeForKey(link.to).data) })
		return true
	} else return false	
}

function conditionVisible(link) {
	if(link.condition == true) return false
	else if(link.from == 'init' || link.to == 'end') return false
	else if(link.to.substring(0, 7) == 'gateway') return false
	return true
}
