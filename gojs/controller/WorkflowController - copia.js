var objSelected
function getJSONModel() { return myDiagram.model.toJSON() }
function setModel(data) { myDiagram.model = go.Model.fromJson(createModel(data)); completeModel()}
function createModel(data) {
	var model = defaultModel()
	if(data.nodes.length > 0) {
		data.nodes.forEach(function(step) {  model.nodeDataArray.push(step) })		
	} else model.nodeDataArray.push(createEmptyNode(0,1))
	console.log('Model: ', model)
	return model
}

function defaultModel() {
    var model = {
        "class": "go.TreeModel",
		"nodeDataArray": [ { "key": "1", "category": "event", "type": "1" } ],
		"linkDataArray": [ { "from": "6", "to": "3"} ]
    }
    return model
}

function completeModel(model) {
	var contEvent = 1
	myDiagram.model.nodeDataArray.forEach(function(node) {
		if(node.category == 'step') {
			var obj = myDiagram.findNodeForKey(node.key)
			var outNode = null, it = obj.findNodesOutOf(), gateways = new Array();
			while (it.next()) { 
				outNode = it.value.data
				if(outNode.category == 'gateway') gateways.push(outNode)
			}
			if(!outNode) { myDiagram.model.addNodeData(createNode(contEvent++ +'0', 'event', "0", node.key)) }
			else {				
				if(gateways.length > 1) {
					//console.log('gateways: ', gateways) 
					var gate = gateways[0]
					gateways.forEach(function(gateway) {						
						if(gateway.key != gate.key) {
							gateway = myDiagram.findNodeForKey(gateway.key)
							it = gateway.findNodesOutOf()							
							myDiagram.model.removeNodeData(gateway.data)
							while (it.next()) { changeProperty(it.value.data, "parent", gate.key) }
						}
					})
				}
			}
			
		}
	})
}

function callPropertiesPopup(obj) {
    objSelected = obj
    createStepProperties(obj)
}

function callDependsPopup(obj) {
    objSelected = obj
	obj.IdForm = myDiagram.model.nodeDataArray[1].IdForm
	obj.conections = { 
		parent: myDiagram.findNodeForKey(obj.parent).data, 
		childs: getChilds(obj) }
	createDependsProperties(obj)
}

function callGroupsPopup(obj) {
	createGroupsProperties(obj.part.data)
}

function changeProperty(obj, property, value) {
	//console.log('changeProperty: ' + obj.key + ' ' + property + ': ' + value)
    myDiagram.startTransaction("changeProperty");
    myDiagram.model.setDataProperty(obj, property, value);
    myDiagram.commitTransaction("changeProperty");
}

function setProperties(data) {
    for(var index in data) {
        //console.log(index + ': ' + data[index])
        changeProperty(objSelected, index, data[index])
        /*if(index == 'RechazoEnFormularios') {
            if(data[index]) {
                addNodeAndLink(objSelected, 'gateway')
            }
        }*/
    }
}

function addNode(obj, type) {	
	if(isSaved()) {
		var node = obj.part.adornedPart, newnode;
		myDiagram.startTransaction("addNode")
		
		var outNode = null, it = node.findNodesOutOf(), nodesCont = 0;
		while (it.next()) { nodesCont++ }	
		//console.log('Salidas: ', nodesCont)
		
		switch (type) {
			case 'step':
				newnode = createEmptyNode("0", node.data.key); break;
			default:
				newnode = createNode('0', type, 1, node.data.key)
		}
		
		myDiagram.model.addNodeData(newnode)	
		
		if(type != 'step' && nodesCont > 0) {
			it = node.findNodesOutOf()
			while (it.next()) { 
				outNode = it.value.data 
				if(newnode != outNode) changeProperty(outNode, "parent", newnode.key)
			}	
		} else if(type == 'step' && nodesCont > 0) {
			outNode = it.value.data
			if(outNode.category == 'event' && outNode.type == "0") 
				changeProperty(outNode, "parent", newnode.key)
			//else { myDiagram.model.addNodeData(createNode('10', 'event', "0", "0")) }
		}
		
		myDiagram.commitTransaction("addNode")
		return newnode
	} else Ext.Msg.alert('ERROR', 'Aun tiene un paso incompleto')
}

function isSaved() {
	var saved = true
	myDiagram.model.nodeDataArray.forEach(function(node) {		
		if(node.key == '0' || node.key == 0) { saved = false;  }
	})
	return saved
}

function deleteNode(obj) {
	var node = obj.part.adornedPart
	if(node.data.key == "0") {
		var parent = myDiagram.findNodeForKey(node.data.parent)
		//console.log('Parent: ', parent.data)
		var outNode = null, childs = 0, it = parent.findNodesOutOf();
		while (it.next()) { childs++ }	
		it = node.findNodesOutOf();
		while (it.next()) { outNode = it.value.data }		
		myDiagram.model.removeNodeData(outNode)
		myDiagram.model.removeNodeData(node.data)
		if(childs == 1) myDiagram.model.addNodeData(createNode('10', 'event', "0", parent.data.key))
	} else deleteWithOutPopup(node.data)
}

function createEmptyNode(key, parent) {
	return {
        key : key,
		StepId: key,
		category: "step",
        text: "Nuevo Paso",
		Title: "Nuevo Paso",        
		IdState: "1",
		WorkflowID: data.workflow.WorkflowID,
        Description: "",
        StartDate: data.workflow.StartDate,
        EndDate: data.workflow.EndDate,
        GenerateAlerts: false,
        TimeBeforeAlertId: 0,
        ExtraReceptorsMails: "",
        Dias: 1,
        IdState: 1,
        IdForm: '',
        FormReadOnly: false,
        AllowDocs: false,
        RechazoEnFormularios: false,
        NotificaViaMail: false,
        MailsaNotificar: "",
        NotificaAprobaciones: false,
        NotificaRechazos: false,
		Groups: '',
		Users: '',
		parent: parent
	}
}

function createNode(key, category, type, parent) {
	return {
        key : key,
        category: category,
		type: type,
		parent: parent
	}
}

function getChilds(obj) {
	obj = myDiagram.findNodeForKey(obj.key)
	var it = obj.findNodesOutOf(), childs = new Array();
	while (it.next()) {  childs.push(it.value.data) }
	return childs
}

function gatewayVisible(key) {	
	var obj = myDiagram.findNodeForKey(key).data
	var childs = getChilds(obj)
	if(childs.length == 1 && childs[0].category == 'event')
		return false
	else return true
}

function testModel() {
    var model = { "class": "go.TreeModel" }
    model.nodeDataArray = [
		{ category: "event", key: "0", text: "" },
		createEmptyNode(10, 0),
		{ category: "gateway", key: "20", text: "", parent: '10' }
    ]
    model.linkDataArray = []
    console.log(model)
    return model
}

function increaseZoom() { myDiagram.commandHandler.increaseZoom(); }
function decreaseZoom() { myDiagram.commandHandler.decreaseZoom(); }