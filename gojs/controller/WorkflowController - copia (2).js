var objSelected

//------------------------------------------   MODEL   ------------------------------------------------------------------

function setModel(data) {
	var model = createModel(data.nodes)
	myDiagram.model = new go.GraphLinksModel(model.nodesArray, model.linksArray)
}

function updateModel() {
	myDiagram.model = new go.GraphLinksModel(myDiagram.model.nodeDataArray, myDiagram.model.linkDataArray)
}

function getJSONModel() { return myDiagram.model.toJSON() }

function createModel(nodes) {
	var nodesArray =  [ { key: "1", category: "event", type: "1" } ]
	var linksArray = []
	if(nodes && nodes.length > 0) {		
		nodes.forEach(function(step) { 
			var addLink = true
			if(!existNode(nodesArray, step.key)) {
				//console.log(step)
				nodesArray.push(step)				
				if(step.Paralelo > 0) {	
					var idGateway = step.parent + '0'
					var gateway = getNode(nodesArray, idGateway)
					if(gateway == null) {
						gateway = createNode(idGateway, 'gateway', step.Paralelo -1, step.key)
						nodesArray.push(gateway)
						linksArray.push(createLink (step.parent, idGateway))
					}					
					linksArray.push(createLink (idGateway, step.key))
					addLink = false
				}
				if(step.UltimoPaso) {
					nodesArray.push(createNode('end', 'event', '0', step.key))
					linksArray.push(createLink(step.key, 'end'))				
				}
			}
			if(addLink) linksArray.push(createLink(step.parent, step.key, step.UnionConCondicional ? true : false, step.Conditions))
		})
	} else { 
		nodesArray.push(createEmptyNode(0,1)) 
		linksArray.push(createLink('1', 0))
		nodesArray.push(createNode('end', 'event', '0', 0))
		linksArray.push(createLink(0, 'end'))
	}
	return { "nodesArray": nodesArray , "linksArray": linksArray }
}

//------------------------------------------   NODES   ------------------------------------------------------------------
function existNode(list, key) {
	var exist = false
	list.forEach(function(obj) {
		if(obj.key == key)  { exist = true }
	})
	//console.log('Existe key: ' + key, exist)
	return exist
}

function getNode(list, key) {
	var exist = null
	list.forEach(function(obj) {
		if(obj.key == key)  { exist = obj }
	})
	//console.log('Existe key: ' + key, exist)
	return exist
}

function isSaved() {
	var saved = true
	myDiagram.model.nodeDataArray.forEach(function(node) {		
		if(node.key == '0' || node.key == 0) { saved = false;  }
	})
	return saved
}

function addNode(node, type) {
	if(isSaved()) {
		var newnode, newlink;
		
		var newId = '0'
		
		switch (type) {
			case 'step':
				newnode = createEmptyNode(newId, node.data.category == 'step' ? node.data.key : node.data.key.substring(0, node.data.key.length-1))
				break;
			default:
				newnode = createNode(newId, type, 1, node.data.key)
		}
		
		myDiagram.startTransaction("addNode")
		
		myDiagram.model.addNodeData(newnode)
		myDiagram.model.addLinkData(createLink (node.data.key, newnode.key))
		
		var links = getLinkByKey(node.data.key)
		if(type != 'step') {	
			links.forEach(function(link) { if(link.to != newnode.key) changeProperty(link, 'from', newnode.key) })
		} else if(links.length == 2) {
			links.forEach(function(link) { 
				if(link.to != newnode.key && myDiagram.findNodeForKey(link.to).data.category == 'event') 
					changeProperty(link, 'from', newnode.key) })
		}
	
		myDiagram.commitTransaction("addNode")
		updateModel()
		return newnode
	}
}

function deleteNode(node, mandatory) {
	if(mandatory || node.data.key == "0") {
		var links = getLinkByKey(node.data.key, 'to')
		var parent = links[0].from
		links.forEach(function(link) { myDiagram.model.removeLinkData(link) })
		myDiagram.model.removeNodeData(node.data)		
		links = getLinkByKey(node.data.key)
		links.forEach(function(link) { changeProperty(link, 'from', parent) })
		updateModel()
	} else {
		if(node.data.category == 'step') deleteStepWithOutPopup(node.data)
		else if(node.data.category == 'gateway') {
			node.data = completeGateway(node.data)
			deleteGatewayWithOutPopup(node.data)
		}
	}
}

function createNode(key, category, type, parent) {
	return {
        key : key,
        category: category,
		type: type,
		parent: parent
	}
}

function createEmptyNode(key, parent) {
	return {
        key : key,
		StepId: key,
		category: "step",
        text: "Nuevo Paso",
		Title: "Nuevo Paso",        
		IdState: "1",
		WorkflowID: testing ? data.workflow.WorkflowID : key,
        Description: "",
        StartDate: data.workflow ? data.workflow.StartDate : null,
        EndDate: data.workflow ?  data.workflow.EndDate : null,
        GenerateAlerts: false,
        TimeBeforeAlertId: 0,
        ExtraReceptorsMails: "",
        Dias: 1,
        IdState: 1,
        IdForm: '',
        FormReadOnly: false,
        AllowDocs: false,
        RechazoEnFormularios: false,
        NotificaViaMail: false,
        MailsaNotificar: "",
        NotificaAprobaciones: false,
        NotificaRechazos: false,
		Groups: '',
		Users: '',
		parent: parent
	}
}

//------------------------------------------   LINKS   ------------------------------------------------------------------

function createLink(from, to, condition, listConditions) { 
	condition = condition == undefined ? false : condition
	return { from: from, to: to, condition: condition, Conditions: listConditions} 
}

function getLinkByKey(key, direction) {
	var linksArray = myDiagram.model.linkDataArray, linkList = new Array();
	linksArray.forEach(function(link) {
		if(direction == 'to') { if(link.to === key) linkList.push(link) }
		else if(link.from === key) linkList.push(link)
	})
	return linkList
}

//------------------------------------------   ZOOM   -------------------------------------------------------------------

function increaseZoom() { myDiagram.commandHandler.increaseZoom(); }
function decreaseZoom() { myDiagram.commandHandler.decreaseZoom(); }

//------------------------------------------   OTHERS   ------------------------------------------------------------------

function addCondition(obj) {
	obj.condition = true
	updateModel()
	callDependsPopup(obj)
}

function addGateway (obj) {
	var node = addNode(obj, 'gateway')	
	callParallelsPopup(node)
}

function changeProperty(obj, property, value) {
	//console.log('changeProperty: ' + obj.key + ' ' + property + ': ' + value)
    myDiagram.startTransaction("changeProperty");
    myDiagram.model.setDataProperty(obj, property, value);
    myDiagram.commitTransaction("changeProperty");
}

function callPropertiesPopup(obj) {
    objSelected = obj
	obj.childs = getLinkByKey(obj.key)
    createStepProperties(obj)
}

function callDependsPopup(obj) {
    objSelected = obj
	obj.firstStep = { StepId: myDiagram.model.nodeDataArray[1].key, IdForm: myDiagram.model.nodeDataArray[1].IdForm }
	var fromNode = myDiagram.findNodeForKey(obj.from)
	if(fromNode.data.category == "gateway")
		obj.fromNode = myDiagram.findNodeForKey(fromNode.data.key.substring(0, fromNode.data.key.length -1)).data
		else obj.fromNode = fromNode.data
	obj.toNode = myDiagram.findNodeForKey(obj.to).data
	createDependsProperties(obj)
}

function callGroupsPopup(obj) {
	createGroupsProperties(obj.part.data)
}

function completeGateway(obj) {
	obj.childs = new Array()
	getLinkByKey(obj.key).forEach(function(link) {
		obj.childs.push(myDiagram.findNodeForKey(link.to).data)
	})
	return obj
}

function callParallelsPopup(obj) {
	obj = completeGateway(obj)
	createParallelsProperties(obj)
}

function gatewayVisible(key) {
	var visible = true
	var childs = getLinkByKey(key)
	if(childs.length <= 1) visible = false 
	return visible
}

function conditionVisible(link) {
	if(link.condition == true) return false
	else if(link.from == '1' || link.to == '0' || link.to == 'end') return false
	return true
}