Ext.define('Designer.Application', {
    extend: 'Ext.app.Application',

    name: 'Designer',
	
	requires: [ 'Designer.util.Utils' ],

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },
	
	config: {
		txtField: {},
		loginUser: null
	},
	
	launch: function() {		
		var test = Designer.util.Utils.getQueryVariable('test'),
			lang = Designer.util.Utils.getQueryVariable('lang');
		if(lang) globalParams.lang = lang
		if(test) globalParams.testing = test == 'true' ? true : false	
		
		if(test)  {
			loginUser = {
				idUser: 1,
				name: "Administrador SGP",
				profile: 1,
				token: "eeb80b13-68a1-4a9c-b1c8-250809df4029",
				username: "admin"
			}
			Ext.create('Designer.view.main.Main')
		} else  {
			var userId = Designer.util.Utils.getQueryVariable('g_loggedUserId')
			var token = Designer.util.Utils.getQueryVariable('g_token')
			if(userId && token) this.autoLogin(userId, token)
			else this.validateSession()
		}
		this.destroyLoader()
	},
	
	autoLogin: function(userId, token) {
		Ext.Ajax.request({
            url: globalParams.urlServices + 'session/validateToken?userId=' + userId + '&token=' + token,
            method: 'POST',
            success: function (response) {
				console.log('SUCCESS...', response)
                response = JSON.parse(response.responseText)
				console.log(response)
				if(response.IdUser) {
					loginUser = {
						idUser: response.IdUser,
						name: response.FirstName + ', ' + response.LastName,
						profile: response.IdProfile,
						token: token,
						username: ""
					}									
					Ext.create('Designer.view.main.Main')					
				} else {
					Ext.Msg.alert('ERROR', 'Identificador o token incorrectos')	
					Ext.create('Designer.view.screen.Lock')
				}
            },
            failure: function (error) {
				console.log('FAIL...', error)
				error = JSON.parse(error.responseText)[0]
				Ext.Msg.alert('ERROR', error.message)
				Ext.create('Designer.view.screen.Lock')                
            }
		})
	},
	
	validateSession: function() {
		Ext.Ajax.request({
            url: globalParams.urlServices + 'session/validate',
            method: 'GET',
            success: function (response) {
                response = Ext.decode(response.responseText);
                loginUser = {
                    idUser: response.user.LoginResult,
                    username: response.user.UserName,
                    name: response.user.Name,
                    legajo: response.user.Legajo,
                    token: response.user.Token
                }
                Ext.create('Designer.view.main.Main')
            },
            failure: function () {
                Ext.create('Designer.view.screen.Lock')
            }
		})
	},
	
	destroyLoader: function () {
        var circles = Ext.fly('loadingSplashCircles'),
            bottom = Ext.get('loadingSplashBottom'),
            top = Ext.get('loadingSplashTop'),
            wrapper = Ext.fly('loadingSplash');

        if (circles) {
            circles.destroy();
        }
    }
});
