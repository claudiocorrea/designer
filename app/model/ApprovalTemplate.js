Ext.define('Designer.model.ApprovalTemplate', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdTemplate', type: 'string' },
        {  name: 'Name', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'Template', type: 'string' },
        {  name: 'Cp', type: 'string' }
    ]

})