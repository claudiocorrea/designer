Ext.define('Designer.model.Main', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'Designer',
		currentView: null,
        txtFields: {},
		loginUser: {}
    },

	formulas: {
        labels: function (key) {
			var me = this;
			//console.log("key: ", key);
			//console.log("This VM: ", this);
			console.log("Cargando el locale: " + Ext.String.format('./locale/locale-{0}.js', appParams.lang));
			Designer.util.Utils.loadScript(Ext.String.format('./locale/locale-{0}.js', appParams.lang), function(err, callback){
				if (!err) { me.set({ txtFields: txtFields })}
			});
        }
    }

});
