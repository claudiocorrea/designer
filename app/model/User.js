Ext.define('Designer.model.User', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdUser', type: 'int' },
        {  name: 'Login', type: 'string' },
		{  name: 'FirstName', type: 'string' },
		{  name: 'LastName', type: 'string' },
		{  name: 'FullName', type: 'string' },
		{  name: 'PasswordSql', type: 'string' },
		{  name: 'SPUser', type: 'string' },
		{  name: 'Legajo', type: 'string' },
        {  name: 'Enabled', type: 'boolean' },
        {  name: 'Email', type: 'string' },
        {  name: 'IdProfile', type: 'int', convert: function(value, record) { return value ? value[0] : undefined } },
		{  name: 'profile', type: 'string' },
        {  name: 'TipoSistema', type: 'boolean' },
    ]

})