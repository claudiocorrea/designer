Ext.define('Designer.model.TimeAlert', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'TimeBeforeAlertsID', type: 'string' },
        {  name: 'Title', type: 'string' },
        {  name: 'Hours', type: 'string' }
    ]

})