Ext.define('Designer.model.DependenceCondition', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'idcondition', type: 'string' },
        {  name: 'Step', type: 'string' },
        {  name: 'condition', type: 'string' }
    ]

})