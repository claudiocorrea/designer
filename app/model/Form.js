Ext.define('Designer.model.Form', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdForm', type: 'string' },
        {  name: 'FormName', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'Code', type: 'string' },
        {  name: 'Original', type: 'boolean' },
		{  name: 'StyleSheet', type: 'string' },
		{  name: 'InfoPathTemplate', type: 'string' },
        {  name: 'IdEmpresa', type: 'string' },
        {  name: 'IdFormType', type: 'string' },
        {  name: 'Columnas', type: 'string' }
    ]

})