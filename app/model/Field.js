Ext.define('Designer.model.Field', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdField', type: 'int' },
        {  name: 'FieldName', type: 'string' },
        {  name: 'FrontendText', type: 'string' },
        {  name: 'Section', type: 'string' },
        {  name: 'Habilitado', type: 'boolean' },
        {  name: 'Visible', type: 'boolean' }
    ]

})