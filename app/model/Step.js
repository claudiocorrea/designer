Ext.define('Designer.model.Step', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'WorkflowID', type: 'string' },
        {  name: 'StepId', type: 'string' },
        {  name: 'Title', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'Category', type: 'string' },
        {  name: 'StartDate', type: 'string' },
        {  name: 'EndDate', type: 'string' },
        {  name: 'GenerateAlerts', type: 'boolean' },
        {  name: 'AllowDocs', type: 'boolean' },
        {  name: 'TimeBeforeAlertId', type: 'int' },
        {  name: 'ExtraReceptorsMails', type: 'string' },
        {  name: 'Dias', type: 'int' },
        {  name: 'IdState', type: 'int' },
        {  name: 'IdForm', type: 'int' },
        {  name: 'FormReadOnly', type: 'boolean' },
        {  name: 'WorkFlowNested', type: 'int' },
        {  name: 'RechazoEnFormularios', type: 'boolean' },
        {  name: 'NotificaViaMail', type: 'boolean' },
        {  name: 'MailsaNotificar', type: 'string' },
        {  name: 'NotificaAprobaciones', type: 'boolean' },
        {  name: 'NotificaRechazos', type: 'boolean' },
        {  name: 'Groups', type: 'string' },
		{  name: 'Users', type: 'string' },
		{  name: 'UnionConCondicional', type: 'boolean' },
		{  name: 'UltimoPaso', type: 'boolean' },
		{  name: 'Paralelo', type: 'int' },
		{  name: 'IdTemplate', type: 'int' },
		{  name: 'IdTemplateAlerts', type: 'int' },
    ]

})