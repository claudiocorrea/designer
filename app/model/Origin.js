Ext.define('Designer.model.Origin', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdOrigen', type: 'string' },
        {  name: 'Nombre', type: 'string' },
        {  name: 'Tipo', type: 'string' },
        {  name: 'Servidor', type: 'string' },
        {  name: 'Base', type: 'string' },
        {  name: 'SeguridadIntegrada', type: 'string' },
        {  name: 'Usuario', type: 'string' },
        {  name: 'Clave', type: 'string' },
        {  name: 'Servicio', type: 'string' },
        {  name: 'Puerto', type: 'string' }
    ]

})