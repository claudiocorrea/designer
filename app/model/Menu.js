Ext.define('Designer.model.Menu', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.menu', 
	
	stores: {
        navItems: {
            type: 'tree',
            root: {
                expanded: false,
                children: [
					{	text: 'Workflows',
						iconCls: 'x-fa fa-files-o',
						routeId: 'workflows',
						leaf: true
					}, {
						text: 'Formularios',
						iconCls: 'x-fa fa-wpforms',
						routeId: 'forms',
						leaf: true
					}, {
						text: 'Reportes',
						iconCls: 'x-fa fa-clipboard',
						click: false,
						children: [
							{ 	text: 'Eventos',
								routeId: 'frame',
								data: {
									title: 'eventos',
									url: globalParams.urlOldVersion + 'adm/EventViewer.aspx?token='
								},
								leaf: true
							}, { 	
								text: 'Panel de control',
								routeId: 'frame',
								data: {
									title: 'eventos',
									url: globalParams.urlOldVersion + 'wf/DD_Categories.aspx?token='
								},
								leaf: true
							}
						]
					}, {
						text: 'Administracion',
						iconCls: 'x-fa fa-cube',
						click: false,
						children: [
							{ 	text: 'Empresas',
								routeId: 'enterprises',
								/*data: {
									title: 'companies',
									url: globalParams.urlOldVersion + 'adm/CompaniesList.aspx?token='
								},*/
								leaf: true
							}, { 	
								text: 'Grupos y roles',
								routeId: 'frame',
								data: {
									title: 'groups',
									url: globalParams.urlOldVersion + 'adm/GroupsList.aspx?token='
								},
								leaf: true
							}, { 	
								text: 'Usuarios',
								routeId: 'users',
								/*data: {
									title: 'users',
									url: globalParams.urlOldVersion + 'adm/userslist.aspx?token='
								},*/
								leaf: true
							}, { 	
								text: 'Empresas por usuarios',
								routeId: 'frame',
								data: {
									title: 'companyUsers',
									url: globalParams.urlOldVersion + 'adm/CompanyUsers.aspx?token='
								},
								leaf: true
							}, { 	
								text: 'Categorias',
								routeId: 'categories',
								/*data: {
									title: 'categories',
									url: globalParams.urlOldVersion + 'adm/CategoriesList.aspx?token='
								},*/
								leaf: true
							}, { 	
								text: 'Origenes de los datos',
								routeId: 'frame',
								data: {
									title: 'dataOrigins',
									url: globalParams.urlOldVersion + 'adm/OrigenesDeDatos.aspx?token='
								},
								leaf: true
							}, { 	
								text: 'Configuraciones',
								click: false,
								children: [
									{ 	text: 'Password',
										routeId: 'frame',
										data: {
											title: 'password',
											url: globalParams.urlOldVersion + 'adm/Password.aspx?token='
										},
										leaf: true
									}
								]
							}
						]
					}
				]
			}
		}
	}
})