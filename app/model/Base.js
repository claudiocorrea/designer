Ext.define('Designer.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'Designer.model'
    }
});
