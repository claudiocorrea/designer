Ext.define('Designer.model.Operator', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdOperator', type: 'string' },
        {  name: 'Description', type: 'string' }
    ]

})