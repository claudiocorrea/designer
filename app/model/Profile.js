Ext.define('Designer.model.Profile', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdProfile', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'Code', type: 'int' }
    ]

})