Ext.define('Designer.model.Parallel', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdItem', type: 'int' },
        {  name: 'UnionTipoY', type: 'int' },
        {  name: 'StepID1', type: 'int' },
        {  name: 'StepID2', type: 'int' }
    ]

})