Ext.define('Designer.model.Group', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdGroup', type: 'string' },
        {  name: 'Name', type: 'string' },
		{  name: 'Enabled', type: 'boolean' },
        {  name: 'Mandatory', type: 'boolean' }
    ]

})