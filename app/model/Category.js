Ext.define('Designer.model.Category', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'CategoryID', type: 'string' },
        {  name: 'Title', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'IdEmpresa', type: 'string' },
        {  name: 'empresa', type: 'string' }
    ]

})