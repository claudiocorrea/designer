Ext.define('Designer.model.FormType', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdFormType', type: 'string' },
        {  name: 'Description', type: 'string' },
		{  name: 'Cp', type: 'string' }
    ]

})