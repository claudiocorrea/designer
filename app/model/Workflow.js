Ext.define('Designer.model.Workflow', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'WorkflowID', type: 'string' },
        {  name: 'Title', type: 'string' },
        {  name: 'Description', type: 'string' },
        {  name: 'CategoryID', type: 'string' },
        {  name: 'Category', type: 'string' },
        {  name: 'IdEmpresa', type: 'string' },
        {  name: 'Empresa', type: 'string' },
        {  name: 'StartDate', type: 'string' },
        {  name: 'EndDate', type: 'string' },
        {  name: 'IdState', type: 'string' },
        {  name: 'RelativeDates', type: 'boolean' },
        {  name: 'Original', type: 'boolean' },
        {  name: 'StateTxt', type: 'string' },
        {  name: 'CanBeNode', type: 'boolean' },
        {  name: 'GeneraAlertas', type: 'boolean' },
        {  name: 'UsaCondicionalesPasos', type: 'boolean' },
        {  name: 'UsaCondicionalesUsuarios', type: 'boolean' }
    ]

})