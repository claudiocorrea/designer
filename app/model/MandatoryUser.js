Ext.define('Designer.model.MandatoryUser', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdUser', type: 'string', convert: function(value, record) { 
			return record.get('IdUser') ? record.get('IdUser')[0] : value }},
        {  name: 'fullname', type: 'string' },
		{  name: 'FirstName', type: 'string' },
		{  name: 'LastName', type: 'string' },
        {  name: 'Enabled', type: 'int' },
        {  name: 'Email', type: 'string' },
        {  name: 'Mandatory', type: 'boolean' }
    ]

})