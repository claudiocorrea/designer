Ext.define('Designer.model.Enterprise', {
    extend: 'Designer.model.Base',

    fields: [
        {  name: 'IdEmpresa', type: 'string' },
        {  name: 'Descripcion', type: 'string' },
        {  name: 'Enabled', type: 'boolean' },
        {  name: 'Codigo', type: 'string' }
    ]

})