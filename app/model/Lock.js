Ext.define('Designer.model.Lock', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.lock',

    data: {
        txtFields: {},
		username: '',
		password: ''
    },

	formulas: {
        labels: function (key) {
            var me = this;
            //console.log("key: ", key);
            //console.log("This VM: ", this);
            console.log("Cargando el locale: " + Ext.String.format('./locale/locale-{0}.js', appParams.lang));
            Designer.util.Utils.loadScript(Ext.String.format('./locale/locale-{0}.js', appParams.lang), function(err, callback){
                if (!err) { me.set({ txtFields: txtFields })}
            });
        }
    }

});
