Ext.define('Designer.util.Utils', {
    singleton : true,

    getQueryVariable: function(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");

        for (var i=0; i<vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {return pair[1];}
        }

        return null;
    },

    loadScript: function(path, callback) {
        Ext.Loader.loadScript({
            scope: this,
            url: Ext.util.Format.format(path),
            onLoad: function() {
                callback(false, null)
            },
            onError: function(o, e) {
                console.log(o)
                console.log(e)
                callback(true, '')
            }
        })
    }
});

//En IE 11 NO existe la funcion trim(), asi que se agrega al prototype
if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g,"");
    }
}

//Agrega la funcion ltrim(), si no esta definida
if (!String.prototype.ltrim) {
    String.prototype.ltrim = function () {
        return this.replace(/^\s+/g,"");
    }
}

//Agrega la funcion rtrim(), si no esta definida
if (!String.prototype.rtrim) {
    String.prototype.rtrim = function () {
        return this.replace(/\s+$/g,"");
    }
}

//Agrega la funcion startsWith(), si no esta definida
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}