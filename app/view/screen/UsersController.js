Ext.define('Designer.view.screen.UsersController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.users',
	
	config: {
		data: null
	},
	
	initScreen: function() {
		this.getUsers(function(error, callback) { console.log(callback) })			
	},
	
	refreshScreen: function() {
		this.initScreen()
	},
	
	getUsers: function(callback) {
		var store = this.lookupReference('usersGrid').getStore()
        store.getUsers(function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else {
				store.getSorters().add({ property: 'Login', direction: 'ASC' })
				callback(false, result.response)
			}
        })
	},
	
	findUser: function(o, record) {
        var store = this.lookupReference('usersGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'Login', value: record})
    },
	
	newUser: function() {
		Ext.getCmp('main').controller.openPopup('user', {})
	},
	
	editUser: function(grid, rowIndex) {
		Ext.getCmp('main').controller.openPopup('user', grid.getStore().getAt(rowIndex).data)
	},
	
	deleteUser: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveUser, data.Login), function(btn) {
            if (btn == 'yes') {
				var store = grid.getStore()
				grid.mask('Espere...');
				store.delete(data, function(error, result) {
					grid.unmask();
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else controller.refreshScreen()
				})
			}			
		})	
	}
	
})