Ext.define('Designer.view.screen.FrameController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.frame',
	
	config: {
		data: null
	},
	
	init: function() {
		this.data = this.getView().config.data
		switch (this.data.title) {
			case 'forms' : this.data.title = txtFields.words.form + 's'; break
			case 'eventos' : this.data.title = txtFields.words.event + 's'; break
			case 'companies' : this.data.title = txtFields.words.enterprise + 's'; break			
			case 'groups' : this.data.title = txtFields.words.group + 's'; break
			case 'users' : this.data.title = txtFields.words.user + 's'; break
			case 'companyUsers' : this.data.title = txtFields.titles.companyUsers; break
			case 'categories' : this.data.title = txtFields.titles.categories; break
			case 'dataOrigins' : this.data.title = txtFields.titles.dataOrigins; break
			case 'password' : this.data.title = txtFields.words.password + 's'; break
			default: this.data.title = this.data.title;	break
		}
		if(this.data.back) this.lookupReference('return').setHidden(false)
		this.lookupReference('titleSection').setText(this.data.title)
		console.log(this.data)	
	},
	
	initScreen: function() { 
		this.getView().removeAll()
		this.getView().add({
			xtype : "component",
			border: false,
			minHeight: 500,
			autoEl : {
				tag : "iframe",
				src : this.data.url + loginUser.token
			}
		})
	},
	
	returnTab: function() {
        Ext.getCmp('main').controller.setCurrentView(this.data.back)
    },
	
})