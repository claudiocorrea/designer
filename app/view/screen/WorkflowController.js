Ext.define('Designer.view.screen.WorkflowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.workflow',
	
	initScreen: function() {
		var controller = this
		this.getWorkflows(function(error, callback) {
			if(!error) {
				var store = controller.lookupReference('instancesGrid').getStore()
				callback.forEach(function(rec) { store.add(rec) })
				store.getFilters().add({id: 'fltOriginals', property: 'Original', value: 'false'})
			}
		})
	},
	
	refreshScreen: function() {
		this.initScreen()
	},
	
	getWorkflows: function(callback) {
		var store = this.lookupReference('originalsGrid').getStore()
        store.getWorkflows(function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else callback(false, result.response)
        })
		store.getFilters().add({id: 'fltOriginals', property: 'Original', value: 'true'})
	},
	
	findWorkflow: function(o, record) {
        var store = this.lookupReference('originalsGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'Title', value: record})
		store = this.lookupReference('instancesGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'Title', value: record})
    },
	
	newWorkflow: function() {
        Ext.getCmp('main').controller.openPopup('workflow', {})
    },
	
	viewWorkflow: function(grid, rowIndex) {
		console.log(grid.getStore().getAt(rowIndex).data)
		Ext.getCmp('main').controller.openPopup('workflow', grid.getStore().getAt(rowIndex).data)
    },
	
	deleteWorkflow: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveWorkflow, data.Title), function(btn) {
            if (btn == 'yes') {
				var store = grid.getStore()
				grid.mask('Espere...');
				store.delete(data, function(error, result) {
					grid.unmask();
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					}
				})
			}			
		})	
	},
	
	openModel: function(grid, rowIndex) {
		Ext.getCmp('main').controller.setCurrentView('designer', grid.getStore().getAt(rowIndex).data)
	},
	
	openModelTesting: function() {
		Ext.getCmp('main').controller.setCurrentView('designer', {})
	},
	
	cloneModel: function(grid, rowIndex) {
		//Ext.getCmp('main').controller.openClone(grid.getStore().getAt(rowIndex).data)
		var controller = this
		var data = grid.getStore().getAt(rowIndex).data
		Ext.Msg.prompt(txtFields.words.clone, txtFields.messages.writeNameWorkflow, function(btn, text) {
			if(text != '') {
				Ext.Ajax.request({
					url: globalParams.urlServices + 'workflow/cloneWorkflow?workflowId=' + data.WorkflowID + '&name=' + text,
					method: 'POST',
					success: function (response) {
						console.log('OK...', response)
						var result = JSON.parse(response.responseText)[0]
						console.log(result, result.Result.indexOf('OK'))
						if(result.Result.split('||')[0] != 'OK') {
							Ext.Msg.alert('ERROR', result.Result.split('||')[1])
						} else { 
							Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
							controller.refreshScreen()
						}
					},
					failure: function (error) {
						console.log('FAIL...', error)
						error = JSON.parse(error.responseText)[0]
						Ext.Msg.alert('ERROR', error.message)
					}
				})
			}
		}, this);
	}
		
})