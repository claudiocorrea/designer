Ext.cmd.derive('Designer.view.screen.FormDesignerController - copia',
Ext.app.ViewController, {
	config: {
		data:null,
		form:null,
		contSections:null,
		contFields:null,
		sectionSelected:null,
		objSelected:null,
		fieldsToDelete:null
	},
	init:function() {
			this.data=this.getView().config.data;
			this.form=this.lookupReference('propertiesForm').getForm();
			if(this.lookupReference('previewPanel')) {
				this.lookupReference('previewPanel').title=	'Preview: '+this.data.FormName
	};
	console.log(this.data)
	},
	initScreen:function(){
		this.contSections=0;
		this.contFields=0;
		this.sectionSelected=null;
		this.objSelected=null;
		this.fieldsToDelete='';
		this.panel=this.lookupReference('formPanel');
		if(this.data.IdForm){
			this.getFormFields()
		} else { this.addSection() }
	},
	returnTab:function(){
		Ext.getCmp('main').controller.setCurrentView('forms')
	},
	refreshScreen:function(){
		this.panel.removeAll();
		this.setVisibles('');
		this.initScreen()
	},
	getFormFields:function(){
		var a=this;
		Ext.Ajax.request({
			url:globalParams.urlServices+'form/getFormFields?idForm='+this.data.IdForm+'&userId='+loginUser.idUser,
			method:'GET',
			success:function(b) {
				console.log('SUCCESS...', b);
				a.response=b.responseText;
				b=JSON.parse(b.responseText);
		console.log(b);
		if(b.secciones.length>0){
		a.contSections=b.secciones.length;
		a.panel.add(b.secciones);
		a.sectionSelected=a.panel.items.items[0]}else {
		a.addSection()}},
	failure:function(a){
		console.log('FAIL...',
a);
		a=JSON.parse(a.responseText);
		Ext.Msg.alert('ERROR',
a.message)}})
	},
	
	setXMLField:function(f, d, b, a){
		var e=b==-1?'DELETE':a.idField?'MODIFY':'ADD';
		var c='\t<campo>\n';
		c+='\t\t<FieldName><![CDATA['+a.name+']]></FieldName>\n\t\t<TableName><![CDATA['+(a.tableDB?a.tableDB:'')+']]></TableName>\n\t\t<IdFieldType><![CDATA['+f+']]></IdFieldType>\n\t\t<Mandatory><![CDATA['+(a.mandatory?1:0)+']]></Mandatory>\n\t\t<OnlyNumbers><![CDATA['+(a.xtype=='numberfield'?1:0)+']]></OnlyNumbers>\n\t\t<Class><![CDATA[]]></Class>\n\t\t<Style><![CDATA['+(a.style?a.style:'')+']]></Style>\n\t\t<ComboField><![CDATA['+(a.fieldValue?a.fieldValue:'')+']]></ComboField>\n\t\t<OnlyDates><![CDATA['+(a.xtype=='datefield'?1:0)+']]></OnlyDates>\n\t\t<FrontendText><![CDATA['+(a.xtype=='panel'?a.title:a.xtype=='checkbox'?a.boxLabel:a.fieldLabel)+']]></FrontendText>\n\t\t<Section><![CDATA['+d+']]></Section>\n\t\t<Posicion><![CDATA['+b+']]></Posicion>\n\t\t<Procedimiento><![CDATA['+(a.spDB?a.spDB:'')+']]></Procedimiento>\n\t\t<Parametros><![CDATA['+(a.input?a.input:'')+']]></Parametros>\n\t\t<ComboFieldText><![CDATA['+(a.comboText?a.comboText:'')+']]></ComboFieldText>\n\t\t<PermiteNull><![CDATA['+(a.allowEmpty?1:0)+']]></PermiteNull>\n\t\t<SoloLectura><![CDATA['+(a.readonly?1:0)+']]></SoloLectura>\n\t\t<EnReferencia><![CDATA['+(a.createReference?1:0)+']]></EnReferencia>\n\t\t<ListaTitulo><![CDATA[]]></ListaTitulo>\n\t\t<ListaCampo1><![CDATA[]]></ListaCampo1>\n\t\t<ListaCampo2><![CDATA[]]></ListaCampo2>\n\t\t<IgnoraSiNoHayDatos><![CDATA['+(a.ignoreIfEmpty?1:0)+']]></IgnoraSiNoHayDatos>\n\t\t<CampoHtml><![CDATA['+(a.htmlfield?a.htmlfield:'')+']]></CampoHtml>\n\t\t<OpcionNuevo><![CDATA['+(a.opcionNew?1:0)+']]></OpcionNuevo>\n\t\t<OcultaAprobarSiEsFalso><![CDATA['+(a.hiddenApprove?1:0)+']]></OcultaAprobarSiEsFalso>\n\t\t<IdOrigen><![CDATA['+(a.originDB?a.originDB:0)+']]></IdOrigen>\n\t\t<FechaMayorOIgualaHoy><![CDATA['+(a.dategreater?1:0)+']]></FechaMayorOIgualaHoy>\n\t\t<MenorQueCampo><![CDATA[0]]></MenorQueCampo>\n\t\t<IdField><![CDATA['+(a.idField?a.idField:0)+']]></IdField>\n\t\t<IdFormField><![CDATA['+(a.IdForm?a.IdForm:0)+']]></IdFormField>\n\t\t<Action><![CDATA['+e+']]></Action>\n\t\t<ActionResult><![CDATA[]]></ActionResult>\n\t\t<ActionError><![CDATA[]]></ActionError>\n';
		return c+='\t</campo>\n'},
		
	getIDType:function(b){
		var a=-1;
		switch(b){
		case 'panel':a=9;
		break;
		case 'fieldcontainer':a=8;
		break;
		case 'textfield':case 'datefield':case 'numberfield':a=1;
		break;
		case 'htmleditor':a=4;
		break;
		case 'radiogroup':a=5;
		break;
		case 'checkbox':a=3;
		break;
		case 'combobox':a=2;
		break;
		case 'tagfield':a=7;
		break;
		}return a
	},
	
	getXMLFields:function(){
		var b='<root>\n',
a=this;
		this.panel.items.items.forEach(function(c){
		var d=0;
		c.items.items.forEach(function(e){
		b+=a.setXMLField(a.getIDType(e.xtype), c.title, ++d, e);
		if(e.xtype=='panel'){
		var f=0;
		e.items.items.forEach(function(d){
		if(d.xtype!='button'){
		b+=a.setXMLField(a.getIDType(d.xtype),
e.name,
++f,
d)}})}})});
		b+=this.fieldsToDelete+'</root>';
		return b
	},
	
	saveFields:function(){
		console.log('saveFields: ',
this.data);
		Ext.Msg.show({
		msg:txtFields.messages.savingForm,
progressText:'Saving...',
width:300,
wait:{
		interval:200}});
		var c=this.getXMLFields(),
b=this;
		var d=new Designer.store.FormFields();
		var a={
		idForm:this.data.IdForm,
xmlData:c};
		console.log(a);
		d.saveFields(a,
function(c,
a){
		if(c){
		if(a.error==401){
		Ext.Msg.alert('ERROR',
txtFields.messages.errors.loginError)}else {
		if(a.error!=0){
		Ext.Msg.alert('ERROR',
a.message)}}}else {
		console.log(a);
		c= false;
		for(i=0;
		i<a.response.length;
		i++){
		if(a.response[i].ActionResult!='OK'){
		Ext.Msg.alert('ERROR',
a.response[i].ActionError);
		c=!0;
		break}}b.refreshScreen();
		if(!c){
		Ext.Msg.alert(txtFields.words.confirm,
txtFields.messages.alerts.saveSuccessfull)}}})
	},
	
	setVisibles:function(a){
		if(this.lookupReference('iconDelete')){
		this.lookupReference('iconDelete').setHidden( false)}this.lookupReference('tfName').setHidden( false);
		this.lookupReference('tfLabel').setHidden( false);
		this.lookupReference('nfPosition').setHidden(!0);
		this.lookupReference('fcReadOnly').setHidden( false);
		this.lookupReference('ckReference').setHidden(!0);
		this.lookupReference('ckOptionNew').setHidden(!0);
		this.lookupReference('ckGreater').setHidden(!0);
		this.lookupReference('tfStyle').setHidden( false);
		this.lookupReference('tfHtmlField').setHidden(!0);
		this.lookupReference('sqlPanel').setHidden( false);
		this.lookupReference('tfComboText').setHidden( false);
		this.lookupReference('fcExtras').setHidden(!0);
		this.lookupReference('cbForm').setHidden(!0);
		switch(a){
		case 'panel':if(this.panel.items.items.length==1){
		if(this.lookupReference('iconDelete')){
		this.lookupReference('iconDelete').setHidden(!0)}};
		this.lookupReference('fcReadOnly').setHidden(!0);
		this.lookupReference('tfStyle').setHidden(!0);
		this.lookupReference('sqlPanel').setHidden(!0);
		break;
		case 'fieldContainer':this.lookupReference('tfLabel').setHidden(!0);
		this.lookupReference('fcReadOnly').setHidden(!0);
		this.lookupReference('tfStyle').setHidden(!0);
		this.lookupReference('sqlPanel').setHidden(!0);
		break;
		case 'textfield':case 'datefield':case 'numberfield':case 'htmleditor':case 'radiogroup':this.lookupReference('ckReference').setHidden( false);
		this.lookupReference('tfComboText').setHidden(!0);
		if(a=='datefield'){
		this.lookupReference('ckGreater').setHidden( false)};
		break;
		case 'combobox':case 'tagfield':this.lookupReference('ckOptionNew').setHidden( false);
		break;
		case 'checkbox':this.lookupReference('fcExtras').setHidden( false);
		this.lookupReference('tfHtmlField').setHidden( false);
		break;
		case 'form':this.lookupReference('cbForm').setHidden( false);
		this.lookupReference('tfStyle').setHidden(!0);
		this.lookupReference('sqlPanel').setHidden(!0);
		break;
		default:if(this.lookupReference('iconDelete')){
		this.lookupReference('iconDelete').setHidden(!0)};
		this.lookupReference('tfName').setHidden(!0);
		this.lookupReference('tfLabel').setHidden(!0);
		this.lookupReference('fcReadOnly').setHidden(!0);
		this.lookupReference('tfStyle').setHidden(!0);
		this.lookupReference('sqlPanel').setHidden(!0);
		}},
	fieldSelection:function(a){
		console.log('fieldSelection',
a);
		if(a){
		this.inSelection(a)}var c=a.xtype;
		if(a.xtype=='header'){
		c='panel';
		a=a.container.component}else {
		if(a.xtype=='panel'){
		c='fieldContainer'}else {
		if(a.xtype=='fieldcontainer'){
		c='form'}}}a.realType=c;
		this.sectionSelected=a.container.component;
		this.objSelected=a;
		console.log(a);
		var b={
		xtype:c,
name:a.name,
label:a.fieldLabel,
posicion:a.posicion,
mandatory:a.mandatory,
readonly:a.readonly,
style:a.style,
originDB:a.originDB,
tableDB:a.tableDB,
spDB:a.spDB,
input:a.input,
fieldValue:a.fieldValue,
comboText:a.comboText};
		switch(c){
		case 'panel':b.label=a.title;
		this.sectionSelected=a;
		break;
		case 'fieldContainer':this.sectionSelected=a;
		break;
		case 'textfield':case 'htmleditor':case 'numberfield':case 'radiogroup':b.createReference=a.createReference;
		break;
		case 'datefield':b.createReference=a.createReference;
		b.dategreater=a.dategreater;
		break;
		case 'checkbox':b.label=a.boxLabel;
		b.hiddenApprove=a.hiddenApprove;
		b.ignoreIfEmpty=a.ignoreIfEmpty;
		break;
		case 'combobox':case 'tagfield':b.optionNew=a.optionNew;
		break;
		case 'form':b.IdForm=a.IdForm;
		break;
		}this.setProperties(b);
		this.setVisibles(c)
	},
	
	inSelection:function(a){
		if(this.objSelected){
		this.outSelection(this.objSelected)}a.setStyle('backgroundColor',
'lightgrey');
		if(a.xtype!='panel'){
		a.setStyle('border',
'4px solid lightgrey')}
	},
	
	outSelection:function(a){
		console.log('outSelection: ',
a);
		if(a.xtype!='panel'){
		a.setStyle('backgroundColor',
'white');
		a.setStyle('border',
'none')}else {
		if(a.realType!='fieldContainer'){
		a.getHeader().setStyle('backgroundColor',
'#1E90FF')}}console.log('out Selection...')
	},
	
	getPosition:function(c){
		console.log('getPosition: ',
c);
		var a=-1,
b=this.objSelected.xtype!='panel'?this.sectionSelected.items.items:this.objSelected.container.component.items.items;
		for(i=0;
		i<b.length;
		i++){
		if(b[i].name==c.name){
		a=i;
		break}}return a
	},
	
	upField:function(){
		var a=this.getPosition(this.objSelected);
		console.log('upField: ',
this.objSelected,
' posActual: ',
a);
		if(a-1>-1){
		var b=this.objSelected.xtype!='panel'?this.sectionSelected:this.objSelected.container.component;
		b.moveAfter(b.getComponent(a-1),
b.getComponent(a));
		this.objSelected.posicion=a}
	},
	
	downField:function(){
		var a=this.getPosition(this.objSelected);
		console.log('downField: ',
this.objSelected,
' posActual: ',
a);
		var b=this.objSelected.xtype!='panel'?this.sectionSelected:this.objSelected.container.component;
		if(a+1<b.items.items.length){
		b.moveAfter(b.getComponent(a),
b.getComponent(a+1));
		this.objSelected.posicion=a}
	},
	
	deleteField:function(){
		var a=this;
		Ext.Msg.confirm(txtFields.words.confirm,
Ext.String.format(txtFields.messages.confirmRemoveField,
this.form.getValues().name),
function(b){
		if(b=='yes'){
		console.log('deleteField: ',
a.objSelected);
		if(a.objSelected.xtype=='panel'){
		a.sectionSelected=a.objSelected.container.component;
		a.objSelected.items.items.forEach(function(c){
		if(c.idField){
		a.fieldsToDelete+=a.setXMLField('',
'',
-1,
c)}})}if(a.objSelected.idField){
		a.fieldsToDelete+=a.setXMLField('',
'',
-1,
a.objSelected)}a.sectionSelected.remove(a.objSelected);
		a.objSelected=null;
		a.fieldSelection(a.sectionSelected)}})
	},
	
	setProperties:function(a){
		console.log(a);
		this.setVisibles(a.xtype);
		this.form.setValues(a);
		if(a.xtype=='form'&&a.IdForm){
		if(!this.lookupReference('cbForm').getStore().isLoaded()){
		this.lookupReference('cbForm').getStore().load()}}else {
		if(a.xtype=='combobox'&&a.originDB){
		if(!this.lookupReference('cbOriginDB').getStore().isLoaded()){
		this.lookupReference('cbOriginDB').getStore().load()}}}
	},
	
	changeName:function(b,
a){
		if(b.hasFocus&&this.objSelected){
		this.objSelected.name=a}},
	changeLabel:function(b,
a){
		console.log('changeLabel: ',
this.objSelected);
		if(b.hasFocus&&this.objSelected){
		if(this.objSelected.xtype=='panel'){
		this.objSelected.setBind({
		title:a})}else {
		if(this.objSelected.xtype=='checkbox'){
		this.objSelected.setBind({
		boxLabel:a})}else {
		this.objSelected.setBind({
		fieldLabel:a})}}}},
	
	changeProperties:function(a, b) {
		if(a.hasFocus&&this.objSelected){
		console.log('changeProperties: ', a.name + ' -> ' + b);
		this.objSelected[a.name] = b}
	},
		
	addSection:function(){
		var b='section-'+ ++this.contSections;
		var a=Ext.create({
			xtype:'panel',
			reference:b, name:b,
			bodyPadding:10,
			collapsible:!0,
			defaults:{ width:'100%'},
			items:[],
			viewModel:{ data:{ title:'Nueva seccion'}},
			bind:{ title:'{title}'},
			header:{ listeners:{ click:'fieldSelection'}},
			listeners:{ collapse:'sectionSelect', expand:'sectionSelect'}
		});
		this.panel.add(a);
		this.sectionSelected=a
	},
	
	addFieldContainer:function(){
		var b='fc-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'panel',
			name:b,
			posicion:this.contFields,
			layout:'hbox',
			defaults:{
					flex:1,
			margin:'0 20 10 0'},
				items:[{
					xtype:'button',
			userCls:'circular',
			iconCls:'x-fa fa-plus',
			flex:0,
			handler:'selectedColumns'}],
			listeners:{
					focusleave:'outSelectionColumns',
			add:'addItemToFieldContainer'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	selectedColumns:function(b){
		var a=b.container.component;
		a.setBodyStyle('backgroundColor', 'lightgrey');
		this.fieldSelection(a)
	},
	
	outSelectionColumns:function(a){
		a.setBodyStyle('backgroundColor', 'white')
	},
	
	addItemToFieldContainer:function(b, a){
		if(a.xtype!='button'&&a.xtype!='textfield'&&a.xtype!='datefield'&&a.xtype!='numberfield'&&a.xtype!='combobox'&&a.xtype!='checkbox'){
		b.remove(a)}
	},
	
	addLabel:function(){
		var b='label-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
		xtype:'label',
name:b,
posicion:this.contFields,
bind:{
		text:'Nuevo texto'},
	margin:'0 0 50 0',
listeners:{
		focus:'fieldSelection',
focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addTextfield:function(){
		var b='field-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'textfield',
			name:b,
			editable: false,
			posicion:this.contFields,
			bind:{
					fieldLabel:'Nuevo Campo'},
				mandatory: false,
			readonly: false,
			createReference: false,
			listeners:{
					focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addHTMLEditor:function(){
		var b='htmleditor-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'htmleditor',
			name:b,
			enableSourceEdit: false,
			posicion:this.contFields,
			labelAlign:'top',
			labelClsExtra:'labelField',
			bind:{
					fieldLabel:'Nuevo Campo'},
				mandatory: false,
			readonly: false,
			listeners:{
					focusenter:'fieldSelection',
			sync:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addDatefield:function(){
		var b='datefield-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'datefield',
			name:b,
			editable: false,
			posicion:this.contFields,
			bind:{
			fieldLabel:'Nuevo Campo'},
			mandatory: false,
			readonly: false,
			createReference: false,
			dategreater: false,
			listeners:{
			focusenter:'fieldSelection'}});
		if(this.sectionSelected){ this.sectionSelected.add(a)}
	},
	
	addNumberfield:function(){
		var b='numberfield-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'numberfield',
			name:b,
			editable: false,
			posicion:this.contFields,
			bind:{
					fieldLabel:'Nuevo Campo'},
				mandatory: false,
			readonly: false,
			createReference: false,
			listeners:{
					focusenter:'fieldSelection'}});
		if(this.sectionSelected){ this.sectionSelected.add(a)}
	},
	
	addCombobox:function(){
		var b='combo-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
		xtype:'combobox',
name:b,
editable: false,
posicion:this.contFields,
bind:{
		fieldLabel:'Nuevo combo'},
	displayField:'value',
valueField:'id',
publishes:'value',
mandatory: false,
readonly: false,
optionNew: false,
listeners:{
		focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addCheckbox:function(){
		var b='checkbox-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
		xtype:'checkbox',
name:b,
posicion:this.contFields,
bind:{
		boxLabel:'Nuevo Checkbox'},
	mandatory: false,
readonly: false,
listeners:{
		focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addRadiobutton: function() {
		var b='radiobutton-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
			xtype:'radiogroup',
			name:b,
			posicion:this.contFields,
			items:[{
				name:'opcion',
				bind:{boxLabel:'{txtFields.words.yes}'},
				inputValue:'si',
				labelClsExtra:'labelField'
			}, {
				name:'opcion',
				boxLabel:'No',
				inputValue:'no',
				padding:'0 0 0 15',
				labelClsExtra:'labelField'
			}],
			bind:{fieldLabel:'Nuevo Radio'},
			mandatory: false,
			readonly: false,
			listeners:{focusenter:'fieldSelection'}
		});
		if(this.sectionSelected){this.sectionSelected.add(a)}
	},
	
	addList:function(){
		var b='tagfield-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
		xtype:'tagfield',
name:b,
posicion:this.contFields,
bind:{
		fieldLabel:'Nuevo Campo'},
	mandatory: false,
readonly: false,
listeners:{
		focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	},
	
	addForm:function(){
		var b='form-'+this.contSections+ ++this.contFields;
		var a=Ext.create({
		xtype:'fieldcontainer',
name:b,
bind:{
		fieldLabel:'Nuevo Campo'},
	labelClsExtra:'labelField',
posicion:this.contFields,
IdForm:'',
items:[{
		xtype:'button',
userCls:'circular',
iconCls:'x-fa fa-file-text-o'}],
mandatory: false,
readonly: false,
listeners:{
		focusenter:'fieldSelection'}});
		if(this.sectionSelected){
		this.sectionSelected.add(a)}
	}
},
	0,
0,
0,
0,
['controller.formDesigner'],
0,
[Designer.view.screen,
'FormDesignerController'],
0);
		