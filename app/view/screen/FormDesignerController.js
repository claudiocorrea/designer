Ext.define('Designer.view.screen.FormDesignerController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.formDesigner',
	
	config: {
		data: null,
		form: null,

		kpi: null,
		kpiName: null,

		contSections: null,
		contFields: null,
		
		fieldsToDelete: null,
		formFields: null,
		
		sectionSelected: null,
		objSelected: null
	},
	
	init: function() {
		this.kpi = false
		this.kpiName = 'cpbakpi'
		this.data = this.getView().config.data;
		this.form = this.lookupReference('propertiesForm').getForm();
		if(this.lookupReference('previewPanel')) {
			this.lookupReference('previewPanel').title = 'Preview: ' + this.data.FormName
		};
		console.log(this.data)
	},
	
	initScreen: function() {
		this.contSections = 0;
		this.contFields = 0;
		this.sectionSelected = null;
		this.objSelected = null;
		this.fieldsToDelete = ''
		this.panel = this.lookupReference('formPanel');
		if(this.data.IdForm){
			this.getFormFields()
		} else { this.addSection() }
	},
	
	returnTab: function() {
		Ext.getCmp('main').controller.setCurrentView('forms')
	},
	
	refreshScreen:function(){
		this.panel.removeAll();
		this.setVisibles('');
		this.initScreen()
	},
	
	viewForm: function() {
		console.log(JSON.stringify(this.formFields))
	},
	
	getFormFields: function() {
		this.formFields = new Array()
		var controller = this
		
		Ext.Ajax.request({
			url: globalParams.urlServices + 'form/getFormFields?idForm=' + this.data.IdForm + '&userId=' + loginUser.idUser,
			method: 'GET',
			success: function(response) {
				//console.log('SUCCESS...', response);
				response = JSON.parse(response.responseText);
				console.log(response);
				if(response.secciones.length > 0) {
					controller.contSections = response.secciones.length
					controller.panel.add(response.secciones);
					controller.sectionSelected = controller.panel.items.items[0]
					response.secciones.forEach(function(section) {
						if(section.items.length > 0) {
							section.items.forEach(function(item) {
								if(item.name == controller.kpiName) {
									controller.kpi = true									
									console.log('Se encontro KPI!', item)
								}									
								controller.formFields.push(item)
							})
						}
					})
					if(!controller.kpi) controller.addKPIField()

				} else { controller.addSection() }
			},
			failure: function(error) {
				console.log('FAIL...', error);
				error = JSON.parse(error.responseText);
				Ext.Msg.alert('ERROR', error.message)
			}
		})
	},
	
	deleteField: function() {
		var controller = this;
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveField, this.form.getValues().name), function(btn) {
			if(btn == 'yes') {
				console.log('deleteField: ', controller.objSelected);
				if(controller.objSelected.xtype == 'panel') {
					controller.sectionSelected = controller.objSelected.container.component;
					controller.objSelected.items.items.forEach(function(item) {
						if(item.xtype == 'panel') {
							item.items.items.forEach(function(xitem) {
								if(xitem.idField) { controller.fieldsToDelete += controller.setXMLField('', '', -1, xitem) }
							})
						} else if(item.idField) { controller.fieldsToDelete += controller.setXMLField('', '', -1, item) }
					}
				)}
				if(controller.objSelected.idField) { controller.fieldsToDelete += controller.setXMLField('', '', -1, controller.objSelected) }
				controller.sectionSelected.remove(controller.objSelected);
				controller.objSelected = null;
				controller.fieldSelection(controller.sectionSelected)
			}
			console.log('deleteField: ', controller.fieldsToDelete)
		})
	},
	
	saveFields: function() {
		console.log('saveFields: ', this.data);
		Ext.Msg.show({ msg:txtFields.messages.savingForm, progressText:'Saving...', width:300, wait:{ interval:200}});
		
		var xml = this.getXMLFields(), controller = this, store = new Designer.store.FormFields();
		var data = { idForm: this.data.IdForm, xmlData: xml };
		console.log(data);
		store.saveFields(data, function(xml, result) {
			if(xml) {
				if(result.error == 401) { Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError) }
				else { if(result.error != 0) { Ext.Msg.alert('ERROR', result.message) } }
			} else {
				console.log(result);
				xml = false;
				for(i=0; i<result.response.length; i++) {
					if(result.response[i].ActionResult != 'OK') {
						Ext.Msg.alert('ERROR', result.response[i].ActionError);
						xml= true; break
					}
				}
				controller.refreshScreen();
				if(!xml) { Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull) } 
			}
		})
	},
	
	fieldSelection:function(field){
		console.log('fieldSelection', field);
		if(field){ this.inSelection(field) }
		var xtype = field.xtype;
		if(field.xtype == 'header'){
			xtype = 'panel';
			field = field.container.component
		} else {
			if(field.xtype == 'panel'){ xtype ='fieldContainer'}
			else { if(field.xtype == 'fieldcontainer') { xtype ='form' } } 
		}
		field.realType = xtype;
		this.sectionSelected = field.container.component;
		this.objSelected = field;
		
		var data = {
			xtype: xtype,
			name: field.name,
			label: field.fieldLabel,
			posicion: field.posicion,
			mandatory: field.mandatory,
			readonly: field.readonly,
			style: field.style,
			originDB: field.originDB,
			tableDB: field.tableDB,
			spDB: field.spDB,
			input: field.input,
			fieldValue: field.fieldValue,
			comboText: field.comboText
		};
		switch(xtype) {
			case 'panel':
				data.label = field.title;
				this.sectionSelected = field;
				break;
			case 'fieldContainer': this.sectionSelected = field; break;
			
			case 'textfield':
			case 'htmleditor':
			case 'numberfield':
			case 'radiogroup': data.createReference = field.createReference; break;
			
			case 'datefield':
				data.createReference = field.createReference;
				data.dategreater = field.dategreater;
				break;
			case 'checkbox':
				data.label = field.boxLabel;
				data.hiddenApprove = field.hiddenApprove;
				data.ignoreIfEmpty = field.ignoreIfEmpty;
				break;
			case 'combobox':
			case 'tagfield': data.optionNew = field.optionNew; break;
			case 'form': data.IdForm = field.IdForm; break;
		}
		this.setProperties(data);
		this.setVisibles(xtype)
	},
	
	inSelection:function(field){
		if(this.objSelected){ this.outSelection(this.objSelected) }
		field.setStyle('backgroundColor', 'lightgrey');
		if(field.xtype != 'panel'){ field.setStyle('border', '4px solid lightgrey') }
	},
	
	outSelection:function(field){
		//console.log('outSelection: ', field);
		if(field.xtype != 'panel'){ 
			field.setStyle('backgroundColor', 'white');
			field.setStyle('border', 'none')
		}else {
			if(field.realType != 'fieldContainer') { field.getHeader().setStyle('backgroundColor', '#1E90FF') }
			else field.setStyle('backgroundColor', 'white')
		}
	},
	
	setProperties:function(field){
		//console.log(field);
		this.setVisibles(field.xtype);
		this.form.setValues(field);
		if(field.xtype == 'form' && field.IdForm) {
			if(!this.lookupReference('cbForm').getStore().isLoaded()) {
				this.lookupReference('cbForm').getStore().load()
			}
		}else { 
			if(field.xtype == 'combobox' && field.originDB) {
				if(!this.lookupReference('cbOriginDB').getStore().isLoaded()) {
					this.lookupReference('cbOriginDB').getStore().load()
				}
			}
		}
	},
	
	setVisibles:function(xtype){
		if(this.lookupReference('iconDelete')) { this.lookupReference('iconDelete').setHidden( false) }
		this.lookupReference('tfName').setHidden( false);
		this.lookupReference('tfLabel').setHidden( false);
		this.lookupReference('nfPosition').setHidden(true);
		this.lookupReference('fcReadOnly').setHidden( false);
		this.lookupReference('ckReference').setHidden(true);
		this.lookupReference('ckOptionNew').setHidden(true);
		this.lookupReference('ckGreater').setHidden(true);
		this.lookupReference('tfStyle').setHidden( false);
		this.lookupReference('tfHtmlField').setHidden(true);
		this.lookupReference('sqlPanel').setHidden( false);
		this.lookupReference('tfComboText').setHidden( false);
		this.lookupReference('fcExtras').setHidden(true);
		this.lookupReference('cbForm').setHidden(true);
		switch(xtype){
			case 'panel':
				if(this.panel.items.items.length==1) 
					if(this.lookupReference('iconDelete')) { this.lookupReference('iconDelete').setHidden(true) }

				this.lookupReference('fcReadOnly').setHidden(true);
				this.lookupReference('tfStyle').setHidden(true);
				this.lookupReference('sqlPanel').setHidden(true);
				break;
			case 'fieldContainer':
				this.lookupReference('tfLabel').setHidden(true);
				this.lookupReference('fcReadOnly').setHidden(true);
				this.lookupReference('tfStyle').setHidden(true);
				this.lookupReference('sqlPanel').setHidden(true);
				break;
			case 'textfield':
			case 'datefield':
			case 'numberfield':
			case 'htmleditor':
			case 'radiogroup':
				this.lookupReference('ckReference').setHidden( false);
				this.lookupReference('tfComboText').setHidden(true);
				if(xtype=='datefield') { this.lookupReference('ckGreater').setHidden( false) };
				break;
			case 'combobox':
			case 'tagfield':
				this.lookupReference('ckOptionNew').setHidden( false);
				break;
			case 'checkbox':
				this.lookupReference('fcExtras').setHidden( false);
				this.lookupReference('tfHtmlField').setHidden( false);
				break;
			case 'form':
				this.lookupReference('cbForm').setHidden( false);
				this.lookupReference('tfStyle').setHidden(true);
				this.lookupReference('sqlPanel').setHidden(true);
				break;
			default:
				if(this.lookupReference('iconDelete')) {
					this.lookupReference('iconDelete').setHidden(true)};
					this.lookupReference('tfName').setHidden(true);
					this.lookupReference('tfLabel').setHidden(true);
					this.lookupReference('fcReadOnly').setHidden(true);
					this.lookupReference('tfStyle').setHidden(true);
					this.lookupReference('sqlPanel').setHidden(true);
				}
	},
	
	getIDType:function(xtype) {
		var idType = -1;
		switch(xtype) {
			case 'panel': idType = 9; break;
			case 'fieldcontainer':a = 8; break;
			case 'textfield': 
			case 'datefield': 
			case 'numberfield': idType = 1; break;
			case 'htmleditor': idType = 4; break;
			case 'radiogroup': idType = 5; break;
			case 'checkbox': idType = 3; break;
			case 'combobox': idType = 2; break;
			case 'tagfield': idType = 7; break;
		}
		return idType
	},
	
	getXMLFields:function() {
		var xml = '', controller = this;
		this.panel.items.items.forEach(function(section) {
			var pos = 0;
			section.items.items.forEach(function(field) {
				xml += controller.setXMLField(controller.getIDType(field.xtype), section.title, ++pos, field);
				if(field.xtype == 'panel') {
					var xpos = 0;
					field.items.items.forEach(function(item) {
						if(item.xtype != 'button') xml += controller.setXMLField(controller.getIDType(item.xtype), field.name, ++xpos, item)
					})
				}
			})
		});
		xml += (this.fieldsToDelete) ? this.fieldsToDelete : '';
		return (xml != '') ? '<root>' + xml +  '</root>' : ''
	},
	
	setXMLField:function(xtype, sectionName, pos, obj){
		//console.log('setXMLField: ', obj)
		var xml = '';		
		if(pos == -1) {
			xml += '<IdField><![CDATA[' + (obj.idField ? obj.idField : 0) + ']]></IdField>' + 
				'<Action><![CDATA[DELETE]]></Action>'
		} else {
			if(obj.name == this.kpiName) pos = 0
			if(!obj.idField) {		
				xml += '<FieldName><![CDATA[' + obj.name + ']]></FieldName>' + 
					'<TableName><![CDATA[' + (obj.tableDB  ?  obj.tableDB  :  '') + ']]></TableName>' + 
					'<IdFieldType><![CDATA[' + xtype + ']]></IdFieldType>' + 
					'<Mandatory><![CDATA[' + (obj.mandatory ? 1 : 0) + ']]></Mandatory>' + 
					'<OnlyNumbers><![CDATA[' + (obj.xtype == 'numberfield' ? 1 : 0) + ']]></OnlyNumbers>' + 
					'<Class><![CDATA[]]></Class>' + 
					'<Style><![CDATA[' + (obj.style ? obj.style : '') + ']]></Style>' + 
					'<ComboField><![CDATA[' + (obj.fieldValue ? obj.fieldValue : '') + ']]></ComboField>' + 
					'<OnlyDates><![CDATA[' + (obj.xtype == 'datefield' ? 1 : 0) + ']]></OnlyDates>' + 
					'<FrontendText><![CDATA[' + (obj.xtype == 'panel' ? obj.title : obj.xtype == 'checkbox' ? obj.boxLabel : obj.fieldLabel) + ']]></FrontendText>' + 
					'<Section><![CDATA[' + sectionName + ']]></Section>' + 
					'<Posicion><![CDATA[' + pos + ']]></Posicion>' + 
					'<Procedimiento><![CDATA[' + (obj.spDB ? obj.spDB : '') + ']]></Procedimiento>' + 
					'<Parametros><![CDATA[' + (obj.input ? obj.input : '') + ']]></Parametros>' + 
					'<ComboFieldText><![CDATA[' + (obj.comboText ? obj.comboText : '') + ']]></ComboFieldText>' + 
					'<PermiteNull><![CDATA[' + (obj.allowEmpty ? 1 : 0) + ']]></PermiteNull>' + 
					'<SoloLectura><![CDATA[' + (obj.readonly ? 1 : 0) + ']]></SoloLectura>' + 
					'<EnReferencia><![CDATA[' + (obj.createReference ? 1 : 0) + ']]></EnReferencia>' + 
					'<ListaTitulo><![CDATA[]]></ListaTitulo>' + 
					'<ListaCampo1><![CDATA[]]></ListaCampo1>' + 
					'<ListaCampo2><![CDATA[]]></ListaCampo2>' + 
					'<IgnoraSiNoHayDatos><![CDATA[' + (obj.ignoreIfEmpty ? 1 : 0) + ']]></IgnoraSiNoHayDatos>' + 
					'<CampoHtml><![CDATA[' + (obj.htmlfield ? obj.htmlfield : '') + ']]></CampoHtml>' + 
					'<OpcionNuevo><![CDATA[' + (obj.opcionNew ? 1 : 0) + ']]></OpcionNuevo>' + 
					'<OcultaAprobarSiEsFalso><![CDATA[' + (obj.hiddenApprove ? 1 : 0) + ']]></OcultaAprobarSiEsFalso>' + 
					'<IdOrigen><![CDATA[' + (obj.originDB ? obj.originDB : 0) + ']]></IdOrigen>' + 
					'<FechaMayorOIgualaHoy><![CDATA[' + (obj.dategreater ? 1 : 0) + ']]></FechaMayorOIgualaHoy>' + 
					'<MenorQueCampo><![CDATA[0]]></MenorQueCampo>' + 
					'<IdField><![CDATA[' + (obj.idField ? obj.idField : 0) + ']]></IdField>' + 
					'<IdFormField><![CDATA[' + (obj.IdForm ? obj.IdForm : 0) + ']]></IdFormField>' + 
					'<Action><![CDATA[ADD]]></Action>' + 
					'<ActionResult><![CDATA[]]></ActionResult>' + 
					'<ActionError><![CDATA[]]></ActionError>';
			} else {
				//console.log('Modify: ', this.formFields)
				var saveObj = null
				for(var i=0; i< this.formFields.length;) {
					if(this.formFields[i++].idField == obj.idField) {
						saveObj = this.formFields[i -1]; i = this.formFields.length;
					}
				}
				if(saveObj) {
					var fields = [ { name: 'name', tag: 'FieldName' }, { name: 'fieldLabel', tag: 'FrontendText' }, { name: 'posicion', tag: 'Posicion' }, 
						{ name: 'mandatory', tag: 'Mandatory' }, { name: 'readonly', tag: 'SoloLectura' }, { name: 'style', tag: 'Style' }, 
						{ name: 'originDB', tag: 'IdOrigen' }, { name: 'tableDB', tag: 'TableName' }, { name: 'spDB', tag: 'Procedimiento' }, 
						{ name: 'input', tag: 'Parametros' }, { name: 'fieldValue', tag: 'ComboField' }, { name: 'comboText', tag: 'ComboFieldText' }]
						
					if(obj.xtype == 'radiogroup' || obj.xtype == 'datefield') {
						fields.push({ name: 'createReference', tag: 'EnReferencia' } )
						if(obj.xtype == 'datefield') fields.push({ name: 'dategreater', tag: 'FechaMayorOIgualaHoy' } )
					} else if(obj.xtype == 'checkbox') {
						fields.push({ name: 'hiddenApprove', tag: 'OcultaAprobarSiEsFalso' } )
						fields.push({ name: 'ignoreIfEmpty', tag: 'IgnoraSiNoHayDatos' } )						
					} else if(obj.xtype == 'form') {
						fields.push({ name: 'IdForm', tag: 'IdFormField' } )
					}// else if(obj.xtype == 'tagfield') fields.push({ name: 'optionNew', tag: 'IdFormField' } )
					
					fields.forEach(function(field) {
						if(JSON.stringify(obj[field.name]) != undefined) {
							//console.log('Analizando: ' + [field.name] + ' = ' + JSON.stringify(saveObj[field.name]) + ' -> ' + JSON.stringify(obj[field.name])) 
							if (JSON.stringify(obj[field.name]) != JSON.stringify(saveObj[field.name])) {
								xml += '<' + field.tag + '>' + obj[field.name] + '</' + field.tag + '>'
							}
						}
					})
					if(xml != '') xml += '<IdField><![CDATA[' + (obj.idField ? obj.idField : 0) + ']]></IdField><Action><![CDATA[MODIFY]]></Action>'
				}				
				
			}
		}
		return (xml != '') ? '<campo>' + xml + '</campo>' : ''
	},
	
	getPosition: function(obj) {
		//console.log('getPosition: ', obj);
		var a = false, items = this.objSelected.xtype != 'panel' ? this.sectionSelected.items.items : this.objSelected.container.component.items.items;
		for(i=0; i<items.length; i++) {
			if(items[i].name== obj.name) { a=i; break; }
		}
		return a
	},
	
	upField: function() {
		var pos = this.getPosition(this.objSelected);
		//console.log('upField: ', this.objSelected, ' posActual: ', pos);
		if(pos-1 > -1){
			var obj = this.objSelected.xtype != 'panel' ? this.sectionSelected : this.objSelected.container.component;
		    obj.moveAfter(obj.getComponent(pos-1), obj.getComponent(pos));
			this.objSelected.posicion = pos
		}
	},
	
	downField: function() {
		var pos = this.getPosition(this.objSelected);
		//console.log('downField: ', this.objSelected, ' posActual: ', a);
		var obj = this.objSelected.xtype != 'panel' ? this.sectionSelected : this.objSelected.container.component;
		if(pos +1 < obj.items.items.length) {
			obj.moveAfter(obj.getComponent(pos), obj.getComponent(pos +1));
			this.objSelected.posicion = pos
		}
	},
	
	//*** MODIFY METHODS
	
	changeName:function(obj, value) {
		if(obj.hasFocus && this.objSelected) {
			this.objSelected.name = value
		}
	},
	
	changeLabel:function(obj, value) {
		//console.log('changeLabel: ', this.objSelected);
		if(obj.hasFocus && this.objSelected) {
			if(this.objSelected.xtype == 'panel') {
				this.objSelected.setBind({ title: value})
			} else { 
				if(this.objSelected.xtype == 'checkbox') {
					this.objSelected.setBind({ boxLabel: value })
				}else { this.objSelected.setBind({ fieldLabel: value })}
			}
		}
	},
	
	changeProperties:function(obj, value) {
		if(obj.hasFocus && this.objSelected){
			//console.log('changeProperties: ', obj.name + ' -> ' + value);
			this.objSelected[obj.name] = value
		}
	},
	
	//*** ADD METHODS
	
	addSection:function(){
		var idSection = 'section-'+ ++this.contSections;
		var section = Ext.create({
			xtype:'panel',
			reference: idSection, name: idSection,
			bodyPadding:10, collapsible: true,
			defaults:{ width:'100%' },
			items:[],
			viewModel:{ data:{ title: 'Nueva seccion' } },
			bind:{ title: '{title}' },
			header:{ listeners:{ click: 'fieldSelection' } }
		});		
		this.panel.add(section);
		if(!this.kpi) this.addKPIField()
		this.sectionSelected = section
	},

	addKPIField: function() {
		console.log('Agregando kpi...')
		this.panel.items.items[0].add(Ext.create({
			xtype: 'numberfield', name: this.kpiName,
			editable: false, posicion: 0,
			bind: { fieldLabel:'KPI' }, labelWidth: 125,
			mandatory: false, readonly: false, createReference: false,
			listeners:{ focusenter:'fieldSelection'}, hidden: true
		}))
		this.kpi = true
	},
	
	addFieldContainer:function() {
		var idField = 'fc-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'panel', name: idField,
			posicion: this.contFields,
			layout: 'hbox', bodyPadding: 5, border: true,
			defaults:{ flex:1, margin:'0 20 0 0' }, margin: '5 0 10 0',
			items:[{
				xtype:'button',
				userCls:'circular', margin: '2 10 3 0',
				iconCls:'x-fa fa-plus', flex:0,
				handler:'selectedColumns'
			}],
			listeners:{ focusleave:'outSelectionColumns', add:'addItemToFieldContainer' } 
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	selectedColumns:function(btn) {
		var header = btn.container.component;
		header.setBodyStyle('backgroundColor', 'lightgrey');
		this.fieldSelection(header)
	},
	
	outSelectionColumns:function(a) { a.setBodyStyle('backgroundColor', 'white') },
	
	addItemToFieldContainer:function(b, a){
		if(a.xtype != 'button' && a.xtype != 'textfield' && a.xtype != 'datefield' 
			&& a.xtype != 'numberfield' && a.xtype != 'combobox' && a.xtype != 'checkbox') {
			b.remove(a)
		}
	},
	
	addLabel:function(){
		var idItem = 'label-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'label', name: idItem, 
			posicion: this.contFields,
			bind:{ text:'Nuevo texto' },
			margin:'0 0 50 0',
			listeners:{ focus:'fieldSelection', focusenter:'fieldSelection' }
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addTextfield:function(){
		var idItem = 'field-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype:'textfield', name: idItem,
			editable: false, posicion: this.contFields,
			bind:{ fieldLabel:'Nuevo Campo' }, labelWidth: 125,
			mandatory: false, readonly: false, createReference: false,
			listeners:{ focusenter:'fieldSelection'}
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addHTMLEditor:function(){
		var idItem = 'htmleditor-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'htmleditor', name: idItem,
			enableSourceEdit: false,
			posicion: this.contFields,
			labelAlign: 'top', labelClsExtra: 'labelField',
			bind: { fieldLabel:'Nuevo Campo' },
			mandatory:false, readonly:false,
			listeners:{ focusenter:'fieldSelection', sync:'fieldSelection'}
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addDatefield: function() {
		var idItem = 'datefield-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'datefield', name: idItem,
			editable: false, posicion:this.contFields,
			bind: { fieldLabel:'Nuevo Campo' }, labelWidth: 125,
			mandatory: false, readonly: false, createReference: false,
			dategreater: false,
			listeners:{ focusenter:'fieldSelection' }
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addNumberfield: function() {
		var idItem = 'numberfield-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'numberfield', name: idItem,
			editable: false, posicion: this.contFields,
			bind: { fieldLabel:'Nuevo Campo' }, labelWidth: 125,
			mandatory: false, readonly: false, createReference: false,
			listeners:{ focusenter:'fieldSelection'}
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addCombobox:function(){
		var idItem = 'combo-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'combobox', name: idItem,
			editable: false, posicion: this.contFields,
			bind:{ fieldLabel:'Nuevo combo' }, labelWidth: 125,
			displayField:'value', valueField:'id', publishes:'value',
			mandatory: false, readonly: false, optionNew: false,
			listeners:{ focusenter:'fieldSelection'}
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addCheckbox:function(){
		var idItem = 'checkbox-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'checkbox', name: idItem,
			posicion: this.contFields,
			bind: { boxLabel:'Nuevo Checkbox' }, labelWidth: 125,
			mandatory: false, readonly: false,
			listeners:{ focusenter:'fieldSelection' } 
		});
		if(this.sectionSelected){ this.sectionSelected.add(field) }
	},
	
	addRadiobutton:function(){
		var idItem = 'radiobutton-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'radiogroup', name: idItem,
			posicion: this.contFields,
			items:[{ 
				name:'opcion',
				bind:{ boxLabel:'{txtFields.words.yes}' },
				inputValue:'si', labelClsExtra:'labelField'
			}, {
				name:'opcion',
				boxLabel:'No', inputValue:'no',
				padding:'0 0 0 15', labelClsExtra:'labelField'
			}],
			bind:{ fieldLabel:'Nuevo Radio' }, labelWidth: 125,
			mandatory: false, readonly: false,
			listeners:{ focusenter:'fieldSelection' }
		});
		if(this.sectionSelected){ this.sectionSelected.add(field)}
	},
	
	addList:function(){
		var idItem = 'tagfield-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'tagfield', name: idItem,
			posicion: this.contFields,
			bind: { fieldLabel: 'Nuevo Campo' }, labelWidth: 125,
			mandatory: false, readonly: false,
			listeners:{ focusenter: 'fieldSelection' }
		});
		if(this.sectionSelected){ this.sectionSelected.add(field)}
	},
	
	addForm:function(){
		var idItem = 'form-' + this.contSections + ++this.contFields;
		var field = Ext.create({
			xtype: 'fieldcontainer', name: idItem,
			bind:{ fieldLabel:'Nuevo Campo' }, labelWidth: 125,
			labelClsExtra: 'labelField', posicion:this.contFields,
			IdForm:'',
			items:[{
				xtype:'button', userCls:'circular',
				iconCls:'x-fa fa-file-text-o'
			}],
			mandatory: false, readonly: false,
			listeners:{ focusenter:'fieldSelection' }
		});
		if(this.sectionSelected){ this.sectionSelected.add(field)}
	},
	
})