Ext.define('Designer.view.screen.CategoriesController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.categories',
	
	config: {
		data: null
	},
	
	initScreen: function() {
		this.getCategories(function(error, callback) { console.log(callback) })			
	},
	
	refreshScreen: function() {
		this.initScreen()
	},
	
	getCategories: function(callback) {
		var store = this.lookupReference('categoriesGrid').getStore()
        store.getCategories(function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else {
				store.getSorters().add({ property: 'Title', direction: 'ASC' })
				callback(false, result.response)
			}
        })
	},
	
	findCategory: function(o, record) {
        var store = this.lookupReference('categoriesGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'Title', value: record})
    },
	
	newCategory: function() {
		Ext.getCmp('main').controller.openPopup('category', {})
	},
	
	editCategory: function(grid, rowIndex) {
		Ext.getCmp('main').controller.openPopup('category', grid.getStore().getAt(rowIndex).data)
	},
	
	deleteCategory: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveCategory, data.Title), function(btn) {
            if (btn == 'yes') {
				var store = grid.getStore()
				grid.mask('Espere...');
				store.delete(data, function(error, result) {
					grid.unmask();
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else controller.refreshScreen()
				})
			}			
		})	
	}
	
})