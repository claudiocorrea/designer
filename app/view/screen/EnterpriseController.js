Ext.define('Designer.view.screen.EnterpriseController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.enterprise',
	
	config: {
		data: null
	},
	
	initScreen: function() {
		this.getEnterprises(function(error, callback) { console.log(callback) })			
	},
	
	refreshScreen: function() {
		this.initScreen()
	},
	
	getEnterprises: function(callback) {
		var store = this.lookupReference('enterprisesGrid').getStore()
        store.getEnterprises(function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else {
				store.getSorters().add({ property: 'Descripcion', direction: 'ASC' })
				callback(false, result.response)
			}
        })
	},
	
	findEnterprise: function(o, record) {
        var store = this.lookupReference('enterprisesGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'Descripcion', value: record})
    },
	
	newEnterprise: function() {
		Ext.getCmp('main').controller.openPopup('enterprise', {})
	},
	
	editEnterprise: function(grid, rowIndex) {
		Ext.getCmp('main').controller.openPopup('enterprise', grid.getStore().getAt(rowIndex).data)
	},
	
	deleteEnterprise: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveEnterprise, data.Descripcion), function(btn) {
            if (btn == 'yes') {
				var store = grid.getStore()
				grid.mask('Espere...');
				store.delete(data, function(error, result) {
					grid.unmask();
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else controller.refreshScreen()
				})
			}			
		})	
	}
	
})