Ext.define('Designer.view.screen.FormsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.forms',
	
	config: {
		data: null
	},
	
	initScreen: function() {
		this.getForms(function(error, callback) { console.log(callback) })			
	},
	
	refreshScreen: function() {
		this.initScreen()
	},
	
	getForms: function(callback) {
		var store = this.lookupReference('formsGrid').getStore()
        store.getForms(function(error, result) {
            if(error) {
                if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
                callback(true, null)
            } else {
				store.getSorters().add({ property: 'FormName', direction: 'ASC' })
				callback(false, result.response)
			}
        })
	},
	
	findForm: function(o, record) {
        var store = this.lookupReference('formsGrid').getStore()
        store.getFilters().add({id: 'fltDefault', property: 'FormName', value: record})
    },
	
	newForm: function() {
		Ext.getCmp('main').controller.openPopup('form', {})
	},
	
	newFormDesigner: function() {
		Ext.getCmp('main').controller.setCurrentView('formDesigner', {})
	},
	
	editForm: function(grid, rowIndex) {
		Ext.getCmp('main').controller.openPopup('form', grid.getStore().getAt(rowIndex).data)
	},
	
	designerForm: function(grid, rowIndex) {
		Ext.getCmp('main').controller.setCurrentView('formDesigner', grid.getStore().getAt(rowIndex).data)
	},
	
	deleteForm: function(grid, rowIndex) {
		var data = grid.getStore().getAt(rowIndex).data
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveForm, data.FormName), function(btn) {
            if (btn == 'yes') {
				var store = grid.getStore()
				grid.mask('Espere...');
				store.delete(data, function(error, result) {
					grid.unmask();
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					}
				})
			}			
		})	
	}
	
})