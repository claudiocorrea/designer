Ext.define('Designer.view.screen.DesignerController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.designer',
	
	config: {
		data: null,
		steps: null
	},
	
	init: function() {
		this.data = this.getView().config.data
		console.log(this.data)
	},
	
	loadLibraries: function() {
		Designer.util.Utils.loadScript('./gojs/controller/WorkflowController.js', function (err, cbk) { });
		Designer.util.Utils.loadScript('./gojs/model/Workflow.js', function (err, cbk) { });
	},
	
	initScreen: function() {
		if(this.data.Title) {
			var controller = this
			this.lookupReference('title').setText(this.data.Title)
			this.getStepsByWorkflowId(function(callback) {
				var data = { workflow: controller.data, nodes: {} }
				if(callback.length > 0) data.nodes = controller.stepsToJSON(callback)			
				initModel(data)
			})
		} else initModel({})
	},
	
	refreshScreen: function() {
		if(this.data.Title) {
			var controller = this
			this.getStepsByWorkflowId(function(callback) {
				var data = { workflow: controller.data, nodes: {} }
				if(callback.length > 0) data.nodes = controller.stepsToJSON(callback)			
				setModel(data)
			})
		} else setModel({})
	},
	
	returnTab: function() {
        Ext.getCmp('main').controller.setCurrentView('workflows')
    },
	
	increaseZoom: function () { increaseZoom() },
	decreaseZoom: function () { decreaseZoom() },
	
	viewJSONModel: function() {
		Ext.Msg.alert('MODEL',getJSONModel())
	},
	
	modelCheck: function() {	
		Ext.Msg.show({ msg: txtFields.messages.publishingModel, progressText: 'Publishing...', width: 300, wait: { interval: 200 } });
		Ext.Ajax.request({
            url: globalParams.urlServices + 'workflow/publish?idUser=' + loginUser.idUser + '&workflowId=' + this.data.WorkflowID ,
            method: 'GET',
            success: function (response) {
				console.log('Publish SUCCESS...', response)
				//checkModel()
				Ext.Msg.close()
			},
            failure: function (result) {
				console.log('Publish FAIL...', result)
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			}
		})
	},
	
	getStepsByWorkflowId: function(callback) {		
		var store = new Designer.store.Steps()
		store.getSteps(this.data.WorkflowID, function(error, result) {
			if(!error) {
				console.log(result)
				callback(result.response)
			} else {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			}
		})
	},
	
	findStep: function(array, stepId) {
		var node = 'init'
		array.forEach(function(step) {
			if(node == 'init' && step.step.StepId == stepId)
				node = step.key
		})
		return node
	},
	
	stepsToJSON: function(steps) {		
		var controller = this, nodes = new Array(), contSteps = -1;
		steps.forEach(function(step) {	
			step = step.data
			var node = {
				key: 'step-' + ++contSteps,
				category: "step",
				text: step.Title,
				parent: '',
				error: false,
				step: step
			}
			if(step.Groups != '') {
				var newGroups = new Array()
				var groups = step.Groups.split('|')
				groups.forEach(function(group) {
					if(group != '') newGroups.push({ name: group })
				})
				node.step.Groups = newGroups
			}
			if(step.Users != '') {
				var newUsers = new Array()
				var users = step.Users.split('|')
				users.forEach(function(user) {
					if(user != '') {
						var mandatory = false
						if(user.substring(0,1) == '*') {
							mandatory = true
							user = user.substring(1)
						}
						var approved = 0
						var date = ''
						if(user.indexOf('-Approved:') != -1 ) {
							approved = 1
							date = user.split('-Approved:')[1]
							user = user.substring(0, user.indexOf('-Approved:'))
						} else if(user.indexOf('-Rejected:') != -1 ) {
							approved = -1
							date = user.split('-Rejected:')[1]
							user = user.substring(0, user.indexOf('-Rejected:'))
						}					
						newUsers.push({ "name": user, "mandatory": mandatory, "approved": approved, date: date })
					}
				})
				node.step.Users = newUsers				
			}
			if(step.UnionConCondicional) {					
				var newConditions = new Array()
				var conditions = step.Condicional.split('|')
				conditions.forEach(function(condition) {
					if(condition != '') newConditions.push({ condition: condition })
				})
				node.step.UnionConCondicional = true
				node.step.Conditions = newConditions
			}
				
			node.step.IdFormWorkflow = steps[0].data.IdForm
			nodes.push(node)
		})
		nodes.forEach(function(node) {
			if(node.step.dependsON == null) node.step.dependsON = "init"
			node.parent = node.step.dependsON == null ? "init" : controller.findStep(nodes, node.step.dependsON.toString())			
		})
		return nodes
	}

	
})