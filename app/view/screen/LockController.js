Ext.define('Designer.view.screen.LockController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.lock',
	
	config: {
		user: null
	},

	init: function () {
		if(Ext.getCmp(globalParams.lang)) Ext.getCmp(globalParams.lang).checked = true
	},
	
	login: function() {
		Ext.Msg.show({ msg: txtFields.messages.alerts.initing, progressText: '', wait: { interval: 200 } })
		this.user = this.lookupReference('loginForm').getValues()
		//console.log('Login user...', this.user)
		var controller = this
		Ext.Ajax.request({
            url: globalParams.urlServices + 'session/login?username=' + this.user.username + '&password=' + this.user.password,
            method: 'POST',
            success: function (response) {
				console.log('SUCCESS...', response)
                response = Ext.decode(response.responseText)[0];
				if(response == undefined) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
				if(response.LoginResult != -1) {
					loginUser = {
						idUser: response.LoginResult,
						username: controller.user.username,
						name: response.Name,
						idProfile: response.IdProfile,
						token: response.Token
					}
					console.log(loginUser)
					if(loginUser.idProfile != 2) {
						controller.getView().destroy();
						Ext.Msg.hide()
						Ext.create('Designer.view.main.Main' )
					} else Ext.Msg.alert('ERROR', txtFields.messages.errors.profileError)
				} else Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
            },
            failure: function (error) {
				console.log('FAIL...', error)
				error = JSON.parse(error.responseText)
                Ext.Msg.alert('ERROR', error.message)
            }
		})
    },
	
	changeLanguage: function(bt) {
		globalParams.lang = bt.id
        window.location.href = window.location.pathname + '?lang=' + bt.id
    },
	
	onSpecialKey: function (field, e, options) {
        if (e.getKey() == e.ENTER) {
            var submitBtn = this.lookupReference('btnLogin');
            if (!submitBtn.disabled) {
                this.login();
            }
        }
    }

});
