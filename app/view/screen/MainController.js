Ext.define('Designer.view.screen.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

	init: function () {
		console.log(this)
		console.log(loginUser)
		console.log(globalParams)
		if(Ext.getCmp(globalParams.lang)) Ext.getCmp(globalParams.lang).checked = true
		this.findAvatar()
	},
	
	findAvatar: function() {
		var obj = this.lookupReference('imageUser')
		var btnUser = this.lookupReference('btnUser')
        try {
            Ext.Ajax.request({
                url: 'resources/images/users/IMG' + loginUser.idUser + '.png',
            }).then(function () { 
				obj.setSrc('resources/images/users/IMG' + loginUser.idUser + '.png')
				btnUser.setHtml('<table class="btnUser"><tr><td>' + loginUser.name + '</td><td><img src="resources/images/users/IMG' + loginUser.idUser + '.png" /></td></tr></table>')
            }, function () { 
				obj.setSrc('resources/images/users/HSA0.png')
				btnUser.setHtml('<table class="btnUser"><tr><td>' + loginUser.name + '</td><td><img src="resources/images/users/HSA0.png" /></td></tr></table>')
			}); //ERROR
        } catch (Exception) {
            console.log(Exception)
            obj.setSrc('resources/images/users/HSA0.png')
			btnUser.setHtml('<table class="btnUser"><tr><td>' + loginUser.name + '</td><td><img src="resources/images/users/HSA0.png" /></td></tr></table>')
        }
	},
	
	logout: function() {
        Ext.Msg.confirm(txtFields.words.confirm, txtFields.messages.confirmLogout, function(btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({                    
                    method: 'GET', url: 'session/invalidate',
                    callback: function () {
                        var lang = Designer.util.Utils.getQueryVariable('lang');
                        if(lang) lang = '?lang=' + lang; else lang = ''
                        window.location.href = 'http://' + window.location.hostname + ':' +
                                window.location.port + lang
                    }
                });
            }
        })
    },
	
	changeLanguage: function(bt) {
		globalParams.lang = bt.id
        window.location.href = window.location.pathname + '?lang=' + bt.id
    },
	
	onNavTreeSelectionChange: function (tree, node) {
        var to = node && node.get('routeId'), 
		p_data = node && node.get('data'), data = {};
		if(p_data) data = p_data
        if (to) { this.setCurrentView(to, data); }
    },
	
	setCurrentView: function(screen, data) {
		var panel = this.lookupReference('mainMenuPanel')		
		if(panel) {
			panel.removeAll()			
			panel.add(Ext.create({ xtype: screen + 'Screen', data:data }))
		}
	},
	
	openPopup: function(title, data) {
        var win = this.lookupReference(title + 'Popup')
        if (!win) {
			switch(title) {
				case 'category':
					win = new Designer.view.popup.Category({dataCategory: data }); break;
				case 'form':
					win = new Designer.view.popup.Form({dataForm: data }); break;
				case 'enterprise':
					win = new Designer.view.popup.Enterprise({dataEnterprise: data }); break;
				case 'user':
					win = new Designer.view.popup.User({dataUser: data }); break;
				case 'workflow':
					win = new Designer.view.popup.Workflow({dataWorkflow: data }); break;
			}
            
            this.getView().add(win);
        }
        win.show()
    },
	
	openClone: function(data) {
        var win = this.lookupReference('cloneWorkflowPopup')
        if (!win) {
            win = new Designer.view.popup.CloneWorkflow({dataWorkflow: data });
            this.getView().add(win);
        }
        win.show()
    },
	
	changeTheme: function(bt) {
		Ext.Ajax.request({
			url: globalParams.urlServices + 'util/theme',
			params: { ui: bt.id },
			method: 'GET',
			success: function (response) {
				console.log('SUCCESS...', response)
				window.location.reload()
			},
			failure: function (error) {
				console.log('FAIL...', error)
				error = JSON.parse(error.responseText)[0]
				Ext.Msg.alert('ERROR', error.message)
				Ext.create('Designer.view.screen.Lock')                
			}
		})
	}

});
