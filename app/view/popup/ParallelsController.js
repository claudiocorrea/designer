Ext.define('Designer.view.popup.ParallelsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.parallelsPopup',
	
	config: {
		data: null,
		form: null,
		toDelete: null
	},
	
	init: function() {		
		this.data = this.getView().dataStep
		this.form = this.lookupReference('parallelsForm').getForm()		
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		this.toDelete = ''
		var panel = this.lookupReference('panelList')
		panel.removeAll()
		
		var newChilds = new Array()
		for(var i=0; i < this.data.childs.length; i++) {
			if(this.data.testing) newChilds.push(this.data.childs[i])
			else if(this.data.childs[i].category == 'step') {
				if(this.data.childs[i].key != this.data.childs[i].step.StepId)
					newChilds.push(this.data.childs[i])
			}
		}		
		this.data.childs = newChilds
		
		var visibleButton = true
		if(this.data.type == -1 && this.data.childs.length > 2 ) visibleButton = false
			
		for(var i=0; i < this.data.childs.length; i++) {
			if(this.data.childs[i].category == 'step') {
				panel.add(
					{	xtype: 'panel', layout: 'hbox', border: true, defaults: { margin: 10 }, 
						items: [
							{ 	xtype: 'label', cls: 'titleSection', text: ' - ' + this.data.childs[i].text }, 
							{   xtype: 'button', name: 'btn-' + i, userCls: 'circular', iconCls:'x-fa fa-trash', margin: 5, 
								bind: { tooltip: '{txtFields.words.delete}' }, handler: 'deleteParallelNode',
								//hidden: (this.data.childs.length >= 2 && this.data.type == -1)
								hidden: visibleButton
							}
						]
					})
			}
		}
		this.form.setValues({ union: this.data.type })
	},
	
	deleteParallelNode: function(btn) {
		var panel = this.lookupReference('panelList')
		if(panel.items.items.length > 2) {
			var newChilds = new Array(), pos = btn.name.substring(4)
			if(!this.data.testing) {
				this.toDelete += '\t<StepOr>\n\t\t<StepID1>' + this.data.childs[pos].step.StepId + '</StepID1>\n' + 
							'\t\t<StepID2>' + this.data.childs[pos-1].step.StepId + '</StepID2>\n' + 
							'\t\t<UnionTipoY>' + this.form.getValues().union + '</UnionTipoY>\n\t\t<Operation>delete</Operation>\n\t</StepOr>\n'
			}
			for(var i=0; i < this.data.childs.length; i++) {
				if(i != parseInt(pos))
					newChilds.push(this.data.childs[i])
			}
			this.data.childs = newChilds
			panel.remove(btn.container.component)
		}
		if(panel.items.items.length <= 2) {
			panel.items.items.forEach(function(node) {
				node.items.items[1].setHidden(true)
			})
		}
	},
	
	save: function() {
		console.log('save: ', this.data)
		if(this.data.childs.length > 1) {
			Ext.Msg.show({ msg: txtFields.messages.savingChanges, progressText: 'Saving...', width: 300, wait: { interval: 200 } });
			var controller = this, xmlData = '<root>\n';
			for(var i=1; i < controller.data.childs.length; i++) {
				xmlData += '\t<StepOr>\n\t\t<StepID1>' + controller.data.childs[i].step.StepId + '</StepID1>\n' + 
							'\t\t<StepID2>' + controller.data.childs[i-1].step.StepId + '</StepID2>\n'
				xmlData += '\t\t<UnionTipoY>' + controller.form.getValues().union + '</UnionTipoY>\n\t\t<Operation>add</Operation>\n\t</StepOr>\n'
			}	
			xmlData += controller.toDelete
			xmlData += '</root>'
			console.log(xmlData)
			if(!this.data.testing) {
				var store = new Designer.store.Parallels()
				store.updateUnion(xmlData, function(error, result) {				
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
						Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
						returnObjectProperties(false, { change: true, data: {union: controller.form.getValues().union, childs: controller.data.childs} })
						controller.close()
					}
				})
			} else {		
				Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
				returnObjectProperties(false, { change: true, data: {union: this.form.getValues().union, childs: this.data.childs} })
			}
		} else Ext.Msg.alert('ERROR', txtFields.messages.errors.parallelsSteps)
	},
	
	delete: function() {
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, txtFields.messages.confirmRemoveGateway, function(btn) {
			if (btn == 'yes') {
				var store = new Designer.store.Parallels()
				var xmlData = '<root>'
				for(var i=1; i < controller.data.childs.length; i++) {
					xmlData += '\t<StepOr>\n\t\t<StepID1>' + controller.data.childs[i].step.StepId + '</StepID1>\n' + 
							'\t\t<StepID2>' + controller.data.childs[i-1].step.StepId + '</StepID2>\n'
					xmlData += '\t\t<UnionTipoY>' + controller.form.getValues().union + '</UnionTipoY>\n\t\t<Operation>delete</Operation>\n\t</StepOr>\n'
				}				
				xmlData += '</root>'
				store.updateUnion(xmlData, function(error, result) {				
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
						/*deleteNode({ data: controller.data}, true)
						updateModel()*/
						returnDeleteNode({remove: true, node: controller.data})
						controller.close()
					}
				})
			}
		})
	}
	
})