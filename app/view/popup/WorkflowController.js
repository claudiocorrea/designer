Ext.define('Designer.view.popup.WorkflowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.workflowPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataWorkflow
		this.form = this.lookupReference('workflowForm').getForm()
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		//console.log(this)
		var controller = this
		this.lookupReference('cbEmpresa').getStore().load({
			callback: function () {
				if (!controller.lookupReference('cbCategoria').getStore().isLoaded()) {
					controller.lookupReference('cbCategoria').getStore().load()
				}
			}
		})
		console.log(this.data)
		if(this.data.WorkflowID) {
			this.getView().setTitle(txtFields.titles.editWorkflow)			
		} else {
			this.data = { WorkflowID: '' }
		}
		this.form.setValues(this.data)
	},
	
	save: function() {	
		var save = true 	
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeWorkflow()
			console.log(data)
			var changes = this.haveChange()
			console.log('Tiene cambios: {0}', changes)
			if(changes.length > 0) {
				var store = new Designer.store.Workflows()
				store.saveWorkflow(data, function(error, result) {
					console.log(result)
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else { 
						if(controller.data.WorkflowID == '') {
							controller.data = controller.form.getValues()
							controller.data.WorkflowID = result.response						
							Ext.getCmp('main').controller.setCurrentView('designer', controller.data)
						} else Ext.getCmp('workflowsScreen').controller.refreshScreen()
						controller.close() 
					}	
				}) 
			} else this.close()
		}
	},
	
	completeWorkflow: function() {
        var currentValues = this.form.getValues()
		if(currentValues.WorkflowID == '') currentValues.WorkflowID = 0
        if(!currentValues.EndDate) currentValues.EndDate = currentValues.StartDate
        if(!currentValues.RelativeDates) currentValues.RelativeDates = false
        if(!currentValues.CanBeNode) currentValues.CanBeNode = false
        if(!currentValues.GeneraAlertas) currentValues.GeneraAlertas = false
        if(!currentValues.UsaCondicionalesPasos) currentValues.UsaCondicionalesPasos = false
        if(!currentValues.UsaCondicionalesUsuarios) currentValues.UsaCondicionalesUsuarios = false
        this.form.setValues(currentValues)
        return currentValues
    },
	
	haveChange: function() {
        var currentValues = this.completeWorkflow()
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
                    changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    }
	
})