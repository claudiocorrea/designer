Ext.define('Designer.view.popup.CloneWorkflowController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.cloneWorkflowPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataWorkflow
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		var controller = this
		this.getStepsByWorkflowId(function(callback) {
			if(callback != null) {
				var panel = controller.lookupReference('panelList')
				panel.removeAll()
				callback.forEach(function(step) {
					controller.createStep(step)
				})
			}
		})
	},
	
	save: function() {	
		
	},
	
	getStepsByWorkflowId: function(callback) {		
		var store = new Designer.store.Steps()
		store.getSteps(this.data.WorkflowID, function(error, result) {
			if(!error) {				
				var list = new Array()
				result.response.forEach(function(step) { list.push(step.data) })
				console.log(list)
				callback(list)
			} else {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
				callback(null)
			}
		})
	},
	
	createStep: function(step) {
		var startDate = new Date(step.StartDate.split('/')[2],step.StartDate.split('/')[0]-1, step.StartDate.split('/')[1])
		var endDate = new Date(step.EndDate.split('/')[2],step.EndDate.split('/')[0]-1, step.EndDate.split('/')[1])
		var panel = this.lookupReference('panelList')
		var store = new Designer.store.Users();
		store.getUsers(step.StepId, function(error, result) {
			if(error) {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			} else {
				panel.add({ title: step.Title, collapsed: true,
					items: [
						{ 	xtype: 'panel', layout: 'column',
							defaults: { bodyPadding: 5, columnWidth: 0.5 },
							items: [						
								{	items: [
										{ 	xtype: 'checkboxfield',
											name: 'AllowDocs', 
											bind: { fieldLabel: '{txtFields.titles.externDocs}' },
											value: step.AllowDocs
										}, { 	
											xtype: 'checkboxfield',
											name: 'GenerateAlerts', 
											bind: { fieldLabel: '{txtFields.titles.gralAlerts}' },
											value: step.GenerateAlerts
										}, {
											xtype: 'datefield',
											name: 'StartDate',
											bind: { fieldLabel: '{txtFields.titles.startDate}' },
											width: 250,
											format: 'd/m/Y',
											allowBlank: false, value: startDate,
											minValue: new Date()
										}, {
											xtype: 'datefield',
											name: 'EndDate',
											bind: { fieldLabel: '{txtFields.titles.endDate}' },
											width: 250,
											format: 'd/m/Y',
											allowBlank: false, value: endDate,
											minValue: new Date()
										}, { 	
											xtype: 'numberfield',
											bind: { fieldLabel: '{txtFields.words.day}s' },
											name: 'Dias', width: 250,
											minValue: 1, maxValue: 60,
											value: step.Dias
										}
									]							
								}, {
									scrollable: 'vertical', height: 220,
									items: [{ xtype: 'gridUsers', store: store, flex: 1, border: true} ]
								}
							]					
						}
					]
				})
			}
		})	
	}
	
})