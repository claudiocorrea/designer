Ext.define('Designer.view.popup.StepController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.stepPopup2',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataStep
		this.form = this.lookupReference('stepForm').getForm();
		console.log(this.data)
	},
	
	close: function() {
		returnStepProperties(false, {})
		this.getView().destroy()
	},
	
	loadData: function() {
		var controller = this
		if (!this.lookupReference('cbForms').getStore().isLoaded()) {
			this.lookupReference('cbForms').getStore().load({
				callback: function () {
					if (!controller.lookupReference('cbTimesAlerts').getStore().isLoaded()) {
						controller.lookupReference('cbTimesAlerts').getStore().load({
                            callback: function () {
								if (!controller.lookupReference('cbTemplate').isDisabled()) {
									if (!controller.lookupReference('cbTemplate').getStore().isLoaded()) {
										controller.lookupReference('cbTemplate').getStore().load()
									}
								}
								if (!controller.lookupReference('cbTemplateAlerts').isDisabled()) {
									if (!controller.lookupReference('cbTemplateAlerts').getStore().isLoaded()) {
										controller.lookupReference('cbTemplateAlerts').getStore().load()
									}
								}
                            }
                        })
					}
				}
			})
		}		
		this.form.setValues(this.completeStep(this.data))	
		this.lookupReference('numerationType').setHidden(this.data.parent != '1')
	},
	
	save: function() {
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeStep(this.form.getValues())
			console.log(data)			
			var changes = this.haveChange(false)
			console.log('Tiene cambios: {0}', changes)
			if(changes.length > 0) {
				controller.updateFields(function(error) {
					if(!error) {
						if(controller.haveChange(true).length > 0) {
							var store = new Designer.store.Steps()
							store.saveStep(data, function(error, result) {
								if(error) {
									if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
									else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
								} else { 						
									Ext.getCmp('designerScreen').controller.refreshScreen()
									controller.close() 
								}	
							})
						} else controller.close() 
					}
				})				
			} else this.close()			
		} else 'No es valido!'
	},
	
	completeStep: function(currentValues) {		
		if(currentValues.StepId != '') currentValues.key = currentValues.StepId
        if(currentValues.Title != '') currentValues.text = currentValues.Title
		else currentValues.Title = currentValues.text
		currentValues.Dias = parseInt(currentValues.Dias)
        currentValues.IdState = parseInt(currentValues.IdState)
        currentValues.IdForm = parseInt(currentValues.IdForm)
		if(!currentValues.GenerateAlerts) currentValues.GenerateAlerts = false
        if(!currentValues.FormReadOnly) currentValues.FormReadOnly = false
        if(!currentValues.AllowDocs) currentValues.AllowDocs = false
        if(!currentValues.RechazoEnFormularios) currentValues.RechazoEnFormularios = false
        if(!currentValues.NotificaViaMail) currentValues.NotificaViaMail = false
        if(!currentValues.NotificaAprobaciones) currentValues.NotificaAprobaciones = false
        if(!currentValues.NotificaRechazos) currentValues.NotificaRechazos = false
		return currentValues
	},
	
	haveChange: function(onlyStep) {
        var currentValues = this.completeStep(this.form.getValues())
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
					if(key != 'Groups' && key != 'Users') 
						changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
		if(!onlyStep) {
			var store = this.lookupReference('gridFields').getStore();
			if(store.getModifiedRecords().length > 0)
				changes.push('Se modificaron: ' + store.getModifiedRecords().length + ' fields')
		}
        return changes
    },
	
	delete: function() {
		this.deleteStep(this.data)
	},
	
	deleteStep: function(data) {
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveStep, data.Title), function(btn) {
            if (btn == 'yes') {
				var store = new Designer.store.Steps()
				store.delete(data, function(error, result) {
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
                        Ext.getCmp('designerScreen').controller.refreshScreen()
                        //Ext.Msg.alert('', txtFields.messages.alerts.saveSuccessfull)
                        controller.close()
					}
				})
			}			
		})
	},
	
	loadFields: function() {
		console.log(this.data)
		if(this.data.StepId != 0) {
			var controller = this
			var store = this.lookupReference('gridFields').getStore();
			store.getFields(this.data.IdFormWorkflow, this.data.StepId, function(error, result) {
				if(error) {
					if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
					else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
				}
			})
		}
	},
	
	updateFields: function(callback) {
		var controller =  this
		var store = this.lookupReference('gridFields').getStore();
		if(store.getModifiedRecords().length > 0) {
			var records = '<root>'
            store.data.items.forEach(function (item) {
                records += '<campo><IdField>' + item.data.IdField + '</IdField>' + 
					'<FieldName>' + item.data.FieldName + '</FieldName>' + 
					'<Habilitado>' + item.data.Habilitado + '</Habilitado>' +
					'<Visible>' + item.data.Visible + '</Visible></campo>'
            })
            records += '</root>'
			
			store.updateFields({ stepId: controller.data.StepId, xmlData: records}, function (error, result) {
                if (error) {
                    if (result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                    else if (result.error != 0) Ext.Msg.alert('ERROR', result.message)
                }
				callback(error, result)
            })
		} else callback(false)
	}
})