Ext.define('Designer.view.popup.GroupsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.groupsPopup',
	
	config: {
		data: null
	},
	
	init: function() {
		this.data = this.getView().dataStep
		console.log(this.data)
	},
	
	loadGroups: function() {
		if(this.data.key != this.data.step.StepId) {
			var controller = this
			var store = this.lookupReference('gridGroups').getStore();
			store.getGroups(this.data.step.StepId, function(error, result) {
				if(!error) {
					var enabledUsers = true
					store.data.items.forEach(function (item) {
						if(item.data.Enabled) { enabledUsers = false }						
					})	
					controller.lookupReference('groupsForm').items.items[1].setDisabled(enabledUsers)
					if(!enabledUsers) controller.loadUsers()
				} else {
					if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
					else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
				}
			})
		}
	},
	
	loadUsers: function() {
		var controller = this
		var store = this.lookupReference('gridMandatoryUsers').getStore();
		store.getUsers(this.data.step.StepId, function(error, result) {
			if(error) {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			} else {
				var users = new Array()
				store.data.items.forEach(function (item) {
					if (item.data.IdUser != 2507 && item.data.IdUser != 2518)  {
						users.push({ name: item.data.fullname, mandatory: item.data.Mandatory, approved: 0, date: ""} )
					}
				})
				controller.data.step.Users = users
			}
		})
	},
	
	saveGroups: function() {
		var controller = this
		var store = this.lookupReference('gridGroups').getStore();
		if(store.getModifiedRecords().length > 0) {
            Ext.Msg.show({ msg: txtFields.messages.savingGroups, progressText: 'Saving...', width: 300, wait: { interval: 200 } });
            var records = '<root>', groups = new Array()
            store.data.items.forEach(function (item) {
                if (item.data.Enabled) {
					records += '<Group><IdGroup>' + item.data.IdGroup + '</IdGroup><Mandatory>1</Mandatory></Group>'
					groups.push({ name: item.data.Name } )
				}
            })
			this.data.step.Groups = groups
            records += '</root>'
			if(!this.data.testing) {
				store.setGroups(this.data.StepID, records, function (error, result) {
					if (error) {
						if (result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if (result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
						controller.lookupReference('groupsForm').items.items[1].setDisabled(false)
						controller.loadUsers()
						Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
					}
				})
			}
        }
	},
	
	saveUsers: function() {
		var store = this.lookupReference('gridMandatoryUsers').getStore();
        if(store.getModifiedRecords().length > 0) {
            Ext.Msg.show({ msg: txtFields.messages.savingUsers, progressText: 'Saving...', width: 300, wait: { interval: 200 } });
            var records = '<root>', users = new Array()
            store.data.items.forEach(function (item) {
                if (item.data.Mandatory)  {
					records += '<User><IdUser>' + item.data.IdUser + '</IdUser></User>'
					users.push({ name: item.data.fullname, mandatory: true, approved: 0, date: ""} )
				}
            })
			this.data.step.Users = users
            records += '</root>'
			if(!this.data.testing) {
				store.setUsers(this.data.step.StepID, records, function (error, result) {
					if (error) {
						if (result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if (result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
						Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
					}
				})
			}
        }
	},
	
	close: function() {
        //Ext.getCmp('designerScreen').controller.refreshScreen()
		returnGroupsProperties({ change: true, data: this.data})
		this.getView().destroy()
	}
	
})