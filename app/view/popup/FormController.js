Ext.define('Designer.view.popup.FormController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.formPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataForm
		this.form = this.lookupReference('formForm').getForm()
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		if(!this.data.IdForm) {
			this.data = { IdForm: '', FormName: '', Description: '' }
		} else { 
			if(!this.lookupReference('cbEmpresa').getStore().isLoaded()) this.lookupReference('cbEmpresa').getStore().load()
			if(!this.lookupReference('cbFormType').getStore().isLoaded()) this.lookupReference('cbFormType').getStore().load()
		}
		
		this.form.setValues(this.data)
		//console.log(this.form.getValues())
	},
	
	save: function() {	
		var save = true 	
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeForm()
			console.log(data)
			var changes = this.haveChange()
			console.log('Tiene cambios: ', changes)
			if(changes.length > 0) {
				var store = new Designer.store.Forms()
				store.saveForm(data, function(error, result) {
					console.log(error, result)
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else { 
						Ext.getCmp('formsScreen').controller.refreshScreen()
						controller.close() 
					}	
				})
			} else this.close()
		}
	},
	
	completeForm: function() {
        var currentValues = this.form.getValues()
		if(currentValues.IdForm == '') currentValues.IdForm = 0
        this.form.setValues(currentValues)
        return currentValues
    },
	
	haveChange: function() {
        var currentValues = this.completeForm()
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
                    changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    }
	
})