Ext.define('Designer.view.popup.EnterpriseController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.enterprisePopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataEnterprise
		this.form = this.lookupReference('enterpriseForm').getForm()
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {	
		if(!this.data.IdEmpresa) {
			this.data = { IdEmpresa: '', Descripcion: '' }
		}
		this.form.setValues(this.data)
	},
	
	save: function() {	
		var save = true 	
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeEnterprise()
			console.log(data)
			var changes = this.haveChange()
			console.log('Tiene cambios: ', changes)
			if(changes.length > 0) {
				var store = new Designer.store.Enterprises()
				store.saveEnterprise(data, function(error, result) {
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else { 
						Ext.getCmp('enterprisesScreen').controller.refreshScreen()
						controller.close() 
					}	
				})
			} else this.close()
		}
	},
	
	completeEnterprise: function() {
        var currentValues = this.form.getValues()
		console.log(currentValues)
		if(currentValues.IdEmpresa == '') currentValues.IdEmpresa = 0
		if(!currentValues.Enabled) currentValues.Enabled = false
        this.form.setValues(currentValues)
        return currentValues
    },
	
	haveChange: function() {
        var currentValues = this.completeEnterprise()
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
                    changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    }
	
})