Ext.define('Designer.view.popup.DependsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.dependsPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataStep
		this.form = this.lookupReference('dependsForm')
		console.log(this.data)
	},
	
	close: function() {
		returnDependenceProperties({ change: true, data: this.data})
		Ext.getCmp('designerScreen').controller.refreshScreen()
		this.getView().destroy()
	},
	
	loadData: function() {
		if(!this.data.testing) {
			this.getFields()
			this.getConditions()	
		}
	},
	
	getFields: function() {
		var store = this.lookupReference('cbFields').getStore()
		store.getFields(this.data.firstStep.IdForm, this.data.firstStep.StepId, function(error, result){
			if(error) {				
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			} else {
				store.getFilters().add({id: 'fltSection', filterFn: function(item) {
						return item.data.FieldName.substring(0,2) != 'fc'
					}})
			}
		})
	},
	
	save: function() {
		if(this.form.isValid()) {
			var data = this.form.getValues()
			console.log(data)			
			var controller = this
			var condition = {
				stepId: this.data.toNode.step.StepId,
				dependsOn: this.data.fromNode.step.StepId,
				fieldId: data.field,
				operatorId: data.condition,
				value: data.value
			}
			if(!this.data.testing) {
				var store = new Designer.store.DependenceConditions()
				store.newConditional(condition, function(error, result) {
					console.log(result)
					if(!error) {
						controller.getConditions()
					} else {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					}
				})
			} 
		}
	},
	
	getConditions: function() {
		console.log('getConditions: ', this.data.toNode.step.StepId)
		var controller = this		
		var store = new Designer.store.DependenceConditions()
		store.getDependenceConditions(this.data.toNode.step.StepId, function(error, result) {
			if(!error) {				
				var panel = controller.lookupReference('panelList'), conditions = new Array()
				panel.removeAll()
				result.response.forEach(function(condition) {
					controller.createCondition(condition.data)
					conditions.push({ condition: condition.data.condition})
				})
				controller.data.conditions = conditions
				controller.data.condition = conditions.length == 0 ? false : true
				return result.response
			} else {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			}
		})
	},
	
	createCondition: function(condition) {
		var panel = this.lookupReference('panelList')
		panel.add(
		{	xtype: 'panel', layout: 'hbox', border: true, defaults: { margin: 10 }, 
			items: [
				{ 	xtype: 'label', cls: 'titleSection', text: condition.Step },
				{ 	xtype: 'label', cls: 'titleSection', text: condition.condition }, 
				{   xtype: 'button', id: condition.idcondition, userCls: 'circular', margin: 5, iconCls:'x-fa fa-trash',
					bind: { tooltip: '{txtFields.words.delete}' }, handler: 'deleteCondition'
				}
			]
		})
	},
	
	deleteCondition: function(obj) {
		console.log(obj)
		var controller = this
		var store = new Designer.store.DependenceConditions()
		store.delete(obj.id, function(error, result) {
			if(!error) {				
				controller.getConditions()
			} else {
				if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
			}
		})
	}
	
})