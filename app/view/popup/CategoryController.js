Ext.define('Designer.view.popup.CategoryController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.categoryPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataCategory
		this.form = this.lookupReference('categoryForm').getForm()
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		if(!this.data.CategoryID) {
			this.data = { CategoryID: '', Description: '' }
		} else this.lookupReference('cbEmpresa').getStore().load()
		
		this.form.setValues(this.data)
		console.log(this.form.getValues())	
	},
	
	save: function() {	
		var save = true 	
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeCategory()
			console.log(data)
			var changes = this.haveChange()
			console.log('Tiene cambios: ', changes)
			if(changes.length > 0) {
				var store = new Designer.store.Categories()
				store.saveCategory(data, function(error, result) {
					console.log(error, result)
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else { 
						Ext.getCmp('categoriesScreen').controller.refreshScreen()
						controller.close() 
					}	
				})
			} else this.close()
		}
	},
	
	completeCategory: function() {
        var currentValues = this.form.getValues()
		if(currentValues.CategoryID == '') currentValues.CategoryID = 0
        this.form.setValues(currentValues)
        return currentValues
    },
	
	haveChange: function() {
        var currentValues = this.completeCategory()
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
                    changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    }
	
})