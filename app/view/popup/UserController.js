Ext.define('Designer.view.popup.UserController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.userPopup',
	
	config: {
		data: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataUser
		this.form = this.lookupReference('userForm').getForm()
		console.log(this.data)
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {	
		if(!this.data.IdUser) {
			this.data = { IdUser: '', Login: '', FirstName: '', LastName: '' }
		} else { 			
			if (!this.lookupReference('cbProfile').getStore().isLoaded()) {
				this.lookupReference('cbProfile').getStore().load()
			}			
		}
		this.findAvatar()
		this.form.setValues(this.data)
	},
	
	findAvatar: function() {
		var btnUser = this.lookupReference('btnUser'), controller = this;
        try {
            Ext.Ajax.request({
                url: 'resources/images/users/IMG' + controller.data.IdUser + '.png',
            }).then(function () { 
				btnUser.setHtml('<div><img src="resources/images/users/IMG' + controller.data.IdUser + '.png" width="150" height="150" style="border-radius: 50%;" /></div>')
            }, function () { 
				btnUser.setHtml('<div><img src="resources/images/users/HSA0.png" width="150" height="150" style="border-radius: 50%;"/></div>')
			}); //ERROR
        } catch (Exception) {
            console.log(Exception)
            btnUser.setHtml('<div><img src="resources/images/users/HSA0.png" width="150" height="150" style="border-radius: 50%;" /></div>')
        }
	},
	
	save: function() {	
		var save = true 	
		if(this.form.isValid()) {
			var controller = this
			var data = this.completeUser()
			console.log(data)
			var changes = this.haveChange()
			console.log('Tiene cambios: ', changes)
			if(changes.length > 0) {
				var store = new Designer.store.Users()
				store.saveUser(data, function(error, result) {
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else { 
						Ext.getCmp('usersScreen').controller.refreshScreen()
						controller.close() 
					}	
				})
			} else this.close()
		}
	},
	
	completeUser: function() {
        var currentValues = this.form.getValues()
		//console.log(currentValues)
		if(currentValues.IdUser == '') currentValues.IdEmpresa = 0
		currentValues.IdProfile = parseInt(currentValues.IdProfile)
		if(!currentValues.Enabled) currentValues.Enabled = false
		if(!currentValues.TipoSistema) currentValues.TipoSistema = false
        this.form.setValues(currentValues)
        return currentValues
    },
	
	haveChange: function() {
        var currentValues = this.completeUser()
		var changes = new Array()
        for(var key in this.data) {
            if(JSON.stringify(currentValues[key]) != undefined) {
                if (JSON.stringify(this.data[key]) != JSON.stringify(currentValues[key])) {
                    changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    }
	
})