Ext.define('Designer.view.popup.StepController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.stepPopup',
	
	config: {
		data: null,
		data_bkp: null,
		form: null
	},
	
	init: function() {
		this.data = this.getView().dataStep		
		this.form = this.lookupReference('stepForm').getForm();
		this.form.reset()
		console.log(this.data)
		this.data_bkp = {
			conditions: this.data.step.Conditions ? this.data.step.Conditions : null,
			groups: this.data.step.Groups,
			users: this.data.step.Users
		}
	},
	
	close: function() {
		this.getView().destroy()
	},
	
	loadData: function() {
		var nodes = getAllNodes(), steps = new Array(), controller = this;
		nodes.forEach(function(node){
			if(node.key == 'init') steps.push({id: node.key, name: txtFields.words.init })
			else if(node.category == 'step' && controller.data.step.StepId != node.step.StepId) 
				steps.push({id: node.step.StepId, name: node.text })
		})
		this.lookupReference('cbDependsOn').setStore(Ext.create('Ext.data.Store', {
			fields: ['id', 'name'], data : steps
		}))
		this.lookupReference('numerationType').setHidden(this.data.parent != 'init')	
		
		this.data.step.Title = this.data.text
		this.form.setValues(this.completeStep(this.data.step))		

		this.loadCombos()
		if(this.data.key == this.data.step.StepId) {
			this.lookupReference('stepForm').items.items[2].setDisabled(true)
		} else this.loadFormFields()
	},
	
	loadCombos: function() {
		var controller = this
		if (this.lookupReference('cbForms') && !this.lookupReference('cbForms').getStore().isLoaded()) {
			this.lookupReference('cbForms').getStore().load({
				callback: function () {					
					if (!controller.lookupReference('cbTimesAlerts').getStore().isLoaded()) {
						controller.lookupReference('cbTimesAlerts').getStore().load({
							callback: function () {
								if (!controller.lookupReference('cbTemplate').isDisabled()) {
									if (!controller.lookupReference('cbTemplate').getStore().isLoaded()) {
										controller.lookupReference('cbTemplate').getStore().load()
									}
								}
								if (!controller.lookupReference('cbTemplateAlerts').isDisabled()) {
									if (!controller.lookupReference('cbTemplateAlerts').getStore().isLoaded()) {
										controller.lookupReference('cbTemplateAlerts').getStore().load()
									}
								}
							}
						})
					}
				}
			})
		}
	},
	
	loadFormFields: function() {
		if(this.data.key != this.data.step.StepId) {
			var idForm = this.data.step.IdFormWorkflow ? this.data.step.IdFormWorkflow : this.data.step.IdForm
			var controller = this
			var store = this.lookupReference('gridFields').getStore();
			store.getFields(idForm, this.data.step.StepId, function(error, result) {
				if(error) {
					if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
					else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
				} else {
					var items = store.getData().items,
					 	containers = []
					items.forEach(function(item) {
						item = item.data
						if(item.FieldName.substring(0,2) == 'fc') {
							containers.push({ id: item.FieldName, section: item.Section })
							store.getData().removeAt(item)
						}
					})
					items.forEach(function(item) {
						item = item.data
						for(var i=0; i<containers.length; i++) {
							if(item.Section == containers[i].id) {
								item.Section = containers[i].section
								i = 0
							}
						}
					})
					store.setGroupField('Section')
					store.getFilters().add({id: 'fltSection', filterFn: function(item) {
						return item.data.FieldName.substring(0,2) != 'fc' && item.data.FrontendText != 'null';
					}})
				}
			})
		}
	},
	
	completeStep: function(currentValues) {
		currentValues.IdState = parseInt(currentValues.IdState)
		currentValues.Dias = parseInt(currentValues.Dias)
        currentValues.IdForm = parseInt(currentValues.IdForm)
		if(!currentValues.GenerateAlerts) currentValues.GenerateAlerts = false
        if(!currentValues.FormReadOnly) currentValues.FormReadOnly = false
        if(!currentValues.AllowDocs) currentValues.AllowDocs = false
        if(!currentValues.RechazoEnFormularios) currentValues.RechazoEnFormularios = false
        if(!currentValues.NotificaViaMail) currentValues.NotificaViaMail = false
        if(!currentValues.NotificaAprobaciones) currentValues.NotificaAprobaciones = false
        if(!currentValues.NotificaRechazos) currentValues.NotificaRechazos = false
		currentValues.Conditions = this.data_bkp.conditions
		currentValues.Groups = this.data_bkp.groups
		currentValues.Users = this.data_bkp.users 
		return currentValues
	},
	
	haveChange: function() {
        var currentValues = this.completeStep(this.form.getValues())
		var changes = new Array()
        for(var key in this.data.step) {			
            if(JSON.stringify(currentValues[key]) != undefined) {
				if (JSON.stringify(currentValues[key]) != JSON.stringify(this.data.step[key])) {
					//console.log('Analizando: ' + key + ' = ' + JSON.stringify(this.data.step[key]) + ' -> ' + JSON.stringify(currentValues[key]))  
					if(key != 'Conditions' && key != 'Groups' && key != 'Users') 
						changes.push('Se modifico: ' + key + ' = ' + JSON.stringify(this.data.step[key]) + ' -> ' + JSON.stringify(currentValues[key]))                    
                }
            }
        }
        return changes
    },
	
	haveChangeFormFields: function() {
        var changes = new Array()
        var store = this.lookupReference('gridFields').getStore();
		if(store.getModifiedRecords().length > 0)
			changes.push('Se modificaron: ' + store.getModifiedRecords().length + ' fields')
        return changes
    },
	
	save: function() {		
		var controller = this, data = this.completeStep(this.form.getValues()),
			changes = this.haveChange()
		console.log('save: ', data)
		console.log('Tiene cambios: ', changes.length, changes)		
		if(!this.data.testing) {
			if(this.form.isValid()) {
				if(changes.length > 0) {
					Ext.Msg.show({ msg: txtFields.messages.savingChanges, progressText: 'Saving...', width: 300, wait: { interval: 200 } });
					if(this.data.key == data.StepId) data.StepId = 0
					if(data.dependsON == 'init') data.dependsON = 0
					var store = new Designer.store.Steps()
					store.saveStep(data, function(error, result) {
						if(error) {
							if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
							else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
						}  else {
							data.StepId = result.response.stepId
							returnObjectProperties(false, { change: true, data: data})
							Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)							
							controller.close()
						} 	
					})
				}
			} else Ext.Msg.alert('ERROR', txtFields.messages.errors.fields)
		} else {				
			Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
			returnObjectProperties(false, { change: true, data: data})
		}
	},
	
	delete: function() {
		var controller = this
		Ext.Msg.confirm(txtFields.words.confirm, Ext.String.format(txtFields.messages.confirmRemoveStep, this.data.step.Title), function(btn) {
            if (btn == 'yes') {
				var store = new Designer.store.Steps()
				store.delete(controller.data.step, function(error, result) {
					if(error) {
						if(result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
						else if(result.error != 0) Ext.Msg.alert('ERROR', result.message)
					} else {
                        /*Ext.getCmp('designerScreen').controller.refreshScreen()
                        //Ext.Msg.alert('', txtFields.messages.alerts.saveSuccessfull)*/
						returnDeleteNode({remove: true, node: controller.data})
                        controller.close()						
					}
				})
			}
		})
	},
	
	saveFormFields: function(callback) {
		var controller = this, changes = this.haveChangeFormFields()
		console.log('Tiene cambios: ', changes.length, callback)		
		if(changes.length > 0) {
			Ext.Msg.show({ msg: txtFields.messages.savingFields, progressText: 'Saving...', width: 300, wait: { interval: 200 } });
			var store = this.lookupReference('gridFields').getStore(), records = '<root>';
            store.data.items.forEach(function (item) {
                records += '<campo><IdField>' + item.data.IdField + '</IdField>' + 
					'<FieldName>' + item.data.FieldName + '</FieldName>' + 
					'<Habilitado>' + item.data.Habilitado + '</Habilitado>' +
					'<Visible>' + item.data.Visible + '</Visible></campo>'
            })
            records += '</root>'
			
			store.updateFields({ stepId: controller.data.StepId, xmlData: records}, function (error, result) {
                if (error) {
                    if (result.error == 401) Ext.Msg.alert('ERROR', txtFields.messages.errors.loginError)
                    else if (result.error != 0) Ext.Msg.alert('ERROR', result.message)
                } else Ext.Msg.alert(txtFields.words.confirm, txtFields.messages.alerts.saveSuccessfull)
				if(callback == '[object Function]') callback(error, result)
            })
		} else if(callback == '[object Function]') callback(false)
	}
	
	
})