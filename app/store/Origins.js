Ext.define('Designer.store.Origins', {
    extend: 'Ext.data.Store',
    alias: 'store.origins',
	
	requires: [ 'Ext.data.proxy.Rest' ],

    storeId: 'originsStore',
    model: 'Designer.model.Origin',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'origins/getAll',
			create: globalParams.urlServices + 'origins/newOrigin',
			destroy: globalParams.urlServices + 'origins/deleteOrigin'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getOrigins: function(callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	/*saveUser: function(userData, callback) {
		userData.idUser = loginUser.idUser
        userData.token = loginUser.token
		var store = this
        store.add(new Designer.model.User())
        store.save({
            scope: this,
            params: userData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: 'OK',
						response: JSON.parse(result._response.responseText),
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},
	
	delete: function(data, callback) {
		var store = this
        store.remove(data)        
        store.erase({
            scope: this,
			id: data.IdUser,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                idUserDelete: data.IdUser
            },
            callback: function (o, response) {
				console.log(response)
				var result = response
				if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
					callback(false, result)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
					callback(true, result)
                }                
            }
        })
		
	}
	*/
})