Ext.define('Designer.store.FormFields', {
    extend: 'Ext.data.Store',
    alias: 'store.formFields',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'formFieldsStore',
    model: 'Designer.model.Field',
	
	//groupField: 'Section',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'form/getFieldsByFormId',
			create: globalParams.urlServices + 'form/saveFormFields',
			update: globalParams.urlServices + 'form/updateFields'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getFields: function(formId, stepId, callback) {
		if(callback) {
			var store = this
			store.getProxy().setExtraParams({
				formId: formId,
				stepId: stepId
			})
			store.load({
				callback: function (o, response) {
					var result = response
					console.log('FormFields: ', result)
					if(result.success) {
						result = {
							error: -1,
							message: 'OK',
							response: result._resultSet.records
						}
					} else {
						result = {
							error: result.error.response.status,
							message: result.error.response.statusText,
							response: {}
						}
					}				
					callback(response.exception, result)
				}
			})
		}
    },
	
	updateFields: function(data, callback) {
		data.idUser = loginUser.idUser
        data.token = loginUser.token
		var store = this		
        store.save({
            scope: this,
            params: data,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: result._response.statusText,
						response: '',
					}
                } else {
					result = {
                        error: result.error.response.status,
                        message: JSON.parse(result.error.response.responseText).error.message,
                        response: {}
                    }
                }
                callback(response.operations[0].exception, result)
            }
        })
	},
	
	saveFields: function(data, callback) {
		data.idUser = loginUser.idUser
        data.token = loginUser.token
		var store = this
        store.add(new Designer.model.Field())
        store.save({
            scope: this,
            params: data,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: 'OK',
						response: (result._response != undefined) ? JSON.parse(result._response.responseText) : 'El servidor no responde.',
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : 
							(result.error.response.responseText != "") ? JSON.parse(result.error.response.responseText).message : result.error.statusText,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},
	
})