Ext.define('Designer.store.DependenceConditions', {
    extend: 'Ext.data.Store',
    alias: 'store.dependenceConditions',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'dependenceConditionsStore',
    model: 'Designer.model.DependenceCondition',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'step/getDependenceConditions',
			create: globalParams.urlServices + 'step/addDependenceConditions',
			destroy: globalParams.urlServices + 'step/removeDependenceConditions'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getDependenceConditions: function(stepId, callback) {
        var store = this
		store.getProxy().setExtraParams({
            stepId: stepId
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	newConditional: function(conditional, callback) {
		conditional.idUser = loginUser.idUser
		var store = this
        store.add(new Designer.model.DependenceCondition())
        store.save({
            scope: this,
            params: conditional,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) { 
					result = JSON.parse(result._response.responseText)[0]
					if(result.Status == 'Error') {
						response.exception = true
						result = {
							error: result.Status,
							message: result.Error,
							response: {}
						}
					} else {
						result = {
							error: -1,
							message: 'OK',
							response: {}
						}
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},
	
	delete: function(conditionId, callback) {
        var store = this
        store.erase({
            scope: this,
			id: conditionId,
            params: {
                idUser: loginUser.idUser,
                conditionId: conditionId
            },
            callback: function (o, response) {
                console.log(response)
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                    callback(false, result)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                    callback(true, result)
                }
            }
        })
    }
	
})