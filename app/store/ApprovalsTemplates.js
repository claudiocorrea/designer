Ext.define('Designer.store.ApprovalsTemplates', {
    extend: 'Ext.data.Store',
    alias: 'store.approvalsTemplates',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'ApprovalsTemplatesStore',
    model: 'Designer.model.ApprovalTemplate',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'stores/getAllApprovalsTemplates'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getApprovalsTemplates: function(callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
})