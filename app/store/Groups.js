Ext.define('Designer.store.Groups', {
    extend: 'Ext.data.Store',
    alias: 'store.groups',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'groupsStore',
    model: 'Designer.model.Group',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'step/getGroupsByStep',
			update: globalParams.urlServices + 'step/setGroups'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getGroups: function(stepId, callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token,
			stepId: stepId
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	setGroups: function(stepId, groups, callback) {
		var groupsData = {
			idUser: loginUser.idUser,
			token: loginUser.token,
			stepId: stepId,
			groups: groups
		}		
		var store = this
        store.save({
            scope: this,
            params: groupsData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: result._response.statusText,
						response: JSON.parse(result._response.responseText).result,
					}
                } else {
                    result = {
                        error: result.error.response.status,
                        message: JSON.parse(result.error.response.responseText).error,
                        response: {}
                    }
                }
                callback(response.operations[0].exception, result)
            }
        })
	}
	
})