Ext.define('Designer.store.Categories', {
    extend: 'Ext.data.Store',
    alias: 'store.categories',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'categoriesStore',
    model: 'Designer.model.Category',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'stores/getAllCategories',
            create: globalParams.urlServices + 'stores/newCategory',
			destroy: globalParams.urlServices + 'stores/deleteCategory'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getCategories: function(callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	saveCategory: function(data, callback) {
		data.token = loginUser.token
		var store = this
        store.add(new Designer.model.Category())
        store.save({
            scope: this,
            params: data,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]				
                if(result.success) { 
					response = JSON.parse(result._response.responseText)[0]				
					result = {
						error: response.Resultado == 'OK' ? -1 : response.Resultado,
						message: response.Error,
						response: {}
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).error.message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},

    delete: function(data, callback) {
        var store = this
        store.remove(data)
        store.erase({
            scope: this,
            id: data.CategoryID,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                idCategory: data.CategoryID
            },
            callback: function (o, response) {
                console.log(response)
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                    callback(false, result)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                    callback(true, result)
                }
            }
        })
    }
	
})