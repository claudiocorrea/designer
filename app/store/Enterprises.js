Ext.define('Designer.store.Enterprises', {
    extend: 'Ext.data.Store',
    alias: 'store.enterprises',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'enterprisesStore',
    model: 'Designer.model.Enterprise',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'stores/getAllEnterprises',
            create: globalParams.urlServices + 'stores/newEnterprise',
			destroy: globalParams.urlServices + 'stores/deleteEnterprise'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getEnterprises: function(callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	saveEnterprise: function(data, callback) {
		data.token = loginUser.token
		var store = this
        store.add(new Designer.model.Enterprise())
        store.save({
            scope: this,
            params: data,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
				response = JSON.parse(result._response.responseText)[0]
                if(result.success) { 					
					result = {
						error: response.Resultado == 'OK' ? -1 : response.Resultado,
						message: response.Error,
						response: {}
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.Resultado != 'OK', result)
            }
        })
	},

    delete: function(data, callback) {
        var store = this
        store.remove(data)
        store.erase({
            scope: this,
            id: data.IdEmpresa,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                idEmpresa: data.IdEmpresa
            },
            callback: function (o, response) {
                console.log(response)
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                    callback(false, result)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                    callback(true, result)
                }
            }
        })
    }
	
})