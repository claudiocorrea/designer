Ext.define('Designer.store.Workflows', {
    extend: 'Ext.data.Store',
    alias: 'store.workflows',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'workflowsStore',
    model: 'Designer.model.Workflow',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'workflow/getAllWorkflows',
			create: globalParams.urlServices + 'workflow/newWorkflow',
			destroy: globalParams.urlServices + 'workflow/deleteWorkflow'			
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getWorkflows: function(callback) {
        var store = this
        store.load({
			scope: this,
            params: {
                idUser: loginUser.idUser,
				token: loginUser.token
            },
            callback: function (o, response) {
				console.log(response)
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
					result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	saveWorkflow: function(workflowData, callback) {
		workflowData.idUser = loginUser.idUser
        workflowData.token = loginUser.token
		var store = this
        store.add(new Designer.model.Workflow())
        store.save({
            scope: this,
            params: workflowData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: 'OK',
						response: JSON.parse(result._response.responseText),
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},
	
	delete: function(data, callback) {
		var store = this
        store.remove(data)        
        store.erase({
            scope: this,
			id: data.WorkflowID,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                workflowId: data.WorkflowID
            },
            callback: function (o, response) {
				console.log(response)
				var result = response
				if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
					store.getWorkflows(callback)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
					callback(true, result)
                }                
            }
        })
		
	}
	
})