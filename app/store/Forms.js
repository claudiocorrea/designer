Ext.define('Designer.store.Forms', {
    extend: 'Ext.data.Store',
    alias: 'store.forms',
	
	requires: [ 'Ext.data.proxy.Rest' ],

    storeId: 'formsStore',
    model: 'Designer.model.Form',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'form/getAll',
            create: globalParams.urlServices + 'form/newForm',
			destroy: globalParams.urlServices + 'form/deleteForm'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getForms: function(callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	saveForm: function(formData, callback) {
		formData.idUser = loginUser.idUser
        formData.token = loginUser.token
		var store = this
        store.add(new Designer.model.Form())
        store.save({
            scope: this,
            params: formData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: 'OK',
						response: JSON.parse(result._response.responseText),
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},
	
	delete: function(data, callback) {
		var store = this
        store.remove(data)        
        store.erase({
            scope: this,
			id: data.IdForm,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                formId: data.IdForm
            },
            callback: function (o, response) {
				console.log(response)
				var result = response
				if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
					store.getForms(callback)
                } else {
                    result = {
                        error: result.error.response.status,
                        message: JSON.parse(result.error.response.responseText).error,
                        response: {}
                    }
					callback(true, result)
                }                
            }
        })
		
	}
	
})