Ext.define('Designer.store.MandatoryUsers', {
    extend: 'Ext.data.Store',
    alias: 'store.mandatoryUsers',
	
	requires: [ 'Ext.data.proxy.Rest' ],

    storeId: 'mandatoryUsersStore',
    model: 'Designer.model.MandatoryUser',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'step/getUsersByStep',
			update: globalParams.urlServices + 'step/setUsers',
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getUsers: function(stepId, callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token,
			stepId: stepId
        })
        store.load({
            callback: function (o, response) {
				console.log(response)
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	setUsers: function(stepId, users, callback) {
		var usersData = {
			idUser: loginUser.idUser,
			token: loginUser.token,
			stepId: stepId,
			users: users
		}		
		var store = this
        store.save({
            scope: this,
            params: usersData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: result._response.statusText,
						response: JSON.parse(result._response.responseText).result,
					}
                } else {
                    result = {
                        error: result.error.response.status,
                        message: JSON.parse(result.error.response.responseText).error,
                        response: {}
                    }
                }
                callback(response.operations[0].exception, result)
            }
        })
	}
	
})