Ext.define('Designer.store.Parallels', {
    extend: 'Ext.data.Store',
    alias: 'store.parallels',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'parallelsStore',
    model: 'Designer.model.Parallel',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'step/getUnion',
            create: globalParams.urlServices + 'step/updateUnion'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getUnion: function(StepId, callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token,
			stepId: StepId
        })
        store.load({
            callback: function (o, response) {
                var result = response			
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.response.status,
                        message: result.error.response.statusText,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	updateUnion: function(xmlData, callback) {
		var data = {
			idUser: loginUser.idUser,
			parallels: xmlData
		}			
		var store = this
        store.add(new Designer.model.Parallel())
        store.save({
            scope: this,
            params: data,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) { 
					response = JSON.parse(result._response.responseText)[0]
					if(response.Result == 'OK') {
						result = {
							error: -1,
							message: 'OK',
							response: {}
						}
					} else {
						result = {
							error: response.Status,
							message: response.Error,
							response: {}
						}
						response.exception = true
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	}
	
})