Ext.define('Designer.store.Steps', {
    extend: 'Ext.data.Store',
    alias: 'store.steps',
	
	requires: [
		'Ext.data.proxy.Rest'
	],

    storeId: 'stepsStore',
    model: 'Designer.model.Step',

    proxy: {
        type: 'rest',
        api: {
            read: globalParams.urlServices + 'step/getSteps',
			create: globalParams.urlServices + 'step/newStep',
            destroy: globalParams.urlServices + 'step/deleteStep'
        },
        reader: {
            type: 'json',
            rootProperty: 'data'
        },
        writer: {
            type: 'json'
        },
        listeners: {
            exception: function(proxy, response) {
                if (response.status == '0') {
                    Ext.Msg.alert('Error', 'El servidor no responde.')
                }
            }
        }
    },
	
	getSteps: function(workflowId, callback) {
        var store = this
		store.getProxy().setExtraParams({
            idUser: loginUser.idUser,
			token: loginUser.token,
			workflowId: workflowId
        })
        store.load({
            callback: function (o, response) {
                var result = response
				console.log(result)				
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }				
                callback(response.exception, result)
            }
        })
    },
	
	saveStep: function(stepData, callback) {
		stepData.idUser = loginUser.idUser
        stepData.token = loginUser.token
		var store = this
        store.add(new Designer.model.Workflow())
        store.save({
            scope: this,
            params: stepData,
            callback: function (response) {
				console.log(response)
                var result = response.operations[0]
                if(result.success) {                    
                    result = {
						error: -1,
						message: 'OK',
						response: JSON.parse(result._response.responseText)
					}
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                }
                callback(response.exception, result)
            }
        })
	},

    delete: function(data, callback) {
        var store = this
        store.remove(data)
        store.erase({
            scope: this,
            id: data.StepId,
            params: {
                idUser: loginUser.idUser,
                token: loginUser.token,
                stepId: data.StepId
            },
            callback: function (o, response) {
                console.log(response)
                var result = response
                if(result.success) {
                    result = {
                        error: -1,
                        message: 'OK',
                        response: result._resultSet.records
                    }
                    callback(false, result)
                } else {
                    result = {
                        error: result.error.status,
                        message: result.error.status == '0' ? 'El servidor no responde.' : JSON.parse(result.error.response.responseText).message,
                        response: {}
                    }
                    callback(true, result)
                }
            }
        })
    }
	
})