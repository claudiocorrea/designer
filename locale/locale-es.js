//Ext.onReady(function() {
    var txtFields = {
        words: {
			approved: 'Aprobados',
			back: 'Regresar',
			cancel: 'Cancelar',
			category: 'Categoria',
			clone: 'Clonar',
			close: 'Cerrar',
			code: 'Codigo',
			column: 'Columna',
			condition: 'Condición',
			confirm: 'Confirmación',
			create: 'Crear',
			date: 'Fecha',
			day: 'Dia',
			delete: 'Eliminar',
			dependency: 'Dependencia',
			description: 'Descripción',
			designer: 'Diseñador',
			edit: 'Editar',
			enabled: 'Habilitado',
			english: 'Ingles',
			enterprise: 'Empresa',
			event: 'Evento',
			firstname: 'Nombre',
			form: 'Formulario',
			group: 'Grupo',
			id: 'Identificador',
			information: 'Información',
			init: 'Inicio',
			instance: 'Instancia',
			label: 'Etiqueta', 
			lang: 'Idioma',
			lastname: 'Apellido',
			login: 'Acceder',
            logout: 'Salir',
			mandatory: 'Obligatorio',
			name: 'Nombre',
			new: 'Nuevo',
			original: 'Original',			
			password: 'Contraseña',
			position: 'Posición',
			profile: 'Perfil',
			properties: 'Propiedades',
			publish: 'Publicar',
			reference: 'Referencia',
			refresh: 'Actualizar',
			rejection: 'Rechazo',
			save: 'Guardar',
			signIn: 'Iniciar sesión',
			spanish: 'Español',
			state: 'Estado',
			step: 'Paso',
			theme: 'Tema',
			table: 'Tabla',
			title: 'Titulo',
			type: 'Tipo',
			user: 'Usuario',
			username: 'Usuario',
			workflow: 'Workflow',
			yes: 'Si'
        },
        titles: {  
			alertAndNotifications: 'Alertas y notificaciones',
			alertTemplate: 'Template de Alerta',
			allowEmpty: 'Permite vacio',
			approvalTemplate: 'Template de Aprobación',
			categories: 'Categorias',
			cloneWorkflow: 'Clonar workflow',
			createReference: 'Crear referencia',	
			dataOrigins: 'Origenes de los datos',
			dateGreaterOrSameAsToday: 'Fecha mayor o igual a hoy',
			dateType: 'Tipo fecha',
			dependsOn: 'Depende de',
			editWorkflow: 'Editar workflow',
			enableFields: 'Habilitar campos',
			enabledUsers: 'Usuarios habilitados',
			endDate: 'Fecha Fin',
			enterpriseUsers: 'Empresas por usuarios',
			externDocs: 'Documentos externos',
			fieldName: 'Nombre del campo',
			fieldValue: 'Valor del campo',
			findCategory: 'Buscar categoria...',
			findEnterprise: 'Buscar empresa...',
			findForm: 'Buscar formulario...',			
			findWorkflow: 'Buscar workflow...',
			findUser: 'Buscar usuario por nombre de usuario',
			formDesigner: 'Diseñador de formularios',
			formFields: 'Campos del formulario',
			generateExtras: 'Genera extras',
			gralAlerts: 'Genera alertas',
			groupsAndRoles: 'Grupos y roles',
			hiddenApprove: 'Oculta aprobar si es falso',
			htmlfield: 'Campo HTML', 
			ignoreIfEmpty: 'Ignora si esta vacio',
			inputParameters: 'Parametros de entrada',
			mandatoryUsers: 'Usuarios obligatorios',
			modelCheck: 'Verificación del modelo',
			newEnterprise: 'Nueva empresa',
			newWorkflow: 'Nuevo workflow',
			notifications: 'Notificaciones',
			onlyNumbers: 'Solo números',
			onlyRead: 'Solo lectura',
			optionNew: 'Opción nuevo', 
			originals: 'Originales',
			parallelNodes: 'Nodos paralelos',
			parallelStep: 'Pasos paralelos',			
			rejectionInForm: 'Rechazo en formulario',
			relationsNodes: 'Relación con otros nodos',
			relativeDates: 'Fechas relativas',
			selfEnumeration: 'Autonumeración',
			storeProcedure: 'Procedimiento almacenado',
			startDate: 'Fecha inicio',
			styleSheet: 'Hoja de estilo',
			textCombo: 'Campo texto del combo',
			timeAlerts: 'Tiempo Alerta',
			unionType: 'Tipo de union',
			viaMail: 'Via mail'
        },
        messages: {		
			checkingModel: 'Verificando modelo...',
			confirmLogout: 'Seguro desea salir?',
			confirmRemoveCategory: 'Seguro desea eliminar la categoria \"{0}\"?',
			confirmRemoveEnterprise: 'Seguro desea eliminar la empresa \"{0}\"?',
			confirmRemoveField: 'Seguro desea eliminar el campo \"{0}\"?',
			confirmRemoveForm: 'Seguro desea eliminar el formulario \"{0}\"?',
			confirmRemoveGateway: 'Seguro desea eliminar la condición?',
			confirmRemoveStep: 'Seguro desea eliminar el paso \"{0}\"?',
			confirmRemoveUser: 'Seguro desea eliminar el usuario \"{0}\"?',
			confirmRemoveWorkflow: 'Seguro desea eliminar el workflow \"{0}\"?',
			publishingModel: 'Publicando modelo...',
			savingChanges: 'Guardando cambios...',
			savingFields: 'Guardando campos...',
			savingForm: 'Guardando formulario...',
			savingGroups: 'Guardando grupos...',
			savingUsers: 'Guardando usuarios...',			
            alerts: {
				initing: 'Iniciando...',
                load: 'Cargando...',
				saving: 'Guandando...',
				wait: 'Espere...',
				saveSuccessfull: 'Datos guardados con &eacute;xito'
            },
            errors: {
				fields: 'Complete los campos faltantes',
				deleteField: 'No se puede eliminar. Debe existir al menos una seccion.',
				loginError: 'Usuario o contraseña incorrectos',
				parallelsSteps: 'Debe haber más de un paso seleccionado.',
				profileError: 'No tiene los permisos necesarios para ingresar.',
				sessionError: 'Sesion finalizada',
                tokenError: 'Identificador o token incorrectos',
                urlError: 'Error al cargar el ID de la solicitud',              
            },
			writeNameEnterprise: 'Ingrese nombre de la nueva empresa:',
			writeNameWorkflow: 'Ingrese nombre del nuevo workflow:', 
        }
    }

    if (Ext.Date) {
        Ext.Date.monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        Ext.Date.getShortMonthName = function(month) {
            return Ext.Date.monthNames[month].substring(0, 3);
        };

        Ext.Date.monthNumbers = {Ene: 0,Feb: 1,Mar: 2,Abr: 3,May: 4,Jun: 5,Jul: 6,Ago: 7,Sep: 8,Oct: 9,Nov: 10,Dic: 11};

        Ext.Date.getMonthNumber = function(name) {
            return Ext.Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
        };

        Ext.Date.dayNames = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];

        Ext.Date.getShortDayName = function(day) {
            if (day == 3) return "Mié";
            if (day == 6) return "Sáb";
            return Ext.Date.dayNames[day].substring(0, 3);
        };

        Ext.Date.formatCodes.a = "(this.getHours() < 12 ? 'a.m.' : 'p.m.')";
        Ext.Date.formatCodes.A = "(this.getHours() < 12 ? 'A.M.' : 'P.M.')";

        // This will match am or a.m.
        Ext.Date.parseCodes.a = Ext.Date.parseCodes.A = {
            g:1,
            c:"if (/(a\\.?m\\.?)/i.test(results[{0}])) {\n"
            + "if (!h || h == 12) { h = 0; }\n"
            + "} else { if (!h || h < 12) { h = (h || 0) + 12; }}",
            s:"(A\\.?M\\.?|P\\.?M\\.?|a\\.?m\\.?|p\\.?m\\.?)",
            calcAtEnd: true
        };

        Ext.Date.parseCodes.S.s = "(?:st|nd|rd|th)";
    }

    if (Ext.util && Ext.util.Format) {
        Ext.apply(Ext.util.Format, {
            thousandSeparator: '.',
            decimalSeparator: ',',
            currencySign: '\u20ac',
            // Spanish Euro
            dateFormat: 'd/m/Y'
        });
    }
//});

Ext.define("Ext.locale.es.view.View", {
    override: "Ext.view.View",
    emptyText: ""
});

Ext.define("Ext.locale.es.grid.plugin.DragDrop", {
    override: "Ext.grid.plugin.DragDrop",
    dragText: "{0} fila(s) seleccionada(s)"
});

// changing the msg text below will affect the LoadMask
Ext.define("Ext.locale.es.view.AbstractView", {
    override: "Ext.view.AbstractView",
    loadingText: "Cargando..."
});

Ext.define("Ext.locale.es.picker.Date", {
    override: "Ext.picker.Date",
    todayText: "Hoy",
    minText: "Esta fecha es anterior a la fecha mínima",
    maxText: "Esta fecha es posterior a la fecha máxima",
    disabledDaysText: "",
    disabledDatesText: "",
    nextText: 'Mes Siguiente (Control+Right)',
    prevText: 'Mes Anterior (Control+Left)',
    monthYearText: 'Seleccione un mes (Control+Up/Down para desplazar el año)',
    todayTip: "{0} (Barra espaciadora)",
    format: "d/m/Y",
    startDay: 1
});

Ext.define("Ext.locale.es.picker.Month", {
    override: "Ext.picker.Month",
    okText: "&#160;Aceptar&#160;",
    cancelText: "Cancelar"
});

Ext.define("Ext.locale.es.toolbar.Paging", {
    override: "Ext.PagingToolbar",
    beforePageText: "Página",
    afterPageText: "de {0}",
    firstText: "Primera página",
    prevText: "Página anterior",
    nextText: "Página siguiente",
    lastText: "Última página",
    refreshText: "Actualizar",
    displayMsg: "Mostrando {0} - {1} de {2}",
    emptyMsg: 'Sin datos para mostrar'
});

Ext.define("Ext.locale.es.form.field.Base", {
    override: "Ext.form.field.Base",
    invalidText: "El valor en este campo es inválido"
});

Ext.define("Ext.locale.es.form.field.Text", {
    override: "Ext.form.field.Text",
    minLengthText: "El tamaño mínimo para este campo es de {0}",
    maxLengthText: "El tamaño máximo para este campo es de {0}",
    blankText: "Este campo es obligatorio",
    regexText: "",
    emptyText: null
});

Ext.define("Ext.locale.es.form.field.Number", {
    override: "Ext.form.field.Number",
    decimalPrecision: 2,
    minText: "El valor mínimo para este campo es de {0}",
    maxText: "El valor máximo para este campo es de {0}",
    nanText: "{0} no es un número válido"
});

Ext.define("Ext.locale.es.form.field.File", {
    override: "Ext.form.field.File",
    buttonText: "Examinar..."
});


Ext.define("Ext.locale.es.form.field.Date", {
    override: "Ext.form.field.Date",
    disabledDaysText: "Deshabilitado",
    disabledDatesText: "Deshabilitado",
    minText: "La fecha para este campo debe ser posterior a {0}",
    maxText: "La fecha para este campo debe ser anterior a {0}",
    invalidText: "{0} no es una fecha válida - debe tener el formato {1}",
    format: "d/m/Y",
    altFormats: "d/m/Y|d-m-y|d-m-Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d"
});

Ext.define("Ext.locale.es.form.field.ComboBox", {
    override: "Ext.form.field.ComboBox",
    valueNotFoundText: undefined
}, function() {
    Ext.apply(Ext.form.field.ComboBox.prototype.defaultListConfig, {
        loadingText: "Cargando..."
    });
});

Ext.define("Ext.locale.es.form.field.VTypes", {
    override: "Ext.form.field.VTypes",
    emailText: 'Este campo debe ser una dirección de correo electrónico con el formato "usuario@dominio.com"',
    urlText: 'Este campo debe ser una URL con el formato "http:/' + '/www.dominio.com"',
    alphaText: 'Este campo sólo debe contener letras y _',
    alphanumText: 'Este campo sólo debe contener letras, números y _'
});

Ext.define("Ext.locale.es.form.field.HtmlEditor", {
    override: "Ext.form.field.HtmlEditor",
    createLinkText: "Por favor proporcione la URL para el enlace:"
}, function() {
    Ext.apply(Ext.form.field.HtmlEditor.prototype, {
        buttonTips: {
            bold: {
                title: 'Negritas (Ctrl+B)',
                text: 'Transforma el texto seleccionado en Negritas.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            italic: {
                title: 'Itálica (Ctrl+I)',
                text: 'Transforma el texto seleccionado en Itálicas.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            underline: {
                title: 'Subrayado (Ctrl+U)',
                text: 'Subraya el texto seleccionado.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            increasefontsize: {
                title: 'Aumentar la fuente',
                text: 'Aumenta el tamaño de la fuente',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            decreasefontsize: {
                title: 'Reducir la fuente',
                text: 'Reduce el tamaño de la fuente.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            backcolor: {
                title: 'Color de fondo',
                text: 'Modifica el color de fondo del texto seleccionado.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            forecolor: {
                title: 'Color de la fuente',
                text: 'Modifica el color del texto seleccionado.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyleft: {
                title: 'Alinear a la izquierda',
                text: 'Alinea el texto a la izquierda.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifycenter: {
                title: 'Centrar',
                text: 'Centrar el texto.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            justifyright: {
                title: 'Alinear a la derecha',
                text: 'Alinea el texto a la derecha.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertunorderedlist: {
                title: 'Lista de viñetas',
                text: 'Inicia una lista con viñetas.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            insertorderedlist: {
                title: 'Lista numerada',
                text: 'Inicia una lista numerada.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            createlink: {
                title: 'Enlace',
                text: 'Inserta un enlace de hipertexto.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            },
            sourceedit: {
                title: 'Código Fuente',
                text: 'Pasar al modo de edición de código fuente.',
                cls: Ext.baseCSSPrefix + 'html-editor-tip'
            }
        }
    });
});

Ext.define("Ext.locale.es.grid.header.Container", {
    override: "Ext.grid.header.Container",
    sortAscText: "Ordenar en forma ascendente",
    sortDescText: "Ordenar en forma descendente",
    columnsText: "Columnas"
});

Ext.define("Ext.locale.es.grid.GroupingFeature", {
    override: "Ext.grid.feature.Grouping",
    emptyGroupText: '(Ninguno)',
    groupByText: 'Agrupar por este campo',
    showGroupsText: 'Mostrar en grupos'
});

Ext.define("Ext.locale.es.grid.PropertyColumnModel", {
    override: "Ext.grid.PropertyColumnModel",
    nameText: "Nombre",
    valueText: "Valor",
    dateFormat: "j/m/Y"
});

Ext.define("Ext.locale.es.form.field.Time", {
    override: "Ext.form.field.Time",
    minText: "La hora en este campo debe ser igual o posterior a {0}",
    maxText: "La hora en este campo debe ser igual o anterior a {0}",
    invalidText: "{0} no es una hora válida",
    format: "g:i A",
    altFormats: "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"
});

Ext.define("Ext.locale.es.form.CheckboxGroup", {
    override: "Ext.form.CheckboxGroup",
    blankText: "Debe seleccionar al menos un étem de este grupo"
});

Ext.define("Ext.locale.es.form.RadioGroup", {
    override: "Ext.form.RadioGroup",
    blankText: "Debe seleccionar un étem de este grupo"
});

Ext.define("Ext.locale.es.window.MessageBox", {
    override: "Ext.window.MessageBox",
    buttonText: {
        ok: "Aceptar",
        cancel: "Cancelar",
        yes: "Sí",
        no: "No"
    }
});

// This is needed until we can refactor all of the locales into individual files
Ext.define("Ext.locale.es.Component", {
    override: "Ext.Component"
});
/*
Ext.define("Ext.locale.es.form.field.Checkbox", {
    override: "Ext.form.field.Checkbox ",
    fieldLabel: "Recordarme"
});

*/